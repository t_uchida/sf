Require Import mathcomp.ssreflect.ssreflect.
From mathcomp Require Import all_ssreflect.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Inductive id : Type := Id : nat -> id.

Definition eqb_id i1 i2 :=
  match (i1, i2) with
  | (Id n1, Id n2) => n1 == n2
  end.

Lemma eqb_idP : Equality.axiom eqb_id.
Proof.
  rewrite /Equality.axiom /eqb_id => [] [] x [] y. case H1: (x == y); constructor.
  - move/eqP :H1 => -> //.
  - move/eqP :H1 => H1 H2. apply H1. by inversion H2.
Qed.  

Canonical id_eqMixin := EqMixin eqb_idP.
Canonical id_eqType := Eval hnf in EqType id id_eqMixin.

Lemma eqb_id_refl (x : id) : x == x.
Proof. by apply/eqP. Qed.

Inductive ty : Type :=
  | ty_nat : ty
  | ty_unit : ty
  | ty_arrow : ty -> ty -> ty
  | ty_ref : ty -> ty.

Inductive tm : Type :=
  | tm_var : id -> tm
  | tm_app : tm -> tm -> tm
  | tm_abs : id -> ty -> tm -> tm
  | tm_nat : nat -> tm
  | tm_succ : tm -> tm
  | tm_pred : tm -> tm
  | tm_mult : tm -> tm -> tm
  | tm_if0 : tm -> tm -> tm -> tm
  | tm_unit : tm
  | tm_ref : tm -> tm
  | tm_deref : tm -> tm
  | tm_assign : tm -> tm -> tm
  | tm_loc : nat -> tm.

Inductive value : tm -> Prop :=
  | v_abs : forall x T t, value (tm_abs x T t)
  | v_nat : forall n, value (tm_nat n)
  | v_unit : value tm_unit
  | v_loc : forall l, value (tm_loc l).

Hint Constructors value.

Fixpoint subst x s t :=
  match t with
  | tm_var x' => if x == x' then s else t
  | tm_app t1 t2 => tm_app (subst x s t1) (subst x s t2)
  | tm_abs x' T t1 => if x == x' then t else tm_abs x' T (subst x s t1)
  | tm_nat n => t
  | tm_succ t1 => tm_succ (subst x s t1)
  | tm_pred t1 => tm_pred (subst x s t1)
  | tm_mult t1 t2 => tm_mult (subst x s t1) (subst x s t2)
  | tm_if0 t1 t2 t3 => tm_if0 (subst x s t1) (subst x s t2) (subst x s t3)
  | tm_unit => t
  | tm_ref t1 => tm_ref (subst x s t1)
  | tm_deref t1 => tm_deref (subst x s t1)
  | tm_assign t1 t2 => tm_assign (subst x s t1) (subst x s t2)
  | tm_loc _ => t
  end.

Definition tm_seq t1 t2 := tm_app (tm_abs (Id 0) ty_unit t2) t1.

Definition store := seq tm.

Definition store_lookup n st := nth tm_unit st n.

Lemma nth_lt_snoc A (l : seq A) x d n : n < size l -> nth d l n = nth d (rcons l x) n.
Proof. elim: l n => [|a l] //= IHl n. case: n => //=. Qed.

Lemma nth_eq_snoc A (l:list A) x d : nth d (rcons l x) (size l) = x.
Proof. by rewrite nth_rcons ltnn eqxx. Qed.

Function replace A n x (l : seq A) :=
  match l with
  | nil => nil
  | h :: t =>
    match n with
    | O => x :: t
    | S n' => h :: replace n' x t
    end
  end.

Lemma replace_nil A n (x : A) : replace n x nil = nil.
Proof. case: n => //=. Qed.

Lemma replace_cons A n (a : A) x xs : ~ replace n a (x :: xs) = nil.
Proof. rewrite replace_equation. case: n => //=. Qed.

Lemma length_replace A n x (l: seq A) : size (replace n x l) = size l.
Proof.
  elim: l n => [|a l IHl] n /=.
  - by rewrite replace_nil.
  - case: n => //= n. by congr (_.+1).
Qed.

Lemma lookup_replace_eq l t st : l < size st -> store_lookup l (replace l t st) = t.
Proof. rewrite /store_lookup. elim: st l => //= a st IHst l. case: l => //=. Qed.

Lemma lookup_replace_neq n1 n2 t st :
  n1 <> n2 -> store_lookup n1 (replace n2 t st) = store_lookup n1 st.
Proof.
  elim: n1 n2 t st => [|n1 IHn1] n2 t st H1 /=.
  - case: st => [|a st] /=.
    + by rewrite replace_nil.
    + case: n2 H1 => //=.
  - case: st IHn1 => [|a st] /= IHn1.
    + by rewrite replace_nil.
    + move H2: (replace n2 t (a :: st)) => st1. case: st1 H2.
      * move=> H2. case: (replace_cons H2).
      * case: n2 H1 => [|n2] H1 a1 st1 /=.
        rewrite /replace => [] [] _ <- //.
        case => _ <-. apply IHn1. move=> H2. apply H1. by congr (_.+1).
Qed.

Reserved Notation "t1 '/' st1 '==>' t2 '/' st2"
  (at level 40, st1 at level 39, t2 at level 39).

Inductive step : tm * store -> tm * store -> Prop :=
  | ST_AppAbs : forall x T t12 v2 st,
         value v2 ->
         tm_app (tm_abs x T t12) v2 / st ==> subst x v2 t12 / st
  | ST_App1 : forall t1 t1' t2 st st',
         t1 / st ==> t1' / st' ->
         tm_app t1 t2 / st ==> tm_app t1' t2 / st'
  | ST_App2 : forall v1 t2 t2' st st',
         value v1 ->
         t2 / st ==> t2' / st' ->
         tm_app v1 t2 / st ==> tm_app v1 t2'/ st'
  | ST_SuccNat : forall n st,
         tm_succ (tm_nat n) / st ==> tm_nat (S n) / st
  | ST_Succ : forall t1 t1' st st',
         t1 / st ==> t1' / st' ->
         tm_succ t1 / st ==> tm_succ t1' / st'
  | ST_PredNat : forall n st,
         tm_pred (tm_nat n) / st ==> tm_nat n.-1 / st
  | ST_Pred : forall t1 t1' st st',
         t1 / st ==> t1' / st' ->
         tm_pred t1 / st ==> tm_pred t1' / st'
  | ST_MultNats : forall n1 n2 st,
         tm_mult (tm_nat n1) (tm_nat n2) / st ==> tm_nat (mult n1 n2) / st
  | ST_Mult1 : forall t1 t2 t1' st st',
         t1 / st ==> t1' / st' ->
         tm_mult t1 t2 / st ==> tm_mult t1' t2 / st'
  | ST_Mult2 : forall v1 t2 t2' st st',
         value v1 ->
         t2 / st ==> t2' / st' ->
         tm_mult v1 t2 / st ==> tm_mult v1 t2' / st'
  | ST_If0 : forall t1 t1' t2 t3 st st',
         t1 / st ==> t1' / st' ->
         tm_if0 t1 t2 t3 / st ==> tm_if0 t1' t2 t3 / st'
  | ST_If0_Zero : forall t2 t3 st,
         tm_if0 (tm_nat 0) t2 t3 / st ==> t2 / st
  | ST_If0_Nonzero : forall n t2 t3 st,
         tm_if0 (tm_nat (S n)) t2 t3 / st ==> t3 / st
  | ST_RefValue : forall v1 st,
         value v1 ->
         tm_ref v1 / st ==> tm_loc (size st) / rcons st v1
  | ST_Ref : forall t1 t1' st st',
         t1 / st ==> t1' / st' ->
         tm_ref t1 / st ==> tm_ref t1' / st'
  | ST_DerefLoc : forall st l,
         l < size st ->
         tm_deref (tm_loc l) / st ==> store_lookup l st / st
  | ST_Deref : forall t1 t1' st st',
         t1 / st ==> t1' / st' ->
         tm_deref t1 / st ==> tm_deref t1' / st'
  | ST_Assign : forall v2 l st,
         value v2 ->
         l < size st ->
         tm_assign (tm_loc l) v2 / st ==> tm_unit / replace l v2 st
  | ST_Assign1 : forall t1 t1' t2 st st',
         t1 / st ==> t1' / st' ->
         tm_assign t1 t2 / st ==> tm_assign t1' t2 / st'
  | ST_Assign2 : forall v1 t2 t2' st st',
         value v1 ->
         t2 / st ==> t2' / st' ->
         tm_assign v1 t2 / st ==> tm_assign v1 t2' / st'

where "t1 '/' st1 '==>' t2 '/' st2" := (step (t1,st1) (t2,st2)).


Hint Constructors step.

Inductive refl_step_closure (X:Type) (R: X -> X -> Prop) 
                            : X -> X -> Prop :=
  | rsc_refl : forall (x : X),
                 refl_step_closure R x x
  | rsc_step : forall (x y z : X),
                    R x y ->
                    refl_step_closure R y z ->
                    refl_step_closure R x z.
Implicit Arguments refl_step_closure [[X]].

Definition stepmany := (refl_step_closure step).
Notation "t1 '/' st '==>*' t2 '/' st'" := (stepmany (t1,st) (t2,st'))
  (at level 40, st at level 39, t2 at level 39).


Definition partial_map (A:Type) := id -> option A.

Definition context := partial_map ty.

Definition store_ty := seq ty.

Definition store_ty_lookup (n:nat) (ST:store_ty) := nth ty_unit ST n.

Definition empty {A:Type} : partial_map A := (fun _ => None).

Definition extend A (c : partial_map A) x T :=
  fun x' => if x == x' then Some T else c x'.

Lemma extend_eq A (c: partial_map A) x T : (extend c x T) x = Some T.
Proof. by rewrite /extend eqb_id_refl. Qed.

Lemma extend_neq A (c : partial_map A) x1 T x2 :
  x2 == x1 = false -> (extend c x2 T) x1 = c x1.
Proof. by rewrite /extend => ->. Qed.

Lemma extend_shadow A (ctxt: partial_map A) t1 t2 x1 x2 :
  extend (extend ctxt x2 t1) x2 t2 x1 = extend ctxt x2 t2 x1.
Proof. rewrite /extend. by case: (x2 == x1). Qed.

Inductive has_type : context -> store_ty -> tm -> ty -> Prop :=
  | T_Var : forall c ST x T,
      c x = Some T ->
      has_type c ST (tm_var x) T
  | T_Abs : forall c ST x T11 T12 t12,
      has_type (extend c x T11) ST t12 T12 ->
      has_type c ST (tm_abs x T11 t12) (ty_arrow T11 T12)
  | T_App : forall T1 T2 c ST t1 t2,
      has_type c ST t1 (ty_arrow T1 T2) ->
      has_type c ST t2 T1 ->
      has_type c ST (tm_app t1 t2) T2
  | T_Nat : forall c ST n,
      has_type c ST (tm_nat n) ty_nat
  | T_Succ : forall c ST t1,
      has_type c ST t1 ty_nat ->
      has_type c ST (tm_succ t1) ty_nat
  | T_Pred : forall c ST t1,
      has_type c ST t1 ty_nat ->
      has_type c ST (tm_pred t1) ty_nat
  | T_Mult : forall c ST t1 t2,
      has_type c ST t1 ty_nat ->
      has_type c ST t2 ty_nat ->
      has_type c ST (tm_mult t1 t2) ty_nat
  | T_If0 : forall c ST t1 t2 t3 T,
      has_type c ST t1 ty_nat ->
      has_type c ST t2 T ->
      has_type c ST t3 T ->
      has_type c ST (tm_if0 t1 t2 t3) T
  | T_Unit : forall c ST,
      has_type c ST tm_unit ty_unit
  | T_Loc : forall c ST l,
      l < size ST ->
      has_type c ST (tm_loc l) (ty_ref (store_ty_lookup l ST))
  | T_Ref : forall c ST t1 T1,
      has_type c ST t1 T1 ->
      has_type c ST (tm_ref t1) (ty_ref T1)
  | T_Deref : forall c ST t1 T11,
      has_type c ST t1 (ty_ref T11) ->
      has_type c ST (tm_deref t1) T11
  | T_Assign : forall c ST t1 t2 T11,
      has_type c ST t1 (ty_ref T11) ->
      has_type c ST t2 T11 ->
      has_type c ST (tm_assign t1 t2) ty_unit.

Hint Constructors has_type.

Definition store_well_typed (ST:store_ty) (st:store) :=
  size ST = size st /\
  (forall l, l < size st -> has_type empty ST (store_lookup l st) (store_ty_lookup l ST)).

Inductive extends : store_ty -> store_ty -> Prop :=
  | extends_nil : forall ST', extends ST' nil
  | extends_cons : forall x ST' ST, extends ST' ST -> extends (x::ST') (x::ST).

Hint Constructors extends.

Lemma extends_lookup l ST ST' :
  l < size ST -> extends ST' ST -> store_ty_lookup l ST' = store_ty_lookup l ST.
Proof.
  elim: ST ST' l => [|T1 ST IHST] ST' l //=.
  rewrite /store_ty_lookup. case: ST' => [|T2 ST'] /= H1 H2; inversion H2; subst.
  case: l H1 => //= l H1. apply IHST => //.
Qed.

Lemma length_extends l ST ST' : l < size ST -> extends ST' ST -> l < size ST'.
Proof. move=> H1 H2. elim: H2 l H1 => //= T ST1 ST2 H2 IH2 l. case: l => //. Qed.  

Lemma extends_snoc ST T : extends (rcons ST T) ST.
Proof. elim: ST => //= T1 ST H1. by constructor. Qed.

Lemma extends_refl ST : extends ST ST.
Proof. elim: ST => //= T ST H1. by constructor. Qed.

Lemma store_ty_lookup_size_rcons ST T : store_ty_lookup (size ST) (rcons ST T) = T.
Proof. rewrite /store_ty_lookup. elim: ST T => //. Qed.

Lemma size_lt_rcons A (s : seq A) x : size s < size (rcons s x).
Proof. elim: s x => //=. Qed.

Inductive appears_free_in : id -> tm -> Prop :=
  | afi_var : forall x,
      appears_free_in x (tm_var x)
  | afi_app1 : forall x t1 t2,
      appears_free_in x t1 -> appears_free_in x (tm_app t1 t2)
  | afi_app2 : forall x t1 t2,
      appears_free_in x t2 -> appears_free_in x (tm_app t1 t2)
  | afi_abs : forall x y T11 t12,
      y <> x ->
      appears_free_in x t12 ->
      appears_free_in x (tm_abs y T11 t12)
  | afi_succ : forall x t1,
      appears_free_in x t1 ->
      appears_free_in x (tm_succ t1)
  | afi_pred : forall x t1,
      appears_free_in x t1 ->
      appears_free_in x (tm_pred t1)
  | afi_mult1 : forall x t1 t2,
      appears_free_in x t1 ->
      appears_free_in x (tm_mult t1 t2)
  | afi_mult2 : forall x t1 t2,
      appears_free_in x t2 ->
      appears_free_in x (tm_mult t1 t2)
  | afi_if0_1 : forall x t1 t2 t3,
      appears_free_in x t1 ->
      appears_free_in x (tm_if0 t1 t2 t3)
  | afi_if0_2 : forall x t1 t2 t3,
      appears_free_in x t2 ->
      appears_free_in x (tm_if0 t1 t2 t3)
  | afi_if0_3 : forall x t1 t2 t3,
      appears_free_in x t3 ->
      appears_free_in x (tm_if0 t1 t2 t3)
  | afi_ref : forall x t1,
      appears_free_in x t1 -> appears_free_in x (tm_ref t1)
  | afi_deref : forall x t1,
      appears_free_in x t1 -> appears_free_in x (tm_deref t1)
  | afi_assign1 : forall x t1 t2,
      appears_free_in x t1 -> appears_free_in x (tm_assign t1 t2)
  | afi_assign2 : forall x t1 t2,
      appears_free_in x t2 -> appears_free_in x (tm_assign t1 t2).

Hint Constructors appears_free_in.

Lemma free_in_context x t T c ST :
  appears_free_in x t -> has_type c ST t T -> exists T', c x = Some T'.
Proof.
  move=> H1. elim: H1 c T => /=
  [x1|x1 t1 t2 H1 IH1|x1 t1 t2 H1 IH1|x1 y T11 t12 H1 H3 IH1|x1 t1 H1 IH1|x1 t1 H1 IH1
  |x1 t1 t2 H1 IH1|x1 t1 t2 H1 IH1|x1 t1 t2 t3 H1 IH1|x1 t1 t2 t3 H1 IH1|x1 t1 t2 t3 H1 IH1
  |x1 t1 H1 IH1|x1 t1 H1 IH1|x1 t1 t2 H1 IH1|x1 t1 t2 H1 IH1] c T H2;
  try (inversion H2; subst; eauto).
  move/IH1: H8. rewrite /extend. move/eqP/negbTE: H1 => -> //.
Qed.

Lemma context_invariance c c' ST t T :
  has_type c ST t T ->
  (forall x, appears_free_in x t -> c x = c' x) ->
  has_type c' ST t T.
Proof.
  move=> H1. elim: H1 c'.
  - move=> c1 ST1 x T1 H1 c'. constructor. by rewrite -H.
  - move=> c1 ST1 x T11 T12 t12 H1 IH1 c' H2. constructor. apply IH1 => x1 H3.
    rewrite /extend. case H4: (x == x1) => //. rewrite H2 => //.
    constructor => //. by move/eqP :H4.
  - move=> T1 T2 c1 ST1 t1 t2 H1 IH1 H2 IH2 c2 H3. eapply T_App.
    + apply IH1 => x H4. apply H3. by apply afi_app1.
    + apply IH2 => x H4. apply H3. by apply afi_app2.
  - move=> c1 ST1 n c2 IH1. constructor.
  - move=> c1 ST1 t1 H1 IH1 c2 IH2. constructor => //. apply IH1 => x H2.
    apply IH2. by constructor.
  - move=> c1 ST1 t1 H1 IH1 c2 H2. constructor. apply IH1 => x H3.
    apply H2. by constructor.
  - move=> c1 ST1 t1 t2 H1 IH1 H2 IH2 c2 H3. constructor.
    + apply IH1 => x H4. apply H3. by apply afi_mult1.
    + apply IH2 => x H4. apply H3. by apply afi_mult2.
  - move=> c1 ST1 t1 t2 t3 T1 H1 IH1 H2 IH2 H3 IH3 c2 H4. constructor.
    + apply IH1 => x H5. apply H4. by apply afi_if0_1.
    + apply IH2 => x H5. apply H4. by apply afi_if0_2.
    + apply IH3 => x H5. apply H4. by apply afi_if0_3.
  - constructor.
  - by constructor.
  - move=> c1 ST1 t1 T1 H1 IH1 c2 H2. constructor. apply IH1 => x H3. apply H2.
    by constructor.
  - move=> c1 ST1 t1 T11 H1 IH1 c2 H2. constructor. apply IH1 => x H3. apply H2.
    by constructor.
  - move=> c1 ST1 t1 t2 T11 H1 IH1 H2 IH2 c2 H3. eapply T_Assign.
    + apply IH1 => x H4. apply H3. by apply afi_assign1.
    + apply IH2 => x H4. apply H3. by apply afi_assign2.
Qed.

Lemma substitution_preserves_typing c ST x s S t T :
  has_type empty ST s S -> has_type (extend c x S) ST t T -> has_type c ST (subst x s t) T.
Proof.
  move=> H1. elim: t c T => /=.
  - move=> i c T H2. rewrite /subst. case H3: (x == i).
    + inversion H2; subst. rewrite /extend H3 in H5. case: H5 => <-. eapply context_invariance.
      apply H1. move=> x1 H4. move: (free_in_context H4 H1) => [] x2 //.
    + apply T_Var. inversion H2; subst. by rewrite /extend H3 in H5.
  - move=> t1 IHt1 t2 IHt2 c T H2. inversion H2; subst. eapply T_App.
    + apply IHt1. apply H5.
    + apply IHt2. apply H7.
  - move=> i T1 t1 IHt1 c T2 H2. case H3: (x == i).
    + inversion H2; subst. apply T_Abs. eapply context_invariance.
      apply H8. move=> x1 H4. move/eqP :H3 => ->. apply extend_shadow.
    + inversion H2; subst. apply T_Abs. apply IHt1. eapply context_invariance.
      apply H8. move=> x1 H4. rewrite /extend.
      case H5: (i == x1). move: H3. move/eqP :H5 => -> -> //. case: (x == x1) => //.
  - move=> n c T H2. inversion H2; subst. constructor.
  - move=> t IHt c T H2. inversion H2; subst. constructor. by apply IHt.
  - move=> t IHt c T H2. inversion H2; subst. constructor. by apply IHt.
  - move=> t1 IHt1 t2 IHt2 c T H2. inversion H2; subst. constructor.
    + by apply IHt1.
    + by apply IHt2.
  - move=> t1 IHt1 t2 IHt2 t3 IHt3 c T H2. inversion H2; subst. constructor.
    + by apply IHt1.
    + by apply IHt2.
    + by apply IHt3.
  - move=> c T H2. inversion H2; subst. constructor.
  - move=> t IHt c T H2. inversion H2; subst. constructor. by apply IHt.
  - move=> t IHt c T H2. inversion H2; subst. constructor. by apply IHt.
  - move=> t1 IHt1 t2 IHt2 c T H2. inversion H2; subst. eapply T_Assign.
    + apply IHt1. apply H5.
    + apply IHt2. apply H7.
  - move=> n c T H2. inversion H2; subst. by constructor.
Qed.

Lemma assign_pres_store_typing ST st l t :
  l < size st ->
  store_well_typed ST st ->
  has_type empty ST t (store_ty_lookup l ST) ->
  store_well_typed ST (replace l t st).
Proof.
  move=> H1 H2 H3. inversion H2; subst. split.
  - by rewrite length_replace.
  - move=> n H4. case H5: (l == n).
    + move/eqP :H5 => <-. by rewrite lookup_replace_eq.
    + move: H5. rewrite eq_sym. move/eqP/lookup_replace_neq => ->. apply H0.
      move: H4. by rewrite length_replace.
Qed.

Lemma store_weakening c ST ST' t T :
  extends ST' ST -> has_type c ST t T -> has_type c ST' t T.
Proof.
  move=> H1 H2. elim: H2 ST' H1; eauto.
  move=> c1 ST1 n H2 ST2 H3. rewrite -(extends_lookup H2 H3). constructor.
  apply (length_extends H2 H3).
Qed.

Lemma store_well_typed_snoc ST st t1 T1 :
  store_well_typed ST st ->
  has_type empty ST t1 T1 ->
  store_well_typed (rcons ST T1) (rcons st t1).
Proof.
  rewrite /store_well_typed /store_lookup /store_ty_lookup => [[H1 H2] H3]. split.
  - rewrite 2!size_rcons. by congr (_.+1).
  - move=> l. rewrite size_rcons ltnS leq_eqVlt. case/orP => H4.
    + move/eqP :(H4) => ->. rewrite nth_eq_snoc. rewrite -H1. rewrite nth_eq_snoc.
      eapply store_weakening. apply extends_snoc. apply H3.
    + rewrite -nth_lt_snoc => //. move: (H4). rewrite -H1 => H5. rewrite -nth_lt_snoc => //.
      eapply store_weakening. apply extends_snoc.
      apply H2. apply H4.
Qed.

Theorem preservation ST t t' T st st' :
  has_type empty ST t T ->
  store_well_typed ST st ->
  t / st ==> t' / st' ->
  exists ST', (extends ST' ST /\ has_type empty ST' t' T /\ store_well_typed ST' st').
Proof.
  move Hc: (@empty ty) => c. move=> H1. elim: H1 t' Hc.
  - move=> c1 ST1 x T1 _ t1 Hc _ H4. inversion H4.
  - move=> c1 ST1 x T11 T12 t12 _ _ t1 Hc _ H4. inversion H4.
  - move=> T1 T2 c1 ST1 t1 t2 H1 IH1 H3 IH3 t3 Hc H4 H5. inversion H5; subst.
    + exists ST1. split; [|split] => //. apply extends_refl.
      inversion H1; subst. eapply substitution_preserves_typing. apply H3. apply H7.
    + move: (IH1 _ erefl H4 H0) => [] ST2 [] H6 [] H7 H8. exists ST2. split; [|split] => //.
      eapply T_App. apply H7. eapply store_weakening. apply H6. apply H3.
    + move: (IH3 _ erefl H4 H9) => [] ST2 [] H6 [] H7 H8. exists ST2. split; [|split] => //.
      eapply T_App. eapply store_weakening. apply H6. apply H1. apply H7.
  - move=> c1 ST1 n t1 _ H2 H3. inversion H3.
  - move=> c1 ST1 t1 H1 IH1 t2 H2 H3 H4. inversion H4; subst.
    + exists ST1. split; [|split] => //. apply extends_refl.
    + move: (IH1 _ erefl H3 H0) => [] ST2 [] H5 [] H6 H7. exists ST2. split; [|split] => //.
      by constructor.
  - move=> c1 ST1 t1 H1 IH1 t2 H2 H3 H4. inversion H4; subst.
    + exists ST1. split; [|split] => //. apply extends_refl.
    + move: (IH1 _ erefl H3 H0) => [] ST2 [] H5 [] H6 H7. exists ST2. split; [|split] => //.
      by constructor.
  - move=> c1 ST1 t1 t2 H1 IH1 H2 IH2 t3 Hc H3 H4. inversion H4; subst.
    + exists ST1. split; [|split] => //. apply extends_refl.
    + move: (IH1 _ erefl H3 H0) => [] ST2 [] H5 [] H6 H7. exists ST2. split; [|split] => //.
      constructor. apply H6. eapply store_weakening. apply H5.  apply H2.
    + move: (IH2 _ erefl H3 H9) => [] ST2 [] H6 [] H7 H8. exists ST2. split; [|split] => //.
      constructor. eapply store_weakening. apply H6. apply H1. apply H7.
  - move=> c1 ST1 t1 t2 t3 T1 H1 IH1 H2 IH2 H3 IH3 t4 Hc H4 H5. inversion H5; subst.
    + move: (IH1 _ erefl H4 H0) => [] ST2 [] H6 [] H7 H8. exists ST2. split; [|split] => //.
      constructor.
      * apply H7.
      * eapply store_weakening. apply H6. apply H2.
      * eapply store_weakening. apply H6. apply H3.
    + exists ST1. split; [|split] => //. apply extends_refl.
    + exists ST1. split; [|split] => //. apply extends_refl.
  - move=> c1 ST1 t1 H1 H2 H3. inversion H3.
  - move=> c1 ST1 n H1 t1 Hc H2 H3. inversion H3.
  - move=> c1 ST1 t1 T1 H1 IH1 t2 Hc H2 H3. inversion H3; subst.
    + exists (rcons ST1 T1). split; [|split].
      * apply extends_snoc.
      * rewrite -{2}(store_ty_lookup_size_rcons ST1 T1). move: H2 => [H4 H5]. rewrite -H4.
        constructor. apply size_lt_rcons.
      * apply store_well_typed_snoc => //.
    + move: (IH1 _ erefl H2 H0) => [] ST2 [] H4 [] H5 H6. exists ST2. split; [|split] => //.
      by constructor.
  - move=> c1 ST1 t1 T11 H1 IH1 t2 Hc H2 H3. inversion H3; subst.
    + exists ST1. split; [|split] => //. apply extends_refl. inversion H1; subst.
      move: H2. rewrite /store_well_typed. case => H8 H9. eapply context_invariance => //.
      apply (H9 l H0).
    + move: (IH1 _ erefl H2 H0) => [] ST2 [] H4 [] H5 H6. exists ST2. split; [|split] => //.
      by constructor.
  - move=> c1 ST1 t1 t2 T11 H1 IH1 H2 IH2 t3 Hc H3 H4. inversion H4; subst.
    + exists ST1. split; [|split] => //. apply extends_refl. apply assign_pres_store_typing => //.
      by inversion H1; subst.
    + move: (IH1 _ erefl H3 H0) => [] ST2 [] H5 [] H6 H7. exists ST2. split; [|split] => //.
      eapply T_Assign. apply H6. eapply store_weakening. apply H5. apply H2.
    + move: (IH2 _ erefl H3 H9) => [] ST2 [] H6 [] H7 H8. exists ST2. split; [|split] => //.
      eapply T_Assign. eapply store_weakening. apply H6. apply H1. apply H7.
Qed.

Theorem progress ST t T st :
  has_type empty ST t T ->
  store_well_typed ST st ->
  (value t \/ exists t', exists st', t / st ==> t' / st').
Proof.
  move H1: (@empty ty) => c1. move=> H3. elim: H3 H1.
  - move=> c ST1 x T1 H1 H2. subst. inversion H1.
  - move=> c ST1 x T11 T12 t12 H1 H2 _. left. constructor.
  - move=> T1 T2 c ST1 t1 t2 H1 IH1 H2 IH2 H3 H4. case: (IH1 H3 H4) => H5.
    + case: (IH2 H3 H4) => H6.
      * inversion H1; subst; try (inversion H5); subst. right.
        exists (subst x t2 t12). exists st. by constructor.
      * move: H6 => [] t2' [] st' H6. right. exists (tm_app t1 t2'). exists st'. by constructor.
    + move: H5 => [] t1' [] st' H5. right. exists (tm_app t1' t2). exists st'. by constructor.
  - move=> c ST1 n. left. constructor.
  - move=> c ST1 t1 H1 IH1 H2 H3. case: (IH1 H2 H3) => H4.
    + inversion H4; subst; inversion H1; subst. right. exists (tm_nat n.+1). exists st.
      constructor.
    + move: H4 => [] t1' [] st' H4. right. exists (tm_succ t1'). exists st'. by constructor.
  - move=> c ST1 t1 H1 IH1 H2 H3. case (IH1 H2 H3) => H4.
    + inversion H4; subst; inversion H1; subst. right. exists (tm_nat n.-1). exists st.
      constructor.
    + move: H4 => [] t1' [] st' H4. right. exists (tm_pred t1'). exists st'. by constructor.
  - move=> c ST1 t1 t2 H1 IH1 H2 IH2 H3 H4. case (IH1 H3 H4) => H5.
    + case: (IH2 H3 H4) => H6.
      * inversion H1; subst; try (inversion H5); subst.
        inversion H2; subst; try (inversion H6); subst.
        right. exists (tm_nat (mult n n0)). exists st. constructor.
      * move: H6 => [] t2' [] st' H6. right. exists (tm_mult t1 t2'). exists st'. by constructor.
    + move: H5 => [] t1' [] st' H5. right. exists (tm_mult t1' t2). exists st'. by constructor.
  - move=> c ST1 t1 t2 t3 T1 H1 IH1 H2 IH2 H3 IH3 H4 H5. case (IH1 H4 H5) => H6.
    + inversion H1; subst; inversion H6; subst. case: n H6 H1 IH1 => [|n] H6 H1 IH1.
      * right. exists t2. exists st. constructor.
      * right. exists t3. exists st. constructor.
    + move: H6 => [] t1' [] st' H6. right. exists (tm_if0 t1' t2 t3). exists st'. by constructor.
  - move=> c ST1 Hc. left. constructor.
  - move=> c ST1 l H1 Hc. left. constructor.
  - move=> c ST1 t1 T1 H1 IH1 Hc H2. case (IH1 Hc H2) => H3.
    + right. exists (tm_loc (size st)). exists (rcons st t1). by constructor.
    + move: H3 => [] t1' [] st' H3. right. exists (tm_ref t1'). exists st'. by constructor.
  - move=> c ST1 t1 T11 H1 IH1 Hc H2. case (IH1 Hc H2) => H3.
    + inversion H3; subst; inversion H1; subst. right. exists (store_lookup l st). exists st.
      constructor. rewrite /store_well_typed in H2. move: H2 => [H7 H8]. by rewrite -H7.
    + move: H3 => [] t1' [] st' H3. right. exists (tm_deref t1'). exists st'. by constructor.
  - move=> c ST1 t1 t2 T11 H1 IH1 H2 IH2 Hc H3. case (IH1 Hc H3) => H4.
    + case (IH2 Hc H3) => H5.
      * inversion H1; subst; inversion H4; subst. right. exists tm_unit. exists (replace l t2 st).
        constructor. apply H5. rewrite /store_well_typed in H3. move: H3 => [H9 H10].
        by rewrite -H9.
      * move: H5 => [] t2' [] st' H5. right. exists (tm_assign t1 t2'). exists st'.
        by constructor.
    + move: H4 => [] t1' [] st' H4. right. exists (tm_assign t1' t2). exists st'.
      by constructor.
Qed.

Function ntho T (s : seq T) (n : nat) : option T :=
  match s with
  | [::] => None
  | x :: s' =>
    match n with
    | 0 => Some x
    | n'.+1 => ntho s' n'
    end
  end.

Inductive gc_step_ : store -> tm -> seq nat -> seq nat -> Prop :=
  | gc_step_loc_1 : forall st l ls, l \in ls -> gc_step_ st (tm_loc l) ls ls
  | gc_step_loc_2 : forall st l ls1 ls2 t, ~ l \in ls1 -> ntho st l = some t ->
      gc_step_ st t (rcons ls1 l) ls2 -> gc_step_ st (tm_loc l) ls1 ls2
  | gc_step_var : forall st v ls, gc_step_ st (tm_var v) ls ls
  | gc_step_app : forall st t1 t2 ls1 ls2 ls3,
      gc_step_ st t1 ls1 ls2 -> gc_step_ st t2 ls2 ls3 -> gc_step_ st (tm_app t1 t2) ls1 ls3
  | gc_step_abs : forall st v T tm ls1 ls2,
      gc_step_ st tm ls1 ls2 -> gc_step_ st (tm_abs v T tm) ls1 ls2
  | gc_step_nat : forall st n ls, gc_step_ st (tm_nat n) ls ls
  | gc_step_succ : forall st t ls1 ls2, gc_step_ st t ls1 ls2 -> gc_step_ st (tm_succ t) ls1 ls2
  | gc_step_pred : forall st t ls1 ls2, gc_step_ st t ls1 ls2 -> gc_step_ st (tm_pred t) ls1 ls2
  | gc_step_mult : forall st t1 t2 ls1 ls2 ls3,
      gc_step_ st t1 ls1 ls2 -> gc_step_ st t2 ls2 ls3 -> gc_step_ st (tm_mult t1 t2) ls1 ls3
  | gc_step_if0 : forall st t1 t2 t3 ls1 ls2 ls3 ls4,
      gc_step_ st t1 ls1 ls2 -> gc_step_ st t2 ls2 ls3 -> gc_step_ st t3 ls3 ls4 ->
      gc_step_ st (tm_if0 t1 t2 t3) ls1 ls4
  | gc_step_unit : forall st ls, gc_step_ st tm_unit ls ls
  | gc_step_ref : forall st t ls1 ls2, gc_step_ st t ls1 ls2 -> gc_step_ st (tm_ref t) ls1 ls2
  | gc_step_deref : forall st t ls1 ls2, gc_step_ st t ls1 ls2 -> gc_step_ st (tm_deref t) ls1 ls2
  | gc_step_assign : forall st t1 t2 ls1 ls2 ls3,
      gc_step_ st t1 ls1 ls2 -> gc_step_ st t2 ls2 ls3 -> gc_step_ st (tm_assign t1 t2) ls1 ls3.

Definition gc_step st t ls := gc_step_ st t [::] ls.

Inductive gc_update_term : seq nat -> tm -> tm -> Prop :=
  | gc_upd_loc : forall ls l1 l2, ntho ls l2 = Some l1 -> gc_update_term ls (tm_loc l1) (tm_loc l2)
  | gc_upd_var : forall ls x, gc_update_term ls (tm_var x) (tm_var x)
  | gc_upd_app : forall ls t11 t12 t21 t22,
      gc_update_term ls t11 t21 -> gc_update_term ls t12 t22 ->
      gc_update_term ls (tm_app t11 t12) (tm_app t21 t22)
  | gc_upd_abs : forall ls x T t1 t2,
      gc_update_term ls t1 t2 -> gc_update_term ls (tm_abs x T t1) (tm_abs x T t2)
  | gc_upd_nat : forall ls n, gc_update_term ls (tm_nat n) (tm_nat n)
  | gc_upd_succ : forall ls t1 t2,
      gc_update_term ls t1 t2 -> gc_update_term ls (tm_succ t1) (tm_succ t2)
  | gc_upd_pred : forall ls t1 t2,
      gc_update_term ls t1 t2 -> gc_update_term ls (tm_pred t1) (tm_pred t2)
  | gc_upd_mult : forall ls t11 t12 t21 t22,
      gc_update_term ls t11 t21 -> gc_update_term ls t12 t22 ->
      gc_update_term ls (tm_mult t11 t12) (tm_mult t21 t22)
  | gc_upd_if0 : forall ls t11 t12 t13 t21 t22 t23,
      gc_update_term ls t11 t21 -> gc_update_term ls t12 t22 -> gc_update_term ls t13 t23 ->
      gc_update_term ls (tm_if0 t11 t12 t13) (tm_if0 t21 t22 t23)
  | gc_upd_unit : forall ls, gc_update_term ls tm_unit tm_unit
  | gc_upd_ref : forall ls t1 t2,
      gc_update_term ls t1 t2 -> gc_update_term ls (tm_ref t1) (tm_ref t2)
  | gc_upd_deref : forall ls t1 t2,
      gc_update_term ls t1 t2 -> gc_update_term ls (tm_deref t1) (tm_deref t2)
  | gc_upd_assign : forall ls t11 t12 t21 t22,
      gc_update_term ls t11 t12 -> gc_update_term ls t21 t22 ->
      gc_update_term ls (tm_assign t11 t12) (tm_assign t21 t22).

Inductive gc_copy_ : store -> seq nat -> seq nat -> store -> Prop :=
  | gc_copy_nil : forall st ns, gc_copy_ st ns [::] [::]
  | gc_copy_cons : forall st ns l ls t1 t2 ts ,
      ntho st l = Some t1 -> gc_update_term ns t1 t2 -> gc_copy_ st ns ls ts ->
      gc_copy_ st ns (rcons ls l) (rcons ts t2).

Definition gc_copy st ls t := gc_copy_ st ls ls t.

Inductive eq_term_store : store -> tm -> store -> tm -> Prop :=
  | eq_ts_var : forall st1 st2 x, eq_term_store st1 (tm_var x) st2 (tm_var x)
  | eq_ts_app : forall st1 st2 t11 t12 t21 t22,
      eq_term_store st1 t11 st2 t21 -> eq_term_store st1 t12 st2 t22 ->
      eq_term_store st1 (tm_app t11 t12) st2 (tm_app t21 t22)
  | eq_ts_abs : forall st1 st2 x T t1 t2,
      eq_term_store st1 t1 st2 t2 -> eq_term_store st1 (tm_abs x T t1) st2 (tm_abs x T t2)
  | eq_ts_nat : forall st1 st2 n, eq_term_store st1 (tm_nat n) st2 (tm_nat n)
  | eq_ts_succ : forall st1 st2 t1 t2,
      eq_term_store st1 t1 st2 t2 -> eq_term_store st1 (tm_succ t1) st2 (tm_succ t2)
  | eq_ts_pred : forall st1 st2 t1 t2,
      eq_term_store st1 t1 st2 t2 -> eq_term_store st1 (tm_pred t1) st2 (tm_pred t2)
  | eq_ts_mult : forall st1 st2 t11 t12 t21 t22,
      eq_term_store st1 t11 st2 t21 -> eq_term_store st1 t12 st2 t22 ->
      eq_term_store st1 (tm_mult t11 t12) st2 (tm_mult t21 t22)
  | eq_ts_if0 : forall st1 st2 t11 t12 t13 t21 t22 t23,
      eq_term_store st1 t11 st2 t21 -> eq_term_store st1 t12 st2 t22 ->
      eq_term_store st1 t13 st2 t23 ->
      eq_term_store st1 (tm_if0 t11 t12 t13) st2 (tm_if0 t21 t22 t23)
  | eq_ts_unit : forall st1 st2, eq_term_store st1 tm_unit st2 tm_unit
  | eq_ts_ref : forall st1 st2 t1 t2,
      eq_term_store st1 t1 st2 t2 -> eq_term_store st1 (tm_ref t1) st2 (tm_ref t2)
  | eq_ts_deref : forall st1 st2 t1 t2,
      eq_term_store st1 t1 st2 t2 -> eq_term_store st1 (tm_deref t1) st2 (tm_deref t2)
  | eq_ts_assign : forall st1 st2 t11 t12 t21 t22,
      eq_term_store st1 t11 st2 t21 -> eq_term_store st1 t12 st2 t22 ->
      eq_term_store st1 (tm_assign t11 t12) st2 (tm_assign t21 t22)
  | eq_ts_loc : forall st1 st2 l1 l2 t1 t2, store_lookup l1 st1 = t1 -> store_lookup l2 st2 = t2 ->
      eq_term_store st1 t1 st2 t2 -> eq_term_store st1 (tm_loc l1) st2 (tm_loc l2).

Inductive resolvable : store -> tm -> seq nat -> Prop :=
  | res_var : forall st v ls, resolvable st (tm_var v) ls
  | res_app : forall st t1 t2 ls,
      resolvable st t1 ls -> resolvable st t2 ls -> resolvable st (tm_app t1 t2) ls
  | res_abs : forall st x t T ls, resolvable st t ls -> resolvable st (tm_abs x T t) ls
  | res_nat : forall st n ls, resolvable st (tm_nat n) ls
  | res_succ : forall st t ls, resolvable st t ls -> resolvable st (tm_succ t) ls
  | res_pred : forall st t ls, resolvable st t ls -> resolvable st (tm_pred t) ls
  | res_mult : forall st t1 t2 ls,
      resolvable st t1 ls -> resolvable st t2 ls -> resolvable st (tm_mult t1 t2) ls
  | res_if0 : forall st t1 t2 t3 ls, resolvable st t1 ls -> resolvable st t2 ls ->
      resolvable st t3 ls -> resolvable st (tm_if0 t1 t2 t3) ls
  | res_unit : forall st ls, resolvable st tm_unit ls
  | res_ref : forall st t ls, resolvable st t ls -> resolvable st (tm_ref t) ls
  | res_deref : forall st t ls, resolvable st t ls -> resolvable st (tm_deref t) ls
  | res_assign : forall st t1 t2 ls,
      resolvable st t1 ls -> resolvable st t2 ls -> resolvable st (tm_mult t1 t2) ls
  | res_loc : forall st t l ls,
      l \in ls -> store_lookup l st = t -> resolvable st t ls -> resolvable st (tm_loc l) ls.

Inductive extends_ A : seq A -> seq A -> Prop :=
  | exnteds_nil_ : forall s, extends_ s [::]
  | extends_cons_ : forall x s1 s2, extends_ s1 s2 -> extends_ (x :: s1) (x :: s2).

Lemma extends_snoc_ A (s : seq A) (x : A) : extends_ (rcons s x) s.
Proof. elim: s => //= [|y s H]; by constructor. Qed.

Lemma extends_refl_ A (s : seq A) : extends_ s s.
Proof. elim: s => //= [|x s H]; by constructor. Qed.

Lemma extends_assoc_ A (s1 s2 s3 : seq A) : extends_ s2 s1 -> extends_ s3 s2 -> extends_ s3 s1.
Proof.
Admitted.  

Lemma gc_step_extends st t ls1 ls2 : gc_step_ st t ls1 ls2 -> extends_ ls2 ls1.
Proof.
  elim: t ls1 ls2.
  - move=> i ls1 ls2 H1. inversion H1; subst. apply extends_refl_.
  - move=> t1 IHt1 t2 IHt2 ls1 ls2 H1. inversion H1; subst.
Admitted.

Lemma resolvable_extends st t ls1 ls2 :
  resolvable st t ls1 -> extends_ ls2 ls1 -> resolvable st t ls2.
Proof.
Admitted.

Lemma gc_step_resolvable st t ls : gc_step st t ls -> resolvable st t ls.
Proof.
Admitted.

Lemma gc_step_resolvable_assoc st t1 t2 ls2 ls3 :
  gc_step_ st t1 [::] ls2 -> gc_step_ st t2 ls2 ls3 -> resolvable st t1 ls3.
Proof.
  move=> H1 H2.
  move: (gc_step_extends H1) => H3. move: (gc_step_extends H2) => H4.
  move: (extends_assoc_ H3 H4) => H5. move: (gc_step_resolvable H1) => H6.
  apply (resolvable_extends H6 H4).
Qed.

Lemma gc_copy_eq st1 st2 ls l1 l2 t1 t2 :
  gc_copy st1 ls st2 -> store_lookup l1 st1 = t1 -> store_lookup l2 st2 = t2 ->
  ntho ls l2 = Some l1 -> eq_term_store st1 t1 st2 t2.
Proof.
  move=> H1.
  elim: H1 l1 l2 t1 t2 => // st3 ns l3 ls3 t3 t4 st4 H1 H2 H3 IH l1 l2 t1 t2 H4 H5 H6.
Admitted.

Theorem gc_consistent st1 st2 t1 t2 ls :
  resolvable st1 t1 ls -> gc_update_term ls t1 t2 -> gc_copy st1 ls st2 ->
  eq_term_store st1 t1 st2 t2.
Proof.
  move=> H1 H2 H3. elim: t1 t2 H1 H2.
  - move=> i t2 H1 H2. inversion H2; subst. constructor.
  - move=> t11 IHt11 t12 IHt12 t2 H1 H2.
    inversion H2; subst. inversion H1; subst. constructor.
    + apply (IHt11 t21) => //.
    + apply (IHt12 t22) => //.
  - move=> i T1 t1 IHt1 t2 H1 H2.
    inversion H2; subst. inversion H1; subst. constructor. apply (IHt1 t3) => //.
  - move=> n t2 H1 H2. inversion H2; subst. constructor.
  - move=> t1 IHt1 t2 H1 H2.
    inversion H2; subst. inversion H1; subst. constructor. apply (IHt1 t3) => //.
  - move=> t1 IHt1 t2 H1 H2.
    inversion H2; subst. inversion H1; subst. constructor. apply (IHt1 t3) => //.
  - move=> t1 IHt1 t2 IHt2 t3 H1 H2.
    inversion H2; subst. inversion H1; subst.
    + constructor. apply (IHt1 t21) => //. apply (IHt2 t22) => //.
    + constructor. apply (IHt1 t21) => //. apply (IHt2 t22) => //.
  - move=> t1 IHt1 t2 IHt2 t3 IHt3 t4 H1 H2.
    inversion H2; subst. inversion H1; subst. constructor.
    + apply (IHt1 t21) => //.
    + apply (IHt2 t22) => //.
    + apply (IHt3 t23) => //.
  - move=> t2 H1 H2. inversion H2; subst. constructor.
  - move=> t1 IHt1 t2 H1 H2.
    inversion H2; subst. inversion H1; subst. constructor. apply (IHt1 t3) => //.
  - move=> t1 IHt1 t2 H1 H2.
    inversion H2; subst. inversion H1; subst. constructor. apply (IHt1 t3) => //.
  - move=> t1 IHt1 t2 IHt2 t3 H1 H2.
    inversion H2; subst. inversion H1; subst.
  - move=> n t H1 H2. inversion H2; subst.
    move H5: (store_lookup n st1) => t1. move H6: (store_lookup l2 st2) => t2.
    move: (gc_copy_eq H3 H5 H6 H4) => H7. apply (eq_ts_loc H5 H6 H7).
Qed.
