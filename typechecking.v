From mathcomp.ssreflect Require Import ssreflect ssrbool ssrfun eqtype ssrnat seq.

Load stlc.

Module STLCChecker.

Fixpoint beq_ty (T1 T2:ty) : bool :=
  match T1,T2 with
  | ty_Bool, ty_Bool => true
  | ty_arrow T11 T12, ty_arrow T21 T22 => andb (beq_ty T11 T21) (beq_ty T12 T22)
  | _, _ => false
  end.

Lemma beq_ty_refl T1 : beq_ty T1 T1 = true.
Proof.
  elim: T1 => // T1 P T2 Q. by rewrite /beq_ty -/beq_ty P Q.
Qed.

Lemma beq_ty__eq T1 T2 : beq_ty T1 T2 = true -> T1 = T2.
Proof.
  move: T2. induction T1; move=> T2 P; destruct T2; inversion P => //.
    - move/andP: H0 => [Q R]. move/IHT1_1: Q => ->. by move/IHT1_2: R => ->.
Qed.

Fixpoint type_check (Γ:context) (t:tm) : option ty :=
  match t with
  | tm_var x => Γ x
  | tm_abs x T11 t12 => match type_check (extend Γ x T11) t12 with
                          | Some T12 => Some (ty_arrow T11 T12)
                          | _ => None
                        end
  | tm_app t1 t2 => match type_check Γ t1, type_check Γ t2 with
                      | Some (ty_arrow T11 T12),Some T2 =>
                        if beq_ty T11 T2 then Some T12 else None
                      | _,_ => None
                    end
  | tm_true => Some ty_Bool
  | tm_false => Some ty_Bool
  | tm_if x t f => match type_check Γ x with
                     | Some ty_Bool =>
                       match type_check Γ t, type_check Γ f with
                         | Some T1, Some T2 =>
                           if beq_ty T1 T2 then Some T1 else None
                         | _,_ => None
                       end
                     | _ => None
                   end
  end.  

Theorem type_checking_sound Γ t T : type_check Γ t = Some T -> has_type Γ t T.
Proof.
  move: Γ T. induction t => Γ T P.
    - move Q: (type_check Γ t1) => [T1|]; move R: (type_check Γ t2) => [T2|].
      + move: (IHt1 _ _ Q) => S. move: (IHt2 _ _ R) => U. eapply T_App.
        * rewrite /type_check -/type_check in P. case T1.
        * apply U.





