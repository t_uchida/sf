From mathcomp.ssreflect Require Import ssreflect ssrbool ssrfun eqtype ssrnat seq.

Load imp.

Definition partial_function {X: Type} (R: X -> X -> Prop) :=
  forall x y1 y2 : X, R x y1 -> R x y2 -> y1 = y2.

Inductive tm : Type :=
  | tm_const : nat -> tm
  | tm_plus : tm -> tm -> tm.

Module Temp1.

Inductive step : tm -> tm -> Prop :=
  | ST_PlusConstConst n1 n2 :
      tm_plus (tm_const n1) (tm_const n2) ==> tm_const (plus n1 n2)
  | ST_Plus1 t1 t1' t2 :
      t1 ==> t1' ->
      tm_plus t1 t2 ==> tm_plus t1' t2
  | ST_Plus2 n1 t2 t2' :
      t2 ==> t2' ->
      tm_plus (tm_const n1) t2 ==> tm_plus (tm_const n1) t2'

  where " t '==>' t' " := (step t t').

Example test_step_1 :
      tm_plus
        (tm_plus (tm_const 0) (tm_const 3))
        (tm_plus (tm_const 2) (tm_const 4))
      ==>
      tm_plus
        (tm_const (plus 0 3))
        (tm_plus (tm_const 2) (tm_const 4)).
Proof.
  apply ST_Plus1. apply ST_PlusConstConst.
Qed.

Example test_step_2 :
      tm_plus
        (tm_const 0)
        (tm_plus
          (tm_const 2)
          (tm_plus (tm_const 0) (tm_const 3)))
      ==>
      tm_plus
        (tm_const 0)
        (tm_plus
          (tm_const 2)
          (tm_const (plus 0 3))).
Proof.
  apply ST_Plus2. apply ST_Plus2. apply ST_PlusConstConst.
Qed.

Theorem step_deterministic : partial_function step.
Proof.
  rewrite /partial_function => x y1 y2 Hy1 Hy2. generalize dependent y2. induction Hy1 => y2 Hy2.
    - inversion Hy2; subst => //; inversion H2.
    - inversion Hy2; subst.
      + inversion Hy1.
      + by rewrite (IHHy1 _ H2).
      + inversion Hy1.
    - inversion Hy2; subst.
      + inversion Hy1.
      + inversion H2.
      + by rewrite (IHHy1 _ H2).
Qed.

End Temp1.

Inductive value : tm -> Prop :=
  v_const : forall n, value (tm_const n).

Inductive step : tm -> tm -> Prop :=
  | ST_PlusConstConst : forall n1 n2,
          tm_plus (tm_const n1) (tm_const n2)
      ==> tm_const (plus n1 n2)
  | ST_Plus1 : forall t1 t1' t2,
        t1 ==> t1' ->
        tm_plus t1 t2 ==> tm_plus t1' t2
  | ST_Plus2 : forall v1 t2 t2',
        value v1 ->
        t2 ==> t2' ->
        tm_plus v1 t2 ==> tm_plus v1 t2'

  where " t '==>' t' " := (step t t').

Theorem step_deterministic : partial_function step.
Proof.
  rewrite /partial_function => x y1 y2 Hy1. move: y2. elim: Hy1.
    - move=> n1 n2 y2 Hy2. inversion Hy2; subst => //. inversion H2. inversion H3.
    - move=> t1 t1' t2 P Q y2 Hy2. inversion Hy2; subst.
      + inversion P.
      + by rewrite (Q _ H2).
      + inversion H1. rewrite -H in P. inversion P.
    - move=> v1 t2 t2' P Q R y2 Hy2. inversion Hy2; subst.
      + inversion Q.
      + inversion P. rewrite -H in H2. inversion H2.
      + by rewrite (R _ H3).
Qed.

Theorem strong_progress t : value t \/ (exists t', t ==> t').
Proof.
  induction t.
    - left. constructor.
    - right. inversion IHt1.
      + inversion IHt2.
        * inversion H. inversion H0. exists (tm_const (plus n n0)). constructor.
        * inversion H0 as [t3 H1]. exists (tm_plus t1 t3). by constructor.
      + inversion H as [t3 H0]. exists (tm_plus t3 t2). by constructor.
Qed.

Definition normal_form {X : Type} (R : X -> X -> Prop) (t : X) : Prop := ~ exists t', R t t'.

Lemma value_is_nf t : value t -> normal_form step t.
Proof.
  rewrite /normal_form => P. inversion P => Q. inversion Q as [t' R]. inversion R.
Qed.

Lemma nf_is_value t : normal_form step t -> value t.
Proof.
  rewrite /normal_form => P.
  have G: value t \/ exists t', t ==> t'. apply strong_progress.
  by inversion G.
Qed.

Corollary nf_same_as_value t : normal_form step t <-> value t.
Proof.
  split. apply nf_is_value. apply value_is_nf.
Qed.

Module Temp4.

Inductive tm : Type :=
  | tm_true : tm
  | tm_false : tm
  | tm_if : tm -> tm -> tm -> tm.

Inductive value : tm -> Prop :=
  | v_true : value tm_true
  | v_false : value tm_false.

Inductive step : tm -> tm -> Prop :=
  | ST_IfTrue : forall t1 t2, tm_if tm_true t1 t2 ==> t1
  | ST_IfFalse : forall t1 t2, tm_if tm_false t1 t2 ==> t2
  | ST_If : forall t1 t1' t2 t3, t1 ==> t1' -> tm_if t1 t2 t3 ==> tm_if t1' t2 t3

  where " t '==>' t' " := (step t t').

Definition bool_step_prop3 :=
     tm_if
       (tm_if tm_true tm_true tm_true)
       (tm_if tm_true tm_true tm_true)
       tm_false
   ==>
     tm_if
       tm_true
       (tm_if tm_true tm_true tm_true)
       tm_false.

Theorem bool_step_prop3_holds : bool_step_prop3.
  rewrite /bool_step_prop3. apply ST_If. apply ST_IfTrue.
Qed.

Theorem strong_progress t : value t \/ (exists t', t ==> t').
Proof.  
  induction t.
    - left. constructor.
    - left. constructor.
    - case IHt1 => P.
      + inversion P; right; [exists t2|exists t3]; constructor.
      + case P => t1' Q. right. exists (tm_if t1' t2 t3). by constructor.
Qed.

End Temp4.

Definition relation x := x -> x -> Prop.

Inductive refl_step_closure (X:Type) (R: relation X) 
                            : X -> X -> Prop :=
  | rsc_refl : forall (x : X),
                 refl_step_closure X R x x
  | rsc_step : forall (x y z : X),
                    R x y ->
                    refl_step_closure X R y z ->
                    refl_step_closure X R x z.
Implicit Arguments refl_step_closure [[X]].

Theorem rsc_trans (X : Type) (R : relation X) (x y z : X) :
      refl_step_closure R x y ->
      refl_step_closure R y z ->
      refl_step_closure R x z.
Proof.
  move=> P Q. induction P => //. eapply rsc_step. apply H. apply (IHP Q).
Qed.

Theorem rsc_R (X:Type) (R:relation X) (x y : X) : R x y -> refl_step_closure R x y.
Proof.
  intros r.
  apply rsc_step with y. apply r. apply rsc_refl. Qed.

Definition stepmany := refl_step_closure step.

Notation " t '==>*' t' " := (stepmany t t') (at level 40).

Lemma test_stepmany_1':
      tm_plus
        (tm_plus (tm_const 0) (tm_const 3))
        (tm_plus (tm_const 2) (tm_const 4))
  ==>*
      tm_const (plus (plus 0 3) (plus 2 4)).
Proof.
  eapply rsc_step. apply ST_Plus1. apply ST_PlusConstConst.
  eapply rsc_step. apply ST_Plus2. apply v_const.
  apply ST_PlusConstConst.
  eapply rsc_step. apply ST_PlusConstConst.
  apply rsc_refl.
Qed.

Lemma test_stepmany_2: tm_const 3 ==>* tm_const 3.
Proof.
  apply rsc_refl.
Qed.

Lemma test_stepmany_3: tm_plus (tm_const 0) (tm_const 3) ==>* tm_plus (tm_const 0) (tm_const 3).
Proof.
  apply rsc_refl.
Qed.

Lemma test_stepmany_4:
  tm_plus
    (tm_const 0)
    (tm_plus
      (tm_const 2)
      (tm_plus (tm_const 0) (tm_const 3)))
  ==>*
  tm_plus
    (tm_const 0)
    (tm_const (plus 2 (plus 0 3))).
Proof.
  eapply rsc_step.
    - apply ST_Plus2. constructor. eapply ST_Plus2. constructor. constructor.
    - eapply rsc_step.
      + apply ST_Plus2. constructor. apply ST_PlusConstConst.
      + apply rsc_refl.
Qed.

Definition step_normal_form := normal_form step.

Definition normal_form_of (t t' : tm) :=
  (t ==>* t' /\ step_normal_form t').

Theorem normal_forms_unique: partial_function normal_form_of.
Proof.
  rewrite /partial_function /normal_form_of /step_normal_form /normal_form => x y1 y2 [P Q] [R S].
  induction P; induction R => //.
    - contradict Q; by exists y.
    - contradict S; by exists y.
    - apply IHP.
      + apply Q.
      + rewrite (step_deterministic x y y0 H H0). apply R.
Qed.

Lemma stepmany_congr_1 t1 t1' t2 :
  t1 ==>* t1' -> tm_plus t1 t2 ==>* tm_plus t1' t2.
Proof.
  move=> H. induction H.
    - constructor.
    - eapply rsc_step.
      + apply ST_Plus1. apply H.
      + assumption.
Qed.

Lemma stepmany_congr_2 t1 t2 t2' :
  value t1 -> t2 ==>* t2' -> tm_plus t1 t2 ==>* tm_plus t1 t2'.
Proof.
  move=> P Q. induction Q.
    - constructor.
    - eapply rsc_step.
      + apply ST_Plus2 => //. apply H.
      + assumption.
Qed.

Definition normalizing {X : Type} (R : X -> X -> Prop) :=
  forall t, exists t', (refl_step_closure R) t t' /\ normal_form R t'.

Theorem step_normalizing : normalizing step.
Proof.
  rewrite /normalizing => t. induction t.
    - exists (tm_const n). split.
      + constructor.
      + rewrite nf_same_as_value. constructor.
    - case IHt1 => t1' H1. case IHt2 => t2' H2.
      move: H1 H2. rewrite 2!nf_same_as_value. move=> [H11 H12] [H21 H22].
      inversion H12 as [n1]; subst. inversion H22 as [n2]; subst.
      exists (tm_const (plus n1 n2)). split.
      + apply rsc_trans with (tm_plus (tm_const n1) t2).
        apply stepmany_congr_1. apply H11.
        apply rsc_trans with (tm_plus (tm_const n1) (tm_const n2)).
        apply stepmany_congr_2. constructor. apply H21.
        apply rsc_R. apply ST_PlusConstConst.
      + rewrite nf_same_as_value. constructor.
Qed.

Inductive eval : tm -> tm -> Prop :=
  | E_Const : forall n1, tm_const n1 || tm_const n1
  | E_Plus : forall t1 n1 t2 n2,
      t1 || tm_const n1 -> t2 || tm_const n2 -> tm_plus t1 t2 || tm_const (plus n1 n2)

  where " t '||' t' " := (eval t t').

Lemma eval__value t1 t2 : eval t1 t2 -> value t2.
Proof.
  move=> P. induction P; constructor.
Qed.

Theorem eval__stepmany t v : t || v -> t ==>* v.
Proof.
  move=> P. induction P.
    - constructor.
    - eapply rsc_trans.
      + apply stepmany_congr_1. apply IHP1.
      + eapply rsc_trans.
        * apply stepmany_congr_2. constructor. apply IHP2.
        * apply rsc_R. constructor.
Qed.

Theorem step__eval' t t' n : t ==> t' -> t' || tm_const n -> t || tm_const n.
  move: t' n. induction t => t' n' P; inversion P; subst; move=> Q; inversion Q; subst.
    - constructor; constructor.
    - constructor => //. apply (IHt1 _ _ H2 H3).
    - constructor => //. apply (IHt2 _ _ H3 H5).
Qed.

Theorem step__eval t t' v : t ==> t' -> t' || v -> t || v.
Proof.
  move=> P Q. induction Q; inversion P; subst.
    - constructor; constructor.
    - constructor => //. eapply step__eval'. apply H1. apply Q1.
    - constructor => //. eapply step__eval'. apply H3. apply Q2.
Qed.

Theorem stepmany__eval t v : normal_form_of t v -> t || v.
Proof.
  rewrite /normal_form_of /step_normal_form nf_same_as_value. move=> [P Q].
  inversion Q. induction P.
    - rewrite -H. apply E_Const.
    - eapply step__eval. apply H0. by apply IHP.
Qed.

Corollary stepmany_iff_eval t v : normal_form_of t v <-> t || v.
Proof.
  split.
    - apply stepmany__eval.
    - rewrite /normal_form_of /step_normal_form nf_same_as_value. move=> P. split.
      + by apply eval__stepmany.
      + eapply eval__value. apply P.
Qed.
