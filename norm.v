From mathcomp.ssreflect Require Import ssreflect ssrbool ssrfun eqtype ssrnat seq.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Inductive id : Type := Id : nat -> id.

Definition beq_id id1 id2 :=
  match (id1, id2) with
    (Id n1, Id n2) => n1 == n2
  end.

Lemma beq_idP : Equality.axiom beq_id.
  rewrite /beq_id /Equality.axiom => x y. case x; case y => nx ny. case P: (ny == nx).
    - constructor. by move/eqnP: P => -> .
    - constructor. move/eqnP: P => P Q. apply P. by inversion Q.
Qed.

Canonical id_eqMixin := EqMixin beq_idP.
Canonical id_eqType := Eval hnf in EqType id id_eqMixin.

Inductive ty : Type :=
  | TyBool : ty
  | TyArrow : ty -> ty -> ty
  | TyProd : ty -> ty -> ty.

Inductive tm : Type :=
  | TmVar : id -> tm
  | TmApp : tm -> tm -> tm
  | TmAbs : id -> ty -> tm -> tm
  | TmPair : tm -> tm -> tm
  | TmFst : tm -> tm
  | TmSnd : tm -> tm
  | TmTrue : tm
  | TmFalse : tm
  | TmIf : tm -> tm -> tm -> tm.

Fixpoint subst (x : id) (s : tm) (t : tm) : tm :=
  match t with
  | TmVar y => if x == y then s else t
  | TmAbs y T t1 => TmAbs y T (if x == y then t1 else (subst x s t1))
  | TmApp t1 t2 => TmApp (subst x s t1) (subst x s t2)
  | TmPair t1 t2 => TmPair (subst x s t1) (subst x s t2)
  | TmFst t1 => TmFst (subst x s t1)
  | TmSnd t1 => TmSnd (subst x s t1)
  | TmTrue => TmTrue
  | TmFalse => TmFalse
  | TmIf t0 t1 t2 => TmIf (subst x s t0) (subst x s t1) (subst x s t2)
  end.

Inductive value : tm -> Prop :=
  | VAbs : forall x T11 t12, value (TmAbs x T11 t12)
  | VPair : forall v1 v2, value v1 -> value v2 -> value (TmPair v1 v2)
  | VTrue : value TmTrue
  | VFalse : value TmFalse.

Hint Constructors value.

Inductive step : tm -> tm -> Prop :=
  | ST_AppAbs : forall x T11 t12 v2, value v2 -> TmApp (TmAbs x T11 t12) v2 ==> subst x v2 t12
  | ST_App1 : forall t1 t1' t2, t1 ==> t1' -> TmApp t1 t2 ==> TmApp t1' t2
  | ST_App2 : forall v1 t2 t2', value v1 -> t2 ==> t2' -> TmApp v1 t2 ==> TmApp v1 t2'

  | ST_Pair1 : forall t1 t1' t2, t1 ==> t1' -> TmPair t1 t2 ==> TmPair t1' t2
  | ST_Pair2 : forall v1 t2 t2', value v1 -> t2 ==> t2' -> TmPair v1 t2 ==> TmPair v1 t2'
  | ST_Fst : forall t1 t1', t1 ==> t1' -> TmFst t1 ==> TmFst t1'
  | ST_FstPair : forall v1 v2, value v1 -> value v2 -> TmFst (TmPair v1 v2) ==> v1
  | ST_Snd : forall t1 t1', t1 ==> t1' -> TmSnd t1 ==> TmSnd t1'
  | ST_SndPair : forall v1 v2, value v1 -> value v2 -> TmSnd (TmPair v1 v2) ==> v2
  
  | ST_IfTrue : forall t1 t2, TmIf TmTrue t1 t2 ==> t1
  | ST_IfFalse : forall t1 t2, TmIf TmFalse t1 t2 ==> t2
  | ST_If : forall t0 t0' t1 t2, t0 ==> t0' -> TmIf t0 t1 t2 ==> TmIf t0' t1 t2

where "t1 '==>' t2" := (step t1 t2).

Definition relation (X:Type) := X -> X -> Prop.

Inductive refl_step_closure (X:Type) (R: relation X) 
                            : X -> X -> Prop :=
  | rsc_refl : forall (x : X),
                 refl_step_closure R x x
  | rsc_step : forall (x y z : X),
                    R x y ->
                    refl_step_closure R y z ->
                    refl_step_closure R x z.

Notation stepmany := (refl_step_closure step).
Notation "t1 '==>*' t2" := (stepmany t1 t2) (at level 40).

Hint Constructors step.

Definition normal_form {X:Type} (R:relation X) (t:X) : Prop :=
  ~ exists t', R t t'.

Notation step_normal_form := (normal_form step).

Lemma value__normal : forall t, value t -> step_normal_form t.
Proof with eauto.
  intros t H; induction H; intros [t' ST]; inversion ST...
Qed.

Definition partial_map (A:Type) := id -> option A.

Definition context := partial_map ty.

Definition extend {A:Type} (Γ : partial_map A) (x:id) (T : A) :=
  fun x' => if x == x' then Some T else Γ x'.

Inductive has_type : context -> tm -> ty -> Prop :=
  | TVar : forall Γ x T, Γ x = Some T -> has_type Γ (TmVar x) T
  | TAbs : forall Γ x T11 T12 t12, has_type (extend Γ x T11) t12 T12 -> has_type Γ (TmAbs x T11 t12) (TyArrow T11 T12)
  | TApp : forall T1 T2 Γ t1 t2, has_type Γ t1 (TyArrow T1 T2) -> has_type Γ t2 T1 -> has_type Γ (TmApp t1 t2) T2

  | TPair : forall Γ t1 t2 T1 T2, has_type Γ t1 T1 -> has_type Γ t2 T2 -> has_type Γ (TmPair t1 t2) (TyProd T1 T2)
  | TFst : forall Γ t T1 T2, has_type Γ t (TyProd T1 T2) -> has_type Γ (TmFst t) T1
  | TSnd : forall Γ t T1 T2, has_type Γ t (TyProd T1 T2) -> has_type Γ (TmSnd t) T2

  | TTrue : forall Γ, has_type Γ TmTrue TyBool
  | TFalse : forall Γ, has_type Γ TmFalse TyBool
  | TIf : forall Γ t0 t1 t2 T, has_type Γ t0 TyBool -> has_type Γ t1 T -> has_type Γ t2 T -> has_type Γ (TmIf t0 t1 t2) T.

Hint Constructors has_type.

Inductive appears_free_in : id -> tm -> Prop :=
  | AVar : forall x, appears_free_in x (TmVar x)
  | AApp1 : forall x t1 t2, appears_free_in x t1 -> appears_free_in x (TmApp t1 t2)
  | AApp2 : forall x t1 t2, appears_free_in x t2 -> appears_free_in x (TmApp t1 t2)
  | AAbs : forall x y T11 t12, y <> x -> appears_free_in x t12 -> appears_free_in x (TmAbs y T11 t12)

  | APair1 : forall x t1 t2, appears_free_in x t1 -> appears_free_in x (TmPair t1 t2)
  | APair2 : forall x t1 t2, appears_free_in x t2 -> appears_free_in x (TmPair t1 t2)
  | AFst : forall x t, appears_free_in x t -> appears_free_in x (TmFst t)
  | ASnd : forall x t, appears_free_in x t -> appears_free_in x (TmSnd t)

  | AIf0 : forall x t0 t1 t2, appears_free_in x t0 -> appears_free_in x (TmIf t0 t1 t2)
  | AIf1 : forall x t0 t1 t2, appears_free_in x t1 -> appears_free_in x (TmIf t0 t1 t2)
  | AIf2 : forall x t0 t1 t2, appears_free_in x t2 -> appears_free_in x (TmIf t0 t1 t2).

Hint Constructors appears_free_in.

Definition closed (t:tm) := forall x, ~ appears_free_in x t.

Lemma context_invariance C C' t T :
  has_type C t T -> (forall x, appears_free_in x t -> C x = C' x) -> has_type C' t T.
Proof.
  move=> P. move: C'. elim: P => {C t T}.
  - move=> C1 x t' H1 C2 H2. apply TVar. by rewrite -H2.
  - move=> C1 x T11 T12 t12 _ IHP C2 H1. apply TAbs. apply IHP => y H2. rewrite /extend. case H3: (x == y) => //.
    apply H1. constructor => //. by move/eqP: H3.
  - move=> T1 T2 C1 t1 t2 _ IHP1 _ IHP2 C2 H1. eapply TApp.
    + apply IHP1 => x H2. apply H1. by apply AApp1.
    + apply IHP2 => x H2. apply H1. by apply AApp2.
  - move=> C1 t1 t2 T1 T2 _ IHP1 _ IHP2 C2 H1. apply TPair.
    + apply IHP1 => x H2. apply H1. by apply APair1.
    + apply IHP2 => x H2. apply H1. by apply APair2.
  - move=> C1 t0 T1 T2 _ IHP C2 H2. eapply TFst. apply IHP => x H3. apply H2. by constructor.
  - move=> C1 t0 T1 T2 _ IHP C2 H2. eapply TSnd. apply IHP => x H3. apply H2. by constructor.
  - constructor.
  - constructor.
  - move=> C1 t0 t1 t2 T0 _ IHP1 _ IHP2 _ IHP3 C2 H1. apply TIf.
    + apply IHP1 => x H2. apply H1. by apply AIf0.
    + apply IHP2 => x H2. apply H1. by apply AIf1.
    + apply IHP3 => x H2. apply H1. by apply AIf2.
Qed.

Lemma free_in_context x t T Γ :
  appears_free_in x t -> has_type Γ t T -> exists T', Γ x = Some T'.
Proof.
  move=> P Q. induction Q; inversion P; subst; try eauto.
    - move: (IHQ H4). rewrite /extend. move/eqP/negPf: H2 => H2. by rewrite H2.
Qed.

Definition empty {A:Type} : partial_map A := (fun _ => None).

Corollary typable_empty__closed t T : has_type empty t T -> closed t.
Proof.
  move=> P. rewrite /closed => x Q.
  case (free_in_context Q P) => y R. inversion R.
Qed.

Lemma substitution_preserves_typing ctx x U v t S :
     has_type (extend ctx x U) t S ->
     has_type empty v U ->
     has_type ctx (subst x v t) S.
Proof.
  move=> P Q. move: ctx S P. induction t => ctx S P.
    - inversion P; subst => /=. case R: (x == i).
      + rewrite /extend R in H1. inversion H1. rewrite H0 in Q. eapply context_invariance.
        apply Q. move=> x0 V. move: (typable_empty__closed Q V) => X. inversion X.
      + apply TVar. rewrite /extend R in H1. exact H1.
    - inversion P; subst => /=. eapply TApp.
      + apply IHt1. apply H2.
      + apply IHt2. apply H4.
    - inversion P; subst => /=. eapply TAbs. case R: (x == i).
      + eapply context_invariance. apply H4. move=> x0 S.
        rewrite /extend. case V: (i == x0) => //.
        move: V. move/eqP: R => <-. by move=> ->.
      + apply IHt. eapply context_invariance. apply H4. move=> x0 S.
        rewrite /extend. case V: (x == x0) => //.
        move: R. move/eqP: V => ->. by rewrite eq_sym => ->.
    - inversion P; subst => /=. eapply TPair.
      + apply IHt1. apply H2.
      + apply IHt2. apply H4.
    - inversion P; subst => /=. eapply TFst. apply IHt. apply H1.
    - inversion P; subst => /=. eapply TSnd. apply IHt. apply H1.
    - inversion P; subst => /=. constructor.
    - inversion P; subst => /=. constructor.
    - inversion P; subst => /=. constructor.
      + apply IHt1. apply H3.
      + apply IHt2. apply H5.
      + apply IHt3. apply H6.
Qed.

Theorem preservation t t' T : has_type empty t T -> t ==> t' -> has_type empty t' T.
Proof.
  move=> P Q. remember (@empty ty) as Gamma. move: t' Q HeqGamma.
  induction P => t' Q R; subst.
    - inversion H.
    - inversion Q.
    - inversion Q; subst.
      + inversion P1; subst. eapply substitution_preserves_typing. apply H1. apply P2.
      + eapply TApp. apply IHP1 => //. apply P2.
      + eapply TApp. apply P1. apply IHP2 => //.
    - inversion Q; subst; constructor.
      + apply IHP1 => //. apply P2.
      + apply P1. apply IHP2 => //.
    - inversion Q; subst.
      + eapply TFst. apply IHP => //.
      + inversion P; subst. apply H5.
    - inversion Q; subst.
      + eapply TSnd. apply IHP => //. 
      + inversion P; subst. apply H7.
    - inversion Q.
    - inversion Q.
    - inversion Q; subst => //. constructor => //. apply IHP1 => //.
Qed.

Definition partial_function {X: Type} (R: relation X) :=
  forall x y1 y2 : X, R x y1 -> R x y2 -> y1 = y2.

Lemma value_no_step t : value t -> forall t', ~ t ==> t'.
  move/value__normal. rewrite /step_normal_form. move=> P t' Q. apply P. by exists t'.
Qed.

Lemma step_deterministic : partial_function step.
Proof.
  rewrite /partial_function => x y1 y2 P Q.
  move: y2 Q. induction P => y2 Q.
    - inversion Q; subst => //.
      + inversion H3.
      + by move: (value_no_step H H4).
    - inversion Q; subst.
      + inversion P.
      + by rewrite (IHP _ H2).
      + by move: (value_no_step H1 P).
    - inversion Q; subst.
      + by move: (value_no_step H3 P).
      + by move: (value_no_step H H3).
      + by rewrite (IHP _ H4).
    - inversion Q; subst.
      + by rewrite (IHP _ H2).
      + by move: (value_no_step H1 P).
    - inversion Q; subst.
      + by move: (value_no_step H H3).
      + by rewrite (IHP _ H4).
    - inversion Q; subst.
      + by rewrite (IHP _ H0).
      + inversion P; subst. by move: (value_no_step H0 H4). by move: (value_no_step H1 H5).
    - inversion Q; subst => //.
      + inversion H2; subst. by move: (value_no_step H H5). by move: (value_no_step H0 H6).
    - inversion Q; subst.
      + by rewrite (IHP _ H0).
      + inversion P; subst. by move: (value_no_step H0 H4). by move: (value_no_step H1 H5).
    - inversion Q; subst => //.
      + inversion H2; subst. by move: (value_no_step H H5). by move: (value_no_step H0 H6).
    - inversion Q; subst => //.
      + inversion H3.
      + inversion Q; subst => //. inversion H3.
    - inversion Q; subst.
      + inversion P.
      + inversion P.
      + by rewrite (IHP _ H3).
Qed.

Definition halts (t:tm) : Prop := exists t', t ==>* t' /\ value t'.

Lemma value_halts v : value v -> halts v.
Proof.
  move=> P. rewrite /halts. exists v. split => //. apply rsc_refl.
Qed.

Fixpoint R (T:ty) (t:tm) {struct T} : Prop :=
  has_type empty t T /\ halts t /\
  (match T with
   | TyBool => True
   | TyArrow T1 T2 => (forall s, R T1 s -> R T2 (TmApp t s))
   | TyProd T1 T2 => False 
   end).

Lemma R_halts : forall {T} {t}, R T t -> halts t.
Proof.
  move=> T t P. destruct T; rewrite /R in P; inversion P; inversion H0; assumption.
Qed.

Lemma R_typable_empty : forall {T} {t}, R T t -> has_type empty t T.
Proof.
  move=> T t P. destruct T; rewrite /R in P; inversion P; inversion H0; assumption.
Qed.

Lemma step_preserves_halting t t' : (t ==> t') -> (halts t <-> halts t').
Proof.
  move=> P. rewrite /halts. split; move => [t'' [Q R]]; exists t''; split => //.
   - inversion Q; subst.
     + by move: (value_no_step R P).
     + by rewrite -(step_deterministic P H) in H0.
   - eapply rsc_step. apply P. apply Q.
Qed.

Lemma step_preserves_R T t t' : (t ==> t') -> R T t -> R T t'.
Proof.
  move: t t'. induction T => t t' P Q /=; inversion Q as [S U']; inversion U' as [U V].
    - split.
      + apply (preservation S P).
      + split => //. by apply (step_preserves_halting P).
    - split.
      + apply (preservation S P).
      + split.
        * by apply (step_preserves_halting P).
        * move=> s W. apply (IHT2 (TmApp t s) (TmApp t' s)).
          by constructor. by apply V.
    - split.
      + apply (preservation S P).
      + split => //.
Qed.

Lemma stepmany_preserves_R T t t' : (t ==>* t') -> R T t -> R T t'.
Proof.
  move=> P. induction P => // Q. apply IHP. eapply step_preserves_R. apply H. apply Q.
Qed.

Lemma halts_back t t' : halts t' -> t ==> t' -> halts t.
Proof.
  rewrite /halts => [[t'' [P Q]] R]. exists t''. split => //. eapply rsc_step. apply R. apply P.
Qed.

Lemma step_preserves_R' T t t' : has_type empty t T -> (t ==> t') -> R T t' -> R T t.
Proof.
  move: t t'. induction T => t t' P Q S => /=.
    - inversion S as [H1 [H2 H3]]. split => //. split => //. apply (halts_back H2 Q). 
    - inversion S as [H1 [H2 H3]]. split => //. split.
      + apply (halts_back H2 Q).
      + move=> s H4. eapply IHT2.
        * move: H4. induction T1; rewrite /R => [[H4 _]]; eapply TApp.
          apply P. apply H4. apply P. apply H4. apply P. apply H4.
        * constructor. apply Q.
        * apply (H3 _ H4).
    - inversion S as [H1 [H2 H3]]. split => //.
Qed.

Lemma stepmany_preserves_R' : forall T t t',
  has_type empty t T -> (t ==>* t') -> R T t' -> R T t.
Proof.
  intros T t t' HT STM.
  induction STM; intros.
    assumption.
    eapply step_preserves_R'. assumption. apply H. apply IHSTM.
    eapply preservation; eauto. auto.
Qed.

Definition env := list (id * tm).

Fixpoint msubst (ss:env) (t:tm) {struct ss} : tm :=
match ss with
| nil => t
| ((x,s)::ss') => msubst ss' (subst x s t)
end.

Definition tass := list (id * ty).

Fixpoint mextend (Γ : context) (xts : tass) :=
  match xts with
  | nil => Γ
  | ((x,v)::xts') => extend (mextend Γ xts') x v
  end.

Fixpoint lookup {X:Set} (k : id) (l : list (id * X)) {struct l} : option X :=
  match l with
    | nil => None
    | (j,x) :: l' =>
      if beq_id j k then Some x else lookup k l'
  end.

Fixpoint drop {X:Set} (n:id) (nxs:list (id * X)) {struct nxs} : list (id * X) :=
  match nxs with
    | nil => nil
    | ((n',x)::nxs') => if beq_id n' n then drop n nxs' else (n',x)::(drop n nxs')
  end.

Inductive instantiation : tass -> env -> Prop :=
| VNil : instantiation nil nil
| VCons : forall x T v c e, value v -> R T v -> instantiation c e -> instantiation ((x,T)::c) ((x,v)::e).

Lemma vacuous_substitution t x : ~ appears_free_in x t -> forall t', subst x t' t = t.
Proof.
  induction t => P t' //=.
    - case Q: (x == i) => //.
      + move/eqP :Q => Q. rewrite Q in P. by move: (P (AVar i)).
    - congr (TmApp _ _).
      + apply IHt1 => Q. apply P. by apply AApp1.
      + apply IHt2 => Q. apply P. by apply AApp2.
    - case Q: (x == i) => //.
      + congr (TmAbs _ _ _). apply IHt => R. apply P. constructor => //.
        rewrite eq_sym in Q. by move: (elimF eqP Q).
    - congr (TmPair _ _).
      + apply IHt1 => Q. apply P. by apply APair1.
      + apply IHt2 => Q. apply P. by apply APair2.
    - congr (TmFst _). apply IHt => Q. apply P. by constructor.
    - congr (TmSnd _). apply IHt => Q. apply P. by constructor.
    - congr (TmIf _ _ _).
      + apply IHt1 => Q. apply P. by apply AIf0.
      + apply IHt2 => Q. apply P. by apply AIf1.
      + apply IHt3 => Q. apply P. by apply AIf2.
Qed.

Lemma subst_closed: forall t,
     closed t ->
     forall x t', subst x t' t = t.
Proof.
  intros. apply vacuous_substitution. apply H. Qed.

Lemma subst_not_afi t x v : closed v -> ~ appears_free_in x (subst x v t).
Proof.
  move: x v. rewrite /closed. induction t => /= x v P Q; apply (P x).
    - case R: (x == i); rewrite R in Q => //.
      + inversion Q; subst. (* move/eqP :R0 => //. *) admit.
    - inversion Q; subst.
      + elim (IHt1 x v P H1).
      + elim (IHt2 x v P H1).
    - case R: (x == i); rewrite R in Q.
      + move/eqP :R => R. rewrite R in Q. inversion Q; subst. by elim: H2.
      + inversion Q; subst. elim (IHt x v P H4).
    - inversion Q; subst.
      + elim (IHt1 x v P H1).
      + elim (IHt2 x v P H1).
    - inversion Q; subst. elim (IHt x v P H1).
    - inversion Q; subst. elim (IHt x v P H1).
    - inversion Q.
    - inversion Q.
    - inversion Q; subst.
      + elim (IHt1 x v P H1).
      + elim (IHt2 x v P H1).
      + elim (IHt3 x v P H1).
Admitted.

Lemma duplicate_subst t' x t v :
  closed v -> subst x t (subst x v t') = subst x v t'.
Proof.
  intros. eapply vacuous_substitution. apply subst_not_afi. auto.
Qed.

Lemma swap_subst t x x1 v v1 :
    x <> x1 -> closed v -> closed v1 ->
    subst x1 v1 (subst x v t) = subst x v (subst x1 v1 t).
Proof.
  move: x x1 v v1. induction t => x x1 v v1 P Q R /=.
    - case S: (x == i).
      + move/eqP :S => <-. rewrite eq_sym. move/eqP/negbTE :P => -> /=. rewrite eq_refl. 
        apply (vacuous_substitution (Q _)).
      + case U: (x1 == i) => /=.
        * move/eqP :U => ->. rewrite eq_refl. symmetry. apply (vacuous_substitution (R _)).
        * by rewrite S U.
    - congr (TmApp _ _). by apply IHt1. by apply IHt2.


