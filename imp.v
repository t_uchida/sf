From mathcomp.ssreflect Require Import ssreflect ssrbool ssrfun eqtype ssrnat seq.

Reserved Notation "e '||' n" (at level 50, left associativity).
(*
Inductive aexp : Type :=
  | ANum : nat -> aexp
  | APlus : aexp -> aexp -> aexp
  | AMinus : aexp -> aexp -> aexp
  | AMult : aexp -> aexp -> aexp.

Inductive bexp : Type :=
  | BTrue : bexp
  | BFalse : bexp
  | BEq : aexp -> aexp -> bexp
  | BLe : aexp -> aexp -> bexp
  | BNot : bexp -> bexp
  | BAnd : bexp -> bexp -> bexp.

Fixpoint aeval (e : aexp) : nat :=
  match e with
  | ANum n => n
  | APlus a1 a2 => (aeval a1) + (aeval a2)
  | AMinus a1 a2 => (aeval a1) - (aeval a2)
  | AMult a1 a2 => (aeval a1) * (aeval a2)
  end.

Inductive aevalR : aexp -> nat -> Prop :=
  | E_ANum : forall (n : nat), (ANum n) || n
  | E_APlus : forall (e1 e2 : aexp) (n1 n2 : nat),
      (e1 || n1) -> (e2 || n2) -> (APlus e1 e2) || (n1 + n2)
  | E_AMinus : forall (e1 e2 : aexp) (n1 n2 : nat),
      (e1 || n1) -> (e2 || n2) -> (AMinus e1 e2) || (n1 - n2)
  | E_AMult : forall (e1 e2 : aexp) (n1 n2 : nat),
      (e1 || n1) -> (e2 || n2) -> (AMult e1 e2) || (n1 * n2)

  where "e '||' n" := (aevalR e n) : type_scope.
*)
Inductive id : Type := Id : nat -> id.

Definition beq_id X1 X2 :=
  match (X1, X2) with
    (Id n1, Id n2) => n1 == n2
  end.

Theorem beq_id_refl X : beq_id X X.
  case X => n. rewrite /beq_id. by apply/eqnP.
Qed.

Theorem beq_id_eq i1 i2 : beq_id i1 i2 -> i1 = i2.
  case i1; case i2 => m n. rewrite /beq_id. by move/eqnP ->.
Qed.

Theorem beq_id_false_not_eq i1 i2 : ~~ beq_id i1 i2 -> i1 <> i2.
  case i1; case i2 => m n. rewrite /beq_id. move/eqnP => P Q. apply P. by case: Q.
Qed.

Theorem not_eq_beq_id_false i1 i2 : i1 <> i2 -> beq_id i1 i2 = false.
  case i1; case i2 => m n P. apply/negP. rewrite /beq_id. move/eqnP => Q. apply P. by rewrite Q.
Qed.

Theorem beq_id_sym  i1 i2 : beq_id i1 i2 = beq_id i2 i1.
  case i1; case i2 => m n. rewrite /beq_id. apply eq_sym.
Qed.

Definition state := id -> nat.

Definition empty_state : state := fun _ => 0.

Definition update (st : state) (X:id) (n : nat) : state :=
  fun X' => if beq_id X X' then n else st X'.

Theorem update_eq n X st : (update st X n) X = n.
  by rewrite /update beq_id_refl.
Qed.

Theorem update_neq V2 V1 n st : beq_id V2 V1 = false -> (update st V2 n) V1 = (st V1).
 rewrite /update => P. by rewrite P.
Qed.

Theorem not_eq_id m n : m <> n -> Id m <> Id n.
  move=> P Q. apply P. by case: Q.
Qed.

Example update_example n : (update empty_state (Id 2) n) (Id 3) = 0.
  apply update_neq. apply not_eq_beq_id_false. by apply not_eq_id.
Qed.

Theorem update_shadow x1 x2 k1 k2 f : (update (update f k2 x1) k2 x2) k1 = (update f k2 x2) k1.
  case P: (beq_id k1 k2).
    - move: P => /beq_id_eq P. by rewrite P 2!update_eq.
    - rewrite beq_id_sym in P. by rewrite 3!(update_neq _ _ _ _ P).
Qed.

Inductive aexp : Type :=
  | ANum : nat -> aexp
  | AId : id -> aexp 
  | APlus : aexp -> aexp -> aexp
  | AMinus : aexp -> aexp -> aexp
  | AMult : aexp -> aexp -> aexp.

Definition X : id := Id 0.
Definition Y : id := Id 1.
Definition Z : id := Id 2.

Inductive bexp : Type :=
  | BTrue : bexp
  | BFalse : bexp
  | BEq : aexp -> aexp -> bexp
  | BLe : aexp -> aexp -> bexp
  | BNot : bexp -> bexp
  | BAnd : bexp -> bexp -> bexp.

Fixpoint aeval (st : state) (e : aexp) : nat :=
  match e with
  | ANum n => n
  | AId X => st X 
  | APlus a1 a2 => (aeval st a1) + (aeval st a2)
  | AMinus a1 a2 => (aeval st a1) - (aeval st a2)
  | AMult a1 a2 => (aeval st a1) * (aeval st a2)
  end.

Fixpoint beval (st : state) (e : bexp) : bool :=
  match e with
  | BTrue => true
  | BFalse => false
  | BEq a1 a2 => aeval st a1 == aeval st a2
  | BLe a1 a2 => aeval st a1 <= aeval st a2
  | BNot b1 => ~~ (beval st b1)
  | BAnd b1 b2 => beval st b1 && beval st b2
  end.

Example aexp1 :
  aeval (update empty_state X 5)
        (APlus (ANum 3) (AMult (AId X) (ANum 2)))
  = 13.
Proof. reflexivity. Qed.

Example bexp1 :
  beval (update empty_state X 5)
        (BAnd BTrue (BNot (BLe (AId X) (ANum 4))))
  = true.
Proof. reflexivity. Qed.

Inductive com : Type :=
  | CSkip : com
  | CAss : id -> aexp -> com
  | CSeq : com -> com -> com
  | CIf : bexp -> com -> com -> com
  | CWhile : bexp -> com -> com.

Notation "'SKIP'" := CSkip.
Notation "X '::=' a" := (CAss X a) (at level 60).
Notation "c1 ; c2" := (CSeq c1 c2) (at level 80, right associativity).
Notation "'WHILE' b 'DO' c 'END'" := (CWhile b c) (at level 80, right associativity).
Notation "'IFB' e1 'THEN' e2 'ELSE' e3 'FI'" := (CIf e1 e2 e3) (at level 80, right associativity).

Definition fact_in_coq : com :=
  Z ::= AId X;
  Y ::= ANum 1;
  WHILE BNot (BEq (AId Z) (ANum 0)) DO
    Y ::= AMult (AId Y) (AId Z);
    Z ::= AMinus (AId Z) (ANum 1)
  END.

Definition plus2 : com :=
  X ::= (APlus (AId X) (ANum 2)).

Definition XtimesYinZ : com :=
  Z ::= (AMult (AId X) (AId Y)).

Definition subtract_slowly_body : com :=
  Z ::= AMinus (AId Z) (ANum 1) ;
  X ::= AMinus (AId X) (ANum 1).

Definition subtract_slowly : com :=
  WHILE BNot (BEq (AId X) (ANum 0)) DO
    subtract_slowly_body
  END.

Definition subtract_3_from_5_slowly : com :=
  X ::= ANum 3 ;
  Z ::= ANum 5 ;
  subtract_slowly.

Definition loop : com :=
  WHILE BTrue DO
    SKIP
  END.

Definition fact_body : com :=
  Y ::= AMult (AId Y) (AId Z) ;
  Z ::= AMinus (AId Z) (ANum 1).

Definition fact_loop : com :=
  WHILE BNot (BEq (AId Z) (ANum 0)) DO
    fact_body
  END.

Definition fact_com : com :=
  Z ::= AId X ;
  Y ::= ANum 1 ;
  fact_loop.

Fixpoint ceval_step1 (st : state) (c : com) : state :=
  match c with
    | SKIP => st
    | l ::= a1 => update st l (aeval st a1)
    | c1 ; c2 => let st' := ceval_step1 st c1 in ceval_step1 st' c2
    | IFB b THEN c1 ELSE c2 FI =>
        if (beval st b)
          then ceval_step1 st c1
          else ceval_step1 st c2
    | WHILE b1 DO c1 END => st 
  end.

Fixpoint ceval_step2 (st : state) (c : com) (i : nat) : state :=
  match i with
  | O => empty_state
  | S i' =>
    match c with
      | SKIP => st
      | l ::= a1 => update st l (aeval st a1)
      | c1 ; c2 => let st' := ceval_step2 st c1 i' in ceval_step2 st' c2 i'
      | IFB b THEN c1 ELSE c2 FI =>
          if (beval st b)
            then ceval_step2 st c1 i'
            else ceval_step2 st c2 i'
      | WHILE b1 DO c1 END =>
          if (beval st b1)
          then let st' := ceval_step2 st c1 i' in
               ceval_step2 st' c i'
          else st
    end
  end.

Fixpoint ceval_step3 (st : state) (c : com) (i : nat)
                    : option state :=
  match i with
  | O => None
  | S i' =>
    match c with
      | SKIP => Some st
      | l ::= a1 => Some (update st l (aeval st a1))
      | c1 ; c2 =>
          match (ceval_step3 st c1 i') with
          | Some st' => ceval_step3 st' c2 i'
          | None => None
          end
      | IFB b THEN c1 ELSE c2 FI =>
          if (beval st b)
            then ceval_step3 st c1 i'
            else ceval_step3 st c2 i'
      | WHILE b1 DO c1 END =>
          if (beval st b1)
          then match (ceval_step3 st c1 i') with
               | Some st' => ceval_step3 st' c i'
               | None => None
               end
          else Some st
    end
  end.

Notation "'LETOPT' x <== e1 'IN' e2"
   := (match e1 with
         | Some x => e2
         | None => None
       end)
   (right associativity, at level 60).

Fixpoint ceval_step (st : state) (c : com) (i : nat)
                    : option state :=
  match i with
  | O => None
  | S i' =>
    match c with
      | SKIP => Some st
      | l ::= a1 => Some (update st l (aeval st a1))
      | c1 ; c2 => LETOPT st' <== ceval_step st c1 i' IN ceval_step st' c2 i'
      | IFB b THEN c1 ELSE c2 FI =>
          if (beval st b)
            then ceval_step st c1 i'
            else ceval_step st c2 i'
      | WHILE b1 DO c1 END =>
          if (beval st b1)
          then LETOPT st' <== ceval_step st c1 i' IN
               ceval_step st' c i'
          else Some st
    end
  end.

Definition test_ceval (st:state) (c:com) :=
  match ceval_step st c 500 with
  | None => None
  | Some st => Some (st X, st Y, st Z)
  end.

Definition pup_to_n : com :=
  Z ::= ANum 1;
  Y ::= ANum 0;
  WHILE (BLe (AId Z) (AId X)) DO (
    Y ::= APlus (AId Y) (AId Z);
    Z ::= APlus (AId Z) (ANum 1)
  ) END.

Reserved Notation "c1 '/' st '||' st'" (at level 40, st at level 39).

Inductive ceval : com -> state -> state -> Prop :=
  | E_Skip : forall st, SKIP / st || st
  | E_Ass : forall st a1 n l, aeval st a1 = n -> (l ::= a1) / st || (update st l n)
  | E_Seq : forall c1 c2 st st' st'',
      c1 / st || st' -> c2 / st' || st'' -> (c1 ; c2) / st || st''
  | E_IfTrue : forall st st' b1 c1 c2,
      beval st b1 = true -> c1 / st || st' -> (IFB b1 THEN c1 ELSE c2 FI) / st || st'
  | E_IfFalse : forall st st' b1 c1 c2,
      beval st b1 = false -> c2 / st || st' -> (IFB b1 THEN c1 ELSE c2 FI) / st || st'
  | E_WhileEnd : forall b1 st c1,
      beval st b1 = false -> (WHILE b1 DO c1 END) / st || st
  | E_WhileLoop : forall st st' st'' b1 c1,
      beval st b1 = true -> c1 / st || st' -> (WHILE b1 DO c1 END) / st' || st'' ->
      (WHILE b1 DO c1 END) / st || st''
  where "c1 '/' st '||' st'" := (ceval c1 st st').

Example ceval_example1:
    (X ::= ANum 2;
     IFB BLe (AId X) (ANum 1)
       THEN Y ::= ANum 3
       ELSE Z ::= ANum 4
     FI)
   / empty_state
   || (update (update empty_state X 2) Z 4).
Proof.
  apply E_Seq with (update empty_state X 2).
    - by apply E_Ass.
    - apply E_IfFalse => //. by apply E_Ass.
Qed.

Example ceval_example2:
    (X ::= ANum 0; Y ::= ANum 1; Z ::= ANum 2)
    / empty_state
    || (update (update (update empty_state X 0) Y 1) Z 2).
Proof.
  apply E_Seq with (update empty_state X 0). by constructor.
  apply E_Seq with (update (update empty_state X 0) Y 1); by constructor.
Qed.
