From mathcomp.ssreflect Require Import ssreflect ssrbool ssrfun eqtype ssrnat seq.

Definition sillyfun1 (n : nat) : bool :=
  if n == 3 then true
  else if n == 5 then true
  else false.

Theorem sillyfun1_odd_FAILED n : sillyfun1 n = true -> odd n = true.
Proof.
  rewrite /sillyfun1. elim P: (n == 3) => Q.
    - move: P => /eqnP -> //.
    - elim R: (n == 5).
      + move: R => /eqnP -> //.
      + by rewrite R in Q.
Qed.

Definition override {X : Type} (f : nat -> X) (k : nat) (x : X) : nat -> X :=
  fun (k' : nat) => if k == k' then x else f k'.

Theorem override_same {X : Type} x1 k1 k2 (f : nat -> X) :
  f k1 = x1 -> (override f k1 x1) k2 = f k2.
Proof.
  move=> P. rewrite /override. elim Q: (k1 == k2) => //.
    - rewrite -P. move: Q => /eqnP -> //.
Qed.

Theorem filter_exercise (X : Type) (test : X -> bool) (x : X) (l lf : list X) :
  filter test l = x :: lf -> test x = true.
Proof.
  elim: l => //= a l Q. elim P: (test a) => R.
    - by inversion R; subst.
    - apply (Q R).
Qed.


