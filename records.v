From mathcomp.ssreflect Require Import ssreflect ssrbool ssrfun eqtype ssrnat seq.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Inductive id : Type := Id : nat -> id.

Definition beq_id id1 id2 :=
  match (id1, id2) with
    (Id n1, Id n2) => n1 == n2
  end.

Lemma beq_idP : Equality.axiom beq_id.
  rewrite /beq_id /Equality.axiom => x y. case x; case y => nx ny. case P: (ny == nx).
    - constructor. by move/eqnP: P => -> .
    - constructor. move/eqnP: P => P Q. apply P. by inversion Q.
Qed.

Canonical id_eqMixin := EqMixin beq_idP.
Canonical id_eqType := Eval hnf in EqType id id_eqMixin.

Inductive ty : Type :=
  | TyBase : id -> ty
  | TyArrow : ty -> ty -> ty
  | TyRnil : ty
  | TyRcons : id -> ty -> ty -> ty.

Inductive tm : Type :=
  | TmVar : id -> tm
  | TmApp : tm -> tm -> tm
  | TmAbs : id -> ty -> tm -> tm
  | TmProj : tm -> id -> tm
  | TmRnil : tm
  | TmRcons : id -> tm -> tm -> tm.

Notation a := (Id 0).
Notation f := (Id 1).
Notation g := (Id 2).
Notation l := (Id 3).
Notation A := (TyBase (Id 4)).
Notation B := (TyBase (Id 5)).
Notation k := (Id 6).
Notation i1 := (Id 7).
Notation i2 := (Id 8).

Inductive record_ty : ty -> Prop :=
  | RTyNil : record_ty TyRnil
  | RTyCons : forall i T1 T2, record_ty (TyRcons i T1 T2).

Inductive record_tm : tm -> Prop :=
  | RTmNil : record_tm TmRnil
  | RTmCons : forall i t1 t2, record_tm (TmRcons i t1 t2).

Inductive well_formed_ty : ty -> Prop :=
  | WfTyBase : forall i, well_formed_ty (TyBase i)
  | WfTyArrow : forall T1 T2, well_formed_ty T1 -> well_formed_ty T2 -> well_formed_ty (TyArrow T1 T2)
  | WfTyRnil : well_formed_ty TyRnil
  | WfTyRcons : forall i T1 T2, well_formed_ty T1 -> well_formed_ty T2 -> record_ty T2 -> well_formed_ty (TyRcons i T1 T2).

Hint Constructors record_ty record_tm well_formed_ty.

Fixpoint subst (x:id) (s:tm) (t:tm) : tm :=
  match t with
  | TmVar y => if x == y then s else t
  | TmAbs y T t1 => TmAbs y T (if x == y then t1 else (subst x s t1))
  | TmApp t1 t2 => TmApp (subst x s t1) (subst x s t2)
  | TmProj t1 i => TmProj (subst x s t1) i
  | TmRnil => TmRnil
  | TmRcons i t1 tr1 => TmRcons i (subst x s t1) (subst x s tr1)
  end.

Inductive value : tm -> Prop :=
  | VAbs : forall x T11 t12, value (TmAbs x T11 t12)
  | VRnil : value TmRnil
  | VRcons : forall i v1 vr, value v1 -> value vr -> value (TmRcons i v1 vr).

Hint Constructors value.

Fixpoint ty_lookup (i:id) (Tr:ty) : option ty :=
  match Tr with
  | TyRcons i' T Tr' => if i == i' then Some T else ty_lookup i Tr'
  | _ => None
  end.

Fixpoint tm_lookup (i:id) (tr:tm) : option tm :=
  match tr with
  | TmRcons i' t tr' => if i == i' then Some t else tm_lookup i tr'
  | _ => None
  end.

Inductive step : tm -> tm -> Prop :=
  | StAppAbs : forall x T11 t12 v2, value v2 -> TmApp (TmAbs x T11 t12) v2 ==> subst x v2 t12
  | StApp1 : forall t1 t1' t2, t1 ==> t1' -> TmApp t1 t2 ==> TmApp t1' t2
  | StApp2 : forall v1 t2 t2', value v1 -> t2 ==> t2' -> TmApp v1 t2 ==> TmApp v1 t2'
  | StProj1 : forall t1 t1' i, t1 ==> t1' -> TmProj t1 i ==> TmProj t1' i
  | StProjRcd : forall tr i vi, value tr -> tm_lookup i tr = Some vi -> TmProj tr i ==> vi
  | StRcdHead : forall i t1 t1' tr2, t1 ==> t1' -> TmRcons i t1 tr2 ==> TmRcons i t1' tr2
  | StRcdTail : forall i v1 tr2 tr2', value v1 -> tr2 ==> tr2' -> TmRcons i v1 tr2 ==> TmRcons i v1 tr2'
  where "t1 '==>' t2" := (step t1 t2).

Definition relation (X:Type) := X -> X -> Prop.

Inductive refl_step_closure (X:Type) (R: relation X) : X -> X -> Prop :=
  | RscRefl : forall (x : X), refl_step_closure R x x
  | RscStep : forall (x y z : X), R x y -> refl_step_closure R y z -> refl_step_closure R x z.

Implicit Arguments refl_step_closure [[X]].

Notation stepmany := (refl_step_closure step).
Notation "t1 '==>*' t2" := (stepmany t1 t2) (at level 40).

Hint Constructors step.

Definition partial_map (A:Type) := id -> option A.

Definition empty {A:Type} : partial_map A := (fun _ => None).

Definition context := partial_map ty.

Definition extend {A:Type} (Γ : partial_map A) (x:id) (T : A) :=
  fun x' => if x == x' then Some T else Γ x'.

Inductive has_type : context -> tm -> ty -> Prop :=
  | TVar : forall Γ x T, Γ x = Some T -> well_formed_ty T -> has_type Γ (TmVar x) T
  | TAbs : forall Γ x T11 T12 t12, well_formed_ty T11 -> has_type (extend Γ x T11) t12 T12 -> has_type Γ (TmAbs x T11 t12) (TyArrow T11 T12)
  | TApp : forall T1 T2 Γ t1 t2, has_type Γ t1 (TyArrow T1 T2) -> has_type Γ t2 T1 -> has_type Γ (TmApp t1 t2) T2
  | TProj : forall Γ i t Ti Tr, has_type Γ t Tr -> ty_lookup i Tr = Some Ti -> has_type Γ (TmProj t i) Ti
  | TRNil : forall Γ, has_type Γ TmRnil TyRnil
  | TRCons : forall Γ i t T tr Tr, has_type Γ t T -> has_type Γ tr Tr ->record_ty Tr -> record_tm tr -> has_type Γ (TmRcons i t tr) (TyRcons i T Tr).

Hint Constructors has_type.

Lemma typing_example_2 :
  has_type empty
    (TmApp (TmAbs a (TyRcons i1 (TyArrow A A)
                      (TyRcons i2 (TyArrow B B)
                       TyRnil))
              (TmProj (TmVar a) i2))
            (TmRcons i1 (TmAbs a A (TmVar a))
            (TmRcons i2 (TmAbs a B (TmVar a))
             TmRnil)))
    (TyArrow B B).
Proof.
  eapply TApp.
    - eapply TAbs.
      + eapply WfTyRcons; repeat constructor.
      + eapply TProj. eapply TVar => //. apply WfTyRcons; repeat constructor. done.
    - apply TRCons; repeat constructor.
Qed.

Example typing_nonexample :
  ~ exists T, has_type (extend empty a (TyRcons i2 (TyArrow A A) TyRnil)) (TmRcons i1 (TmAbs a B (TmVar a)) (TmVar a)) T.
Proof.
  case => T P. inversion P; subst. inversion H7.
Qed.

Example typing_nonexample_2 : forall y,
  ~ exists T,
  has_type
    (extend empty y A)
    (TmApp (TmAbs a (TyRcons i1 A TyRnil) (TmProj (TmVar a) i1))
           (TmRcons i1 (TmVar y) (TmRcons i2 (TmVar y) TmRnil)))
    T.
Proof.
  move=> y. case => T P. inversion P; subst.
  inversion H4; subst. inversion H2; subst. inversion H6.
Qed.

Lemma wf_rcd_lookup i T Ti : well_formed_ty T -> ty_lookup i T = Some Ti -> well_formed_ty Ti.
Proof.
  move=> P Q. induction T; inversion Q; subst.
    - case R: (i == i0); rewrite R in H0.
      + inversion H0; subst. by inversion P; subst.
      + inversion P; subst. by apply IHT2.
Qed.

Lemma step_preserves_record_tm : forall tr tr',
  record_tm tr ->
  tr ==> tr' ->
  record_tm tr'.
Proof.
  intros tr tr' Hrt Hstp.
  inversion Hrt; subst; inversion Hstp; subst; auto.
Qed.

Lemma has_type__wf Γ t T : has_type Γ t T -> well_formed_ty T.
Proof.
  move=> P. induction P => //.
    - by constructor.
    - by inversion IHP1.
    - eapply wf_rcd_lookup. apply IHP. apply H.
    - by constructor.
Qed.

Lemma lookup_field_in_value v T i Ti :
  value v -> has_type empty v T -> ty_lookup i T = Some Ti ->
  exists ti, tm_lookup i v = Some ti /\ has_type empty ti Ti.
Proof.
  move=> P Q R. induction Q.
    - inversion P.
    - inversion R.
    - inversion P.
    - inversion P.
    - inversion R.
    - case S: (i == i0).
      + move/eqP in S. rewrite S in R. inversion R. rewrite eq_refl in H2. inversion H2; subst.
        exists t. split => /=. by rewrite eq_refl. apply Q1.
      + inversion P; subst. simpl in R. rewrite S in R. case: (IHQ2 H5 R) => x [U V].
        exists x. split => //. simpl. by rewrite S.
Qed.

Theorem progress t T : has_type empty t T -> value t \/ exists t', t ==> t'.
Proof.
  move=> P. remember (@empty ty) as Gamma. generalize dependent HeqGamma. induction P => Q; subst.
    - inversion H.
    - left. constructor.
    - right. case: IHP2; subst => // Q.
      + case: IHP1; subst => // R.
        * inversion P1; subst; try (inversion R; subst). exists (subst x t2 t12). by constructor.
        * case: R => t1' R. exists (TmApp t1' t2). by constructor.
      + case: IHP1; subst => // R.
        * case: Q => t2' Q. exists (TmApp t1 t2'). by constructor.
        * case: R => t1' R. exists (TmApp t1' t2). by constructor.
    - right. case: IHP => // Q.
     + case: (lookup_field_in_value Q P H) => [ti [R S]]. exists ti. by constructor.
     + case: Q => t' Q. exists (TmProj t' i). by constructor.
    - by left.
    - case: IHP1 => // Q.
     + case: IHP2 => // R.
       * left. by constructor.
       * right. case: R => tr' R. exists (TmRcons i t tr'). by constructor.
     + right. case: Q => t' Q. exists (TmRcons i t' tr). by constructor.
Qed.


