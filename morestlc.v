From mathcomp.ssreflect Require Import ssreflect ssrbool ssrfun eqtype ssrnat seq.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Inductive id : Type := Id : nat -> id.

Definition beq_id id1 id2 :=
  match (id1, id2) with
    (Id n1, Id n2) => n1 == n2
  end.

Lemma beq_idP : Equality.axiom beq_id.
  rewrite /beq_id /Equality.axiom => x y. case x; case y => nx ny. case P: (ny == nx).
    - constructor. by move/eqnP: P => -> .
    - constructor. move/eqnP: P => P Q. apply P. by inversion Q.
Qed.

Canonical id_eqMixin := EqMixin beq_idP.
Canonical id_eqType := Eval hnf in EqType id id_eqMixin.

Inductive ty : Type :=
  | TyBool : ty
  | TyArrow : ty -> ty -> ty
  | TyPair : ty -> ty -> ty
  | TySum : ty -> ty -> ty.

Inductive tm : Type :=
  | TmVar : id -> tm
  | TmApp : tm -> tm -> tm
  | TmAbs : id -> ty -> tm -> tm
  | TmTrue : tm
  | TmFalse : tm
  | TmIf : tm -> tm -> tm -> tm
  | TmLet : id -> tm -> tm -> tm
  | TmPair : tm -> tm -> tm
  | TmFst : tm -> tm
  | TmSnd : tm -> tm
  | TmInl : ty -> tm -> tm
  | TmInr : ty -> tm -> tm
  | TmCase : tm -> id -> tm -> id -> tm -> tm.

Inductive value : tm -> Prop :=
  | VAbs : forall x T t, value (TmAbs x T t)
  | VTrue : value TmTrue
  | VFalse : value TmFalse
  | VPair : forall t1 t2, value t1 -> value t2 -> value (TmPair t1 t2)
  | VInl : forall T v, value v -> value (TmInl T v)
  | VInr : forall T v, value v -> value (TmInr T v).

Hint Constructors value.

Fixpoint subst (s : tm) (x : id) (t : tm) : tm :=
  match t with
    | TmVar x' => if x == x' then s else t
    | TmAbs x' T t1 => TmAbs x' T (if x == x' then t1 else (subst s x t1))
    | TmApp t1 t2 => TmApp (subst s x t1) (subst s x t2)
    | TmTrue => TmTrue
    | TmFalse => TmFalse
    | TmIf t1 t2 t3 => TmIf (subst s x t1) (subst s x t2) (subst s x t3)
    | TmLet x' t1 t2 => TmLet x' (if x == x' then t1 else (subst s x t1)) (subst s x t2)
    | TmPair t1 t2 => TmPair (subst s x t1) (subst s x t2)
    | TmFst t => TmFst (subst s x t)
    | TmSnd t => TmSnd (subst s x t)
    | TmInl T t => TmInl T (subst s x t)
    | TmInr T t => TmInr T (subst s x t)
    | TmCase t x1 t1 x2 t2 => TmCase t x1 (if x == x1 then t1 else (subst s x t1)) x2 (if x == x2 then t2 else (subst s x t2))
  end.

Inductive step : tm -> tm -> Prop :=
  | SAppAbs : forall x T t12 v2, value v2 -> (TmApp (TmAbs x T t12) v2) ==> (subst v2 x t12)
  | SApp1 : forall t1 t1' t2, t1 ==> t1' -> TmApp t1 t2 ==> TmApp t1' t2
  | SApp2 : forall v1 t2 t2', value v1 -> t2 ==> t2' -> TmApp v1 t2 ==> TmApp v1 t2'
  | SIfTrue : forall t1 t2, (TmIf TmTrue t1 t2) ==> t1
  | SIfFalse : forall t1 t2, (TmIf TmFalse t1 t2) ==> t2
  | SIf : forall t1 t1' t2 t3, t1 ==> t1' -> (TmIf t1 t2 t3) ==> (TmIf t1' t2 t3)
  | SLet1 : forall x t1 t1' t2, t1 ==> t1' -> TmLet x t1 t2 ==> TmLet x t1' t2
  | SLet2 : forall x v1 t2, value v1 -> TmLet x v1 t2 ==> subst v1 x t2
  | SPair1 : forall t1 t1' t2, t1 ==> t1' -> TmPair t1 t2 ==> TmPair t1' t2
  | SPair2 : forall v1 t2 t2', value v1 -> t2 ==> t2' -> TmPair v1 t2 ==> TmPair v1 t2'
  | SFst1 : forall t1 t1', t1 ==> t1' -> TmFst t1 ==> TmFst t1'
  | SFstPair : forall v1 v2, value v1 -> value v2 -> TmFst (TmPair v1 v2) ==> v1
  | SSnd1 : forall t1 t1', t1 ==> t1' -> TmSnd t1 ==> TmSnd t1'
  | SSndPair : forall v1 v2, value v1 -> value v2 -> TmSnd (TmPair v1 v2) ==> v2
  | SInl : forall T t1 t1', t1 ==> t1' -> TmInl T t1 ==> TmInl T t1'
  | SInr : forall T t2 t2', t2 ==> t2' -> TmInr T t2 ==> TmInr T t2'
  | SCase : forall t0 t0' x1 t1 x2 t2, t0 ==> t0' -> TmCase t0 x1 t1 x2 t2 ==> TmCase t0' x1 t1 x2 t2
  | SCaseInl : forall T v0 x1 t1 x2 t2, value v0 -> TmCase (TmInl T v0) x1 t1 x2 t2 ==> subst v0 x1 t1
  | SCaseInr : forall T v0 x1 t1 x2 t2, value v0 -> TmCase (TmInr T v0) x1 t1 x2 t2 ==> subst v0 x2 t2
  where "t1 '==>' t2" := (step t1 t2).

Definition relation (X:Type) := X -> X -> Prop.

Inductive refl_step_closure (X : Type) (R : relation X) : X -> X -> Prop :=
  | RSCRefl : forall (x : X), refl_step_closure R x x
  | RSCStep : forall (x y z : X), R x y -> refl_step_closure R y z -> refl_step_closure R x z.

Notation stepmany := (refl_step_closure step).
Notation "t1 '==>*' t2" := (stepmany t1 t2) (at level 40).

Hint Constructors step.

Definition partial_map (A : Type) := id -> option A.

Definition context := partial_map ty.

Definition extend {A:Type} (Γ : partial_map A) (x:id) (T : A) :=
  fun x' => if x == x' then Some T else Γ x'.

Definition empty {A:Type} : partial_map A := (fun _ => None).

Lemma extend_eq A (ctxt: partial_map A) x T : (extend ctxt x T) x = Some T.
Proof.
  intros. by rewrite /extend (eq_refl x). 
Qed.

Lemma extend_neq A (ctxt: partial_map A) x1 T x2 :
  x2 != x1 -> (extend ctxt x2 T) x1 = ctxt x1.
Proof.
  intros. rewrite /extend. by move/negbTE: H => ->.
Qed.

Inductive has_type : context -> tm -> ty -> Prop :=
  | HVar : forall Γ x T, Γ x = Some T -> has_type Γ (TmVar x) T
  | HAbs : forall Γ x T11 T12 t12, has_type (extend Γ x T11) t12 T12 -> has_type Γ (TmAbs x T11 t12) (TyArrow T11 T12)
  | HApp : forall T11 T12 Γ t1 t2, has_type Γ t1 (TyArrow T11 T12) -> has_type Γ t2 T11 -> has_type Γ (TmApp t1 t2) T12
  | HTrue : forall Γ, has_type Γ TmTrue TyBool
  | HFalse : forall Γ, has_type Γ TmFalse TyBool
  | HIf : forall t1 t2 t3 T Γ,
       has_type Γ t1 TyBool -> has_type Γ t2 T -> has_type Γ t3 T -> has_type Γ (TmIf t1 t2 t3) T
  | HLet : forall Γ x t1 t2 T1 T2, has_type Γ t1 T1 -> has_type (extend Γ x T1) t2 T2 -> has_type Γ (TmLet x t1 t2) T2
  | HPair : forall Γ t1 t2 T1 T2, has_type Γ t1 T1 -> has_type Γ t2 T2 -> has_type Γ (TmPair t1 t2) (TyPair T1 T2)
  | HFst : forall Γ t T1 T2, has_type Γ t (TyPair T1 T2) -> has_type Γ (TmFst t) T1
  | HSnd : forall Γ t T1 T2, has_type Γ t (TyPair T1 T2) -> has_type Γ (TmSnd t) T2
  | HInl : forall Γ t1 T1 T2, has_type Γ t1 T1 -> has_type Γ (TmInl T2 t1) (TySum T1 T2)
  | HInr : forall Γ t1 T1 T2, has_type Γ t1 T2 -> has_type Γ (TmInr T1 t1) (TySum T1 T2)
  | HCase : forall Γ t0 T T1 T2 x1 t1 x2 t2, has_type Γ t0 (TySum T1 T2) -> has_type (extend Γ x1 T1) t1 T -> has_type (extend Γ x2 T2) t2 T -> has_type Γ (TmCase t0 x1 t1 x2 t2) T.

Hint Constructors has_type.

Lemma has_type_inv_pair Γ t T1 T2 : value t -> has_type Γ t (TyPair T1 T2) -> exists t1 t2, t = TmPair t1 t2 /\ value t1 /\ value t2.
  move=> P Q. induction t; subst. inversion P. inversion P. inversion P. inversion Q. inversion Q. inversion Q. inversion P. inversion P. inversion P; subst. exists t1. exists t2. split => //. inversion P. inversion P. inversion Q. inversion Q. inversion P.
Qed.

Inductive appears_free_in : id -> tm -> Prop :=
  | AFIVar : forall x, appears_free_in x (TmVar x)
  | AFIApp1 : forall x t1 t2, appears_free_in x t1 -> appears_free_in x (TmApp t1 t2)
  | AFIApp2 : forall x t1 t2, appears_free_in x t2 -> appears_free_in x (TmApp t1 t2)
  | AFIAbs : forall x y T11 t12, y != x -> appears_free_in x t12 -> appears_free_in x (TmAbs y T11 t12)
  | AFIIf1 : forall x t1 t2
                    t3, appears_free_in x t1 -> appears_free_in x (TmIf t1 t2 t3)
  | AFIIf2 : forall x t1 t2 t3, appears_free_in x t2 -> appears_free_in x (TmIf t1 t2 t3)
  | AFIIf3 : forall x t1 t2 t3, appears_free_in x t3 -> appears_free_in x (TmIf t1 t2 t3)
  | AFILet1 : forall x y t1 t2, appears_free_in x t1 -> appears_free_in x (TmLet y t1 t2)
  | AFILet2 : forall x y t1 t2, y != x -> appears_free_in x t2 -> appears_free_in x (TmLet y t1 t2)
  | AFIPair1 : forall x t1 t2, appears_free_in x t1 -> appears_free_in x (TmPair t1 t2)
  | AFIPair2 : forall x t1 t2, appears_free_in x t2 -> appears_free_in x (TmPair t1 t2)
  | AFIFst : forall x t, appears_free_in x t -> appears_free_in x (TmFst t)
  | AFISnd : forall x t, appears_free_in x t -> appears_free_in x (TmSnd t)
  | AFIInl : forall x t T, appears_free_in x t -> appears_free_in x (TmInl T t)
  | AFIInr : forall x t T, appears_free_in x t -> appears_free_in x (TmInr T t)
  | AFICase0 : forall x y z t0 t1 t2, appears_free_in x t0 -> appears_free_in x (TmCase t0 y t1 z t2)
  | AFICase1 : forall x y z t0 t1 t2, y != x -> appears_free_in x t1 -> appears_free_in x (TmCase t0 y t1 z t2)
  | AFICase2 : forall x y z t0 t1 t2, z != x -> appears_free_in x t2 -> appears_free_in x (TmCase t0 y t1 z t2).

Hint Constructors appears_free_in.

Definition closed (t:tm) := forall x, ~ appears_free_in x t.

Lemma free_in_context x t T Γ : appears_free_in x t -> has_type Γ t T -> exists T', Γ x = Some T'.
Proof.
  move=> P. move: T Γ. induction P => T' Γ Q; try (inversion Q; subst; eauto).
    - move/IHP: H5. by rewrite extend_neq.
    - move/IHP: H6. by rewrite extend_neq.
    - move/IHP: H8. by rewrite extend_neq.
    - move/IHP: H9. by rewrite extend_neq.
Qed.

Corollary typable_empty__closed t T : has_type empty t T -> closed t.
Proof.
  rewrite /closed => P x Q. move: T P. induction Q => T' P.
    - inversion P; subst. inversion H1.
    - inversion P; subst. eapply IHQ. apply H2.
    - inversion P; subst. eapply IHQ. apply H4.
    - inversion P; subst. apply (IHQ T12). move: (free_in_context Q H5).
      rewrite extend_neq. case => T' R. inversion R. apply H.
    - inversion P; subst. eapply IHQ. apply H3.
    - inversion P; subst. eapply IHQ. apply H5.
    - inversion P; subst. eapply IHQ. apply H6.
    - inversion P; subst. eapply IHQ. apply H4.
    - inversion P; subst. eapply (IHQ T'). move: (free_in_context Q H6).
      rewrite extend_neq. case => T'' R. inversion R. apply H.
    - inversion P; subst. eapply IHQ. apply H2.
    - inversion P; subst. eapply IHQ. apply H4.
    - inversion P; subst. eapply IHQ. apply H1.
    - inversion P; subst. eapply IHQ. apply H1.
    - inversion P; subst. eapply IHQ. apply H3.
    - inversion P; subst. eapply IHQ. apply H3.
    - inversion P; subst. eapply IHQ. apply H6.
    - inversion P; subst. eapply (IHQ T'). move: (free_in_context Q H8).
      rewrite extend_neq => //. case => T'' R //. 
    - inversion P; subst. eapply (IHQ T'). move: (free_in_context Q H9).
      rewrite extend_neq => //. case => T'' R //.
Qed.

Lemma context_invariance Γ Γ' t S :
  has_type Γ t S ->
  (forall x, appears_free_in x t -> Γ x = Γ' x) ->
  has_type Γ' t S.
Proof.
  move=> P. move: Γ'. induction P => Γ' Q; try (constructor; auto).
    - by rewrite -Q.
    - apply IHP => y R. rewrite /extend. case S: (x == y) => //. apply Q.
      constructor => //. move: S => -> //.
    - eapply HApp.
      + apply IHP1 => x R. apply Q. by apply AFIApp1.
      + apply IHP2 => x R. apply Q. by apply AFIApp2.
    - eapply HLet.
      + apply IHP1 => x' R. apply Q. apply AFILet1. apply R.
      + apply IHP2 => x' R. rewrite /extend -/extend. case S: (x == x') => //.
        apply Q. apply AFILet2 => //. by move/negbT: S.
    - eapply HFst. apply IHP => x R. apply Q. by apply AFIFst.
    - eapply HSnd. apply IHP => x R. apply Q. by apply AFISnd.
    - eapply HCase.
      + apply IHP1 => x R. apply Q. by apply AFICase0.
      + apply IHP2 => x R. rewrite /extend -/extend. case S: (x1 == x) => //.
        apply Q. apply AFICase1 => //. by move/negbT: S.
      + apply IHP3 => x R. rewrite /extend -/extend. case S: (x2 == x) => //.
        apply Q. apply AFICase2 => //. by move/negbT: S.
Qed.

Lemma substitution_preserves_typing Γ x U v t T :
  has_type (extend Γ x U) t T -> has_type empty v U -> has_type Γ (subst v x t) T.
Proof.
  move=> P Q. move: Γ T P. induction t => Γ T P; inversion P; subst => //=.
    - case R: (x == i).
      + move/eqP in R. rewrite R in H1. rewrite extend_eq in H1. inversion H1; subst.
        eapply (context_invariance Q) => x R. case: (free_in_context R Q) => y S. inversion S.
      + constructor. rewrite extend_neq in H1 => //. by move/negbT in R.
    - eapply HApp.
      + apply IHt1. apply H2.
      + apply IHt2. apply H4.
    - constructor. case R: (x == i).
      + eapply context_invariance. apply H4. rewrite /extend. move=> y S. case H: (i == y) => //.
        move/eqP: R => ->. by rewrite H.
      + apply IHt. eapply context_invariance. apply H4. move=> z S. rewrite /extend.
        case H: (i == z) => //. move/eqP: H => <-. by rewrite R.
    - constructor.
      + apply IHt1. apply H3.
      + apply IHt2. apply H5.
      + apply IHt3. apply H6.
    - case H: (x == i).
      + eapply HLet.
        * eapply context_invariance => [|x' R]. apply H4. rewrite /extend.
          case S: (x == x') => //. admit.
        * apply IHt2. eapply context_invariance => [|x' R]. apply H5.
          rewrite /extend. move/eqP: H => ->. case S: (i == x') => //. admit.
      + eapply HLet.
        * apply IHt1. apply H4.
        * apply IHt2. eapply context_invariance => [|x' R]. apply H5.
          rewrite /extend. case S: (i == x'); case V: (x == x') => //.
          move: S V H. move/eqP => -> -> V. inversion V.
    - apply HPair. by apply IHt1. by apply IHt2.
    - eapply HFst. apply IHt. apply H1.
    - eapply HSnd. apply IHt. apply H1.
    - apply HInl. apply IHt. apply H3.
    - apply HInr. apply IHt. apply H3.
    - eapply HCase.
      + admit.
      + case R: (x == i).
        * eapply context_invariance => [|y V]. apply H7.
          rewrite /extend. case X: (i == y) => //. move/eqP: R => ->. by rewrite X.
        * eapply context_invariance => [|y V]. apply IHt2.
          eapply context_invariance => [|z W]. apply H7.
          rewrite /extend. case X: (i == z) => //. move/eqP: X => <-. admit.
          rewrite /extend. admit.
    - admit.
 Admitted.

Theorem preservation t t' T : has_type empty t T -> t ==> t' -> has_type empty t' T.
Proof.
  move=> P. move: t'. remember empty as ctx. induction P => t' Q; inversion Q; subst; try assumption.
    - inversion P1; subst. eapply substitution_preserves_typing. apply H1. apply P2.
    - eapply HApp. by apply IHP1. apply P2.
    - eapply HApp. apply P1. by apply IHP2.
    - apply HIf. by apply IHP1. apply P2. apply P3.
    - eapply HLet. apply IHP1 => //. apply P2.
    - eapply substitution_preserves_typing. apply P2. apply P1.
    - apply HPair. by apply IHP1. apply P2.
    - apply HPair. apply P1. by apply IHP2.
    - admit.
    - inversion P; subst. apply H5.
    - admit.
    - admit.
    - apply HInl. apply IHP => //.
    - apply HInr. apply IHP => //.
    - eapply HCase. by apply IHP1. apply P2. apply P3.
    - admit.
    - admit.
Admitted.

Theorem progress t T : has_type empty t T -> value t \/ exists t', t ==> t'.
Proof.
  move=> P. remember empty as ctx. induction P.
    - rewrite Heqctx in H. inversion H.
    - left. constructor.
    - case: IHP1 => // Q.
      + inversion Q; subst; try (inversion P1; subst).
        * case: IHP2 => // R.
          - right. exists (subst t2 x t). by constructor.
          - right. case: R => y R. exists (TmApp (TmAbs x T11 t) y). by constructor.
      + case: Q => x Q. right. exists (TmApp x t2). by constructor.
    - left. constructor.
    - left. constructor.
    - inversion P1; subst.
      + inversion H.
      + case: IHP1 => // Q. inversion Q. case: Q => x Q. right. exists (TmIf x t2 t3). by constructor.
      + right. exists t2. constructor.
      + right. exists t3. constructor.
      + case IHP1 => // Q. inversion Q. case: Q => x R. right. exists (TmIf x t2 t3). by constructor.
      + admit.
      + case: IHP1 => // Q. inversion Q. case: Q => x R. right. exists (TmIf x t2 t3). by constructor.
      + case: IHP1 => // Q. inversion Q. case: Q => x R. right. exists (TmIf x t2 t3). by constructor.
      + case: IHP1 => // Q. inversion Q. case: Q => x R. right. exists (TmIf x t2 t3). by constructor.
    - case: IHP1 => // Q.
      + right. exists (subst t1 x t2). by constructor.
      + right. elim: Q => t1' Q. exists (TmLet x t1' t2). by constructor.
    - case: IHP1 => // Q.
      + case: IHP2 => // R.
        * left. by apply VPair.
        * move: R. case => t2' R. right. exists (TmPair t1 t2'). by constructor.
      + move: Q. case => t1' Q. right. exists (TmPair t1' t2). by constructor.
    - case: IHP => // Q.
      + inversion P; subst. inversion H. inversion Q. inversion Q. inversion Q.
        inversion Q; subst. right. exists t1. by constructor. inversion Q. inversion Q. inversion Q.
      + case: Q => t' Q. right. exists (TmFst t'). by constructor.
    - case: IHP => // Q.
      + 
Admitted.

Fixpoint beq_ty (T1 T2:ty) : bool :=
  match T1,T2 with
    | TyBool, TyBool => true
    | TyArrow T11 T12, TyArrow T21 T22 => andb (beq_ty T11 T21) (beq_ty T12 T22)
    | _, _ => false
  end.

Lemma beq_ty_refl T1 : beq_ty T1 T1 = true.
Proof.
  induction T1 => //=. by rewrite IHT1_1 IHT1_2.
Qed.

Lemma beq_ty_eq T1 T2 : beq_ty T1 T2 = true -> T1 = T2.
Proof.
  move: T2. induction T1; destruct T2 => //=. move/andP => [Q R]. rewrite (IHT1_1 T2_1) => //. rewrite (IHT1_2 T2_2) => //.
Qed.

Fixpoint type_check (Γ:context) (t:tm) : option ty := match t with
  | TmVar x => Γ x
  | TmAbs x T11 t12 => match type_check (extend Γ x T11) t12 with
    | Some T12 => Some (TyArrow T11 T12)
    | _ => None
  end
  | TmApp t1 t2 => match type_check Γ t1, type_check Γ t2 with
    | Some (TyArrow T11 T12),Some T2 => if beq_ty T11 T2 then Some T12 else None
    | _,_ => None
  end
  | TmTrue => Some TyBool
  | TmFalse => Some TyBool
  | TmIf x t f => match type_check Γ x with
    | Some TyBool => match type_check Γ t, type_check Γ f with
      | Some T1, Some T2 => if beq_ty T1 T2 then Some T1 else None
      | _,_ => None
    end
  | _ => None
  end
  | TmLet x t1 t2 => match type_check Γ t1 with
    | Some T1 => type_check (extend Γ x T1) t2
    | _ => None
  end
end.

Theorem type_checking_sound Γ t T : type_check Γ t = Some T -> has_type Γ t T.
Proof.
  move: Γ T. induction t => Γ T /= P; try (inversion P; by constructor).
    - case Q: (type_check Γ t1) => [T1|]; case R: (type_check Γ t2) => [T2|].
      + case S: T1 => [|T11 T12]; rewrite Q R S in P.
        * inversion P.
        * case U: (beq_ty T11 T2); rewrite U in P.
          - inversion P; subst. move/beq_ty_eq: U => U. rewrite -U in R.
            eapply HApp. apply IHt1. apply Q. by apply IHt2.
          - inversion P.
      + case S: T1 => [|T11 T12]; rewrite Q R S in P; inversion P.
      + rewrite Q in P. inversion P.
      + rewrite Q in P. inversion P.
    - case Q: (type_check (extend Γ i t) t0) => [T1|].
      + rewrite Q in P. inversion P; subst. eapply HAbs. apply IHt. apply Q.
      + rewrite Q in P. inversion P.
    - case Q: (type_check Γ t1) => [T1|]; rewrite Q in P.
      + case R: T1 => [|T11 T12]; rewrite R in P.
        * case S: (type_check Γ t2) => [T2|]; rewrite S in P.
          - case U: (type_check Γ t3) => [T3|]; rewrite U in P.
            + case V: (beq_ty T2 T3); rewrite V in P; inversion P; subst.
              * eapply HIf. by apply IHt1. by apply IHt2. apply IHt3.
                by move/beq_ty_eq: V => ->.
              * inversion P.
            + inversion P.
          - inversion P.
        * inversion P.
    - case Q: (type_check Γ t1) => [T1|]; rewrite Q in P.
      + eapply HLet. apply IHt1. apply Q. apply IHt2. apply P.
      + inversion P.
Qed.

Theorem type_checking_complete Γ t T : has_type Γ t T -> type_check Γ t = Some T.
Proof.
  move=> P. induction P => //=.
    - by rewrite -/type_check IHP.
    - by rewrite -/type_check IHP1 IHP2 beq_ty_refl.
    - by rewrite -/type_check IHP1 IHP2 IHP3 beq_ty_refl.
    - rewrite IHP1. apply IHP2.
Qed.
