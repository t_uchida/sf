From mathcomp.ssreflect Require Import ssreflect ssrbool ssrfun eqtype ssrnat seq.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Inductive id : Type := Id : nat -> id.

Definition beq_id id1 id2 :=
  match (id1, id2) with
    (Id n1, Id n2) => n1 == n2
  end.

Lemma beq_idP : Equality.axiom beq_id.
  rewrite /beq_id /Equality.axiom => x y. case x; case y => nx ny. case P: (ny == nx).
    - constructor. by move/eqnP: P => -> .
    - constructor. move/eqnP: P => P Q. apply P. by inversion Q.
Qed.

Canonical id_eqMixin := EqMixin beq_idP.
Canonical id_eqType := Eval hnf in EqType id id_eqMixin.

Inductive ty : Type :=
  | TyNat: ty
  | TyArrow : ty -> ty -> ty.

Inductive tm : Type :=
  | TmVar : id -> tm
  | TmApp : tm -> tm -> tm
  | TmAbs : id -> ty -> tm -> tm
  | TmNat : nat -> tm
  | TmSucc : tm -> tm
  | TmPred : tm -> tm
  | TmPlus : tm -> tm -> tm
  | TmIf0 : tm -> tm -> tm -> tm.

Notation a := (Id 0).
Notation b := (Id 1).
Notation c := (Id 2).

Inductive value : tm -> Prop :=
  | VAbs : forall x T t, value (TmAbs x T t)
  | VNat : forall n, value (TmNat n).

Hint Constructors value.

Fixpoint subst (s : tm) (x : id) (t : tm) : tm :=
  match t with
    | TmVar x' => if x == x' then s else t
    | TmAbs x' T t1 => TmAbs x' T (if x == x' then t1 else (subst s x t1))
    | TmApp t1 t2 => TmApp (subst s x t1) (subst s x t2)
    | TmNat n => TmNat n
    | TmSucc n => TmSucc (subst s x n)
    | TmPred n => TmPred (subst s x n)
    | TmPlus n m => TmPlus (subst s x n) (subst s x m)
    | TmIf0 t1 t2 t3 => TmIf0 (subst s x t1) (subst s x t2) (subst s x t3)
  end.

Inductive step : tm -> tm -> Prop :=
  | SAppAbs : forall x T t12 v2, value v2 -> (TmApp (TmAbs x T t12) v2) ==> (subst v2 x t12)
  | SApp1 : forall t1 t1' t2, t1 ==> t1' -> TmApp t1 t2 ==> TmApp t1' t2
  | SApp2 : forall v1 t2 t2', value v1 -> t2 ==> t2' -> TmApp v1 t2 ==> TmApp v1 t2'
  | SSucc1 : forall t1 t1', t1 ==> t1' -> TmSucc t1 ==> TmSucc t1'
  | SSucc2 : forall n, TmSucc (TmNat n) ==> TmNat (n.+1)
  | SPred0 : TmPred (TmNat O) ==> TmNat O
  | SPredS : forall n, TmPred (TmNat (n.+1)) ==> TmNat n
  | SPredSucc : forall n, TmPred (TmSucc (TmNat n)) ==> TmNat n
  | SPred : forall t1 t1', t1 ==> t1' -> TmPred t1 ==> TmPred t1'
  | SPlus1 : forall t1 t1' t2, t1 ==> t1' -> TmPlus t1 t2 ==> TmPlus t1' t2
  | SPlus2 : forall t1 t2 t2', value t1 -> t2 ==> t2' -> TmPlus t1 t2 ==> TmPlus t1 t2'
  | SPlus : forall n1 n2, TmPlus (TmNat n1) (TmNat n2) ==> (TmNat (n1 + n2))
  | SIf0 : forall t1 t2, (TmIf0 (TmNat O) t1 t2) ==> t1
  | SIfS : forall n t1 t2, (TmIf0 (TmNat (S n)) t1 t2) ==> t2
  | SIf : forall t1 t1' t2 t3, t1 ==> t1' -> (TmIf0 t1 t2 t3) ==> (TmIf0 t1' t2 t3)
  where "t1 '==>' t2" := (step t1 t2).

Definition relation (X:Type) := X -> X -> Prop.

Inductive refl_step_closure (X : Type) (R : relation X) : X -> X -> Prop :=
  | RSCRefl : forall (x : X), refl_step_closure R x x
  | RSCStep : forall (x y z : X), R x y -> refl_step_closure R y z -> refl_step_closure R x z.

Notation stepmany := (refl_step_closure step).
Notation "t1 '==>*' t2" := (stepmany t1 t2) (at level 40).

Hint Constructors step.

Definition partial_map (A : Type) := id -> option A.

Definition context := partial_map ty.

Definition extend {A:Type} (Γ : partial_map A) (x:id) (T : A) :=
  fun x' => if x == x' then Some T else Γ x'.

Definition empty {A:Type} : partial_map A := (fun _ => None).

Lemma extend_eq A (ctxt: partial_map A) x T : (extend ctxt x T) x = Some T.
Proof.
  intros. by rewrite /extend (eq_refl x). 
Qed.

Lemma extend_neq A (ctxt: partial_map A) x1 T x2 :
  x2 != x1 -> (extend ctxt x2 T) x1 = ctxt x1.
Proof.
  intros. rewrite /extend. by move/negbTE: H => ->.
Qed.

Inductive has_type : context -> tm -> ty -> Prop :=
  | HVar : forall Γ x T, Γ x = Some T -> has_type Γ (TmVar x) T
  | HAbs : forall Γ x T11 T12 t12, has_type (extend Γ x T11) t12 T12 -> has_type Γ (TmAbs x T11 t12) (TyArrow T11 T12)
  | HApp : forall T11 T12 Γ t1 t2, has_type Γ t1 (TyArrow T11 T12) -> has_type Γ t2 T11 -> has_type Γ (TmApp t1 t2) T12
  | HNat : forall Γ n, has_type Γ (TmNat n) TyNat
  | HSucc : forall Γ n, has_type Γ n TyNat -> has_type Γ (TmSucc n) TyNat
  | HPred : forall Γ n, has_type Γ n TyNat -> has_type Γ (TmPred n) TyNat
  | HPlus : forall Γ t1 t2, has_type Γ t1 TyNat -> has_type Γ t2 TyNat -> has_type Γ (TmPlus t1 t2) TyNat
  | HIf : forall t1 t2 t3 T Γ, has_type Γ t1 TyNat -> has_type Γ t2 T -> has_type Γ t3 T -> has_type Γ (TmIf0 t1 t2 t3) T.

Hint Constructors has_type.

Inductive appears_free_in : id -> tm -> Prop :=
  | AFIVar : forall x, appears_free_in x (TmVar x)
  | AFIApp1 : forall x t1 t2, appears_free_in x t1 -> appears_free_in x (TmApp t1 t2)
  | AFIApp2 : forall x t1 t2, appears_free_in x t2 -> appears_free_in x (TmApp t1 t2)
  | AFIAbs : forall x y T11 t12, y != x -> appears_free_in x t12 -> appears_free_in x (TmAbs y T11 t12)
  | AFISucc : forall x t, appears_free_in x t -> appears_free_in x (TmSucc t)
  | AFIPred : forall x t, appears_free_in x t -> appears_free_in x (TmPred t)
  | AFIPlus1 : forall x t1 t2, appears_free_in x t1 -> appears_free_in x (TmPlus t1 t2)
  | AFIPlus2 : forall x t1 t2, appears_free_in x t2 -> appears_free_in x (TmPlus t1 t2)
  | AFIIf1 : forall x t1 t2 t3, appears_free_in x t1 -> appears_free_in x (TmIf0 t1 t2 t3)
  | AFIIf2 : forall x t1 t2 t3, appears_free_in x t2 -> appears_free_in x (TmIf0 t1 t2 t3)
  | AFIIf3 : forall x t1 t2 t3, appears_free_in x t3 -> appears_free_in x (TmIf0 t1 t2 t3).

Hint Constructors appears_free_in.

Definition closed (t:tm) := forall x, ~ appears_free_in x t.

Lemma free_in_context x t T Γ : appears_free_in x t -> has_type Γ t T -> exists T', Γ x = Some T'.
Proof.
  move=> P. move: T Γ. induction P => T Γ Q; try (inversion Q; subst; eauto).
    - move/IHP: H5. by rewrite extend_neq.
Qed.

Corollary typable_empty_closed t T : has_type empty t T -> closed t.
Proof.
  rewrite /closed => P x Q. move: T P. induction Q => T P; try (inversion P; subst; eauto).
    - inversion P; subst. inversion H1.
    - inversion P; subst. apply (IHQ T12). move: (free_in_context Q H5). rewrite extend_neq. case => T' R. inversion R. apply H.
Qed.

Lemma context_invariance Γ Γ' t S : has_type Γ t S -> (forall x, appears_free_in x t -> Γ x = Γ' x) -> has_type Γ' t S.
Proof.
  move=> P. move: Γ'. induction P => Γ' Q; try (constructor; auto).
    - by rewrite -Q.
    - apply IHP => y R. rewrite /extend. case S: (x == y) => //. apply Q. constructor => //. move: S => -> //.
    - eapply HApp; auto.
Qed.

Lemma substitution_preserves_typing Γ x U v t T : has_type (extend Γ x U) t T -> has_type empty v U -> has_type Γ (subst v x t) T.
Proof.
  move=> P Q. move: Γ T P. induction t => Γ T P; inversion P; subst => //=; eauto.
    - case R: (x == i).
      + move/eqP in R. rewrite R in H1. rewrite extend_eq in H1. inversion H1; subst.
        eapply (context_invariance Q) => x R. case: (free_in_context R Q) => y S. inversion S.
      + constructor. rewrite extend_neq in H1 => //. by move/negbT in R.
    - constructor. case R: (x == i).
      + eapply context_invariance. apply H4. rewrite /extend. move=> y S. case H: (i == y) => //.
        move/eqP: R => ->. by rewrite H.
      + apply IHt. eapply context_invariance. apply H4. move=> z S. rewrite /extend.
        case H: (i == z) => //. move/eqP: H => <-. by rewrite R.
Qed.

Theorem preservation t t' T : has_type empty t T -> t ==> t' -> has_type empty t' T.
Proof.
  move=> P. move: t'. remember empty as ctx. induction P => t' Q; inversion Q; subst; eauto.
    - inversion P1; subst. eapply substitution_preserves_typing. apply H1. apply P2.
Qed.

Theorem progress t T : has_type empty t T -> value t \/ exists t', t ==> t'.
Proof.
  move=> P. remember empty as ctx. induction P; try (left; constructor).
    - rewrite Heqctx in H. inversion H.
    - case: IHP1 => // Q.
      + inversion Q; subst.
        * case: IHP2 => // R.
          - right. exists (subst t2 x t). by constructor.
          - right. case: R => y R. exists (TmApp (TmAbs x T t) y). by constructor.
        * inversion P1.
        * inversion P1; subst; case: Q => y Q; right; exists (TmApp y t2); by constructor.
    - case: IHP => // Q.
      + inversion Q; subst; inversion P; subst. right. exists (TmNat (n0.+1)). constructor.
      + case: Q => x Q. right. exists (TmSucc x). by constructor.
    - case: IHP => // Q.
      + inversion Q; subst; inversion P; subst. case n0.
        * right. exists (TmNat 0). constructor.
        * right. exists (TmNat n). constructor.
      + case: Q => x Q. right. exists (TmPred x). by constructor.
    - case: IHP1 => // Q.
      + case: IHP2 => // R.
        * inversion Q; subst; inversion P1; subst; inversion R; subst; inversion P2; subst. right. exists (TmNat (n + n0)). constructor.
        * case: R => // t2' R . right. exists (TmPlus t1 t2'). by constructor.
      + case: Q => t1' Q. right. exists (TmPlus t1' t2). by constructor.
    - case: IHP1 => // Q.
      + inversion Q; subst; inversion P1; subst. case n.
        * right. exists t2. constructor.
        * move=> n'. right. exists t3. constructor.
      + case: Q => t1' Q. right. exists (TmIf0 t1' t2 t3). by constructor.
Qed.



