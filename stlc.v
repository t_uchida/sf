From mathcomp.ssreflect Require Import ssreflect ssrbool ssrfun eqtype ssrnat seq.

Inductive id : Type := Id : nat -> id.

Definition beq_id id1 id2 :=
  match (id1, id2) with
    (Id n1, Id n2) => n1 == n2
  end.

Theorem beq_id_refl X : beq_id X X.
  case X => n. rewrite /beq_id. by apply/eqnP.
Qed.

Theorem beq_id_eq i1 i2 : beq_id i1 i2 -> i1 = i2.
  case i1; case i2 => m n. rewrite /beq_id. by move/eqnP ->.
Qed.

Theorem beq_id_false_not_eq i1 i2 : ~~ beq_id i1 i2 -> i1 <> i2.
  case i1; case i2 => m n. rewrite /beq_id. move/eqnP => P Q. apply P. by case: Q.
Qed.

Theorem not_eq_beq_id_false i1 i2 : i1 <> i2 -> beq_id i1 i2 = false.
  case i1; case i2 => m n P. apply/negP. rewrite /beq_id. move/eqnP => Q. apply P. by rewrite Q.
Qed.

Theorem beq_id_sym  i1 i2 : beq_id i1 i2 = beq_id i2 i1.
  case i1; case i2 => m n. rewrite /beq_id. apply eq_sym.
Qed.

Theorem beq_id_eq_neq x y z : beq_id x y = true -> beq_id y z = false -> beq_id x z = false.
  case x; case y; case z => a b c. rewrite /beq_id. move/eqnP => P. move/eqnP => Q.
  apply/eqnP => R. apply Q. by rewrite -P.
Qed.  

Inductive ty : Type :=
  | ty_Bool : ty
  | ty_arrow : ty -> ty -> ty.

Inductive tm : Type :=
  | tm_var : id -> tm
  | tm_app : tm -> tm -> tm
  | tm_abs : id -> ty -> tm -> tm
  | tm_true : tm
  | tm_false : tm
  | tm_if : tm -> tm -> tm -> tm.

Notation a := (Id 0).
Notation b := (Id 1).
Notation c := (Id 2).

Notation idB := (tm_abs a ty_Bool (tm_var a)).

Notation idBB :=
  (tm_abs a (ty_arrow ty_Bool ty_Bool) (tm_var a)).

Notation idBBBB :=
  (tm_abs a (ty_arrow (ty_arrow ty_Bool ty_Bool)
                      (ty_arrow ty_Bool ty_Bool))
    (tm_var a)).

Notation k := (tm_abs a ty_Bool (tm_abs b ty_Bool (tm_var a))).

Inductive value : tm -> Prop :=
  | v_abs : forall x T t, value (tm_abs x T t)
  | t_true : value tm_true
  | t_false : value tm_false.

Hint Constructors value.

Fixpoint subst (s : tm) (x : id) (t : tm) : tm :=
  match t with
  | tm_var x' => if beq_id x x' then s else t
  | tm_abs x' T t1 => tm_abs x' T (if beq_id x x' then t1 else (subst s x t1))
  | tm_app t1 t2 => tm_app (subst s x t1) (subst s x t2)
  | tm_true => tm_true
  | tm_false => tm_false
  | tm_if t1 t2 t3 => tm_if (subst s x t1) (subst s x t2) (subst s x t3)
  end.

Inductive step : tm -> tm -> Prop :=
  | ST_AppAbs : forall x T t12 v2,
         value v2 -> (tm_app (tm_abs x T t12) v2) ==> (subst v2 x t12)
  | ST_App1 : forall t1 t1' t2,
         t1 ==> t1' -> tm_app t1 t2 ==> tm_app t1' t2
  | ST_App2 : forall v1 t2 t2',
         value v1 -> t2 ==> t2' -> tm_app v1 t2 ==> tm_app v1 t2'
  | ST_IfTrue : forall t1 t2,
      (tm_if tm_true t1 t2) ==> t1
  | ST_IfFalse : forall t1 t2,
      (tm_if tm_false t1 t2) ==> t2
  | ST_If : forall t1 t1' t2 t3,
      t1 ==> t1' -> (tm_if t1 t2 t3) ==> (tm_if t1' t2 t3)

where "t1 '==>' t2" := (step t1 t2).

Definition relation (X : Type) := X -> X -> Prop.

Inductive refl_step_closure (X:Type) (R: relation X) : X -> X -> Prop :=
  | rsc_refl : forall (x : X),
                 refl_step_closure X R x x
  | rsc_step : forall (x y z : X),
                    R x y ->
                    refl_step_closure X R y z ->
                    refl_step_closure X R x z.
Implicit Arguments refl_step_closure [[X]].

Notation stepmany := (refl_step_closure step).
Notation "t1 '==>*' t2" := (stepmany t1 t2) (at level 40).

Hint Constructors step.

Tactic Notation "print_goal" := match goal with |- ?x => idtac x end.
Tactic Notation "normalize" :=
   repeat (print_goal; eapply rsc_step ;
             [ (eauto 10; fail) | (instantiate; simpl)]);
   apply rsc_refl.

Lemma step_example1 :
  (tm_app idBB idB) ==>* idB.
Proof.
  eapply rsc_step. apply ST_AppAbs. constructor.
  simpl. apply rsc_refl.
Qed.

Lemma step_example1' :
  (tm_app idBB idB) ==>* idB.
Proof.
  normalize.
Qed.

Lemma step_example2 :
  (tm_app idBB (tm_app idBB idB)) ==>* idB.
Proof.
  normalize.
Qed.

Lemma step_example3 :
  (tm_app (tm_app idBBBB idBB) idB) ==>* idB.
Proof.
  eapply rsc_step.
    - apply ST_App1. apply ST_AppAbs. constructor.
    - simpl. apply step_example1.
Qed.

Lemma step_example3' :
  (tm_app (tm_app idBBBB idBB) idB) ==>* idB.
Proof.
  normalize.
Qed.

Definition partial_map (A : Type) := id -> option A.

Definition context := partial_map ty.

Definition empty {A:Type} : partial_map A := (fun _ => None).

Definition extend {A:Type} (ctx : partial_map A) (x:id) (T : A) :=
  fun x' => if beq_id x x' then Some T else ctx x'.

Lemma extend_eq A (ctxt: partial_map A) x T : (extend ctxt x T) x = Some T.
Proof.
  by rewrite /extend beq_id_refl.
Qed.

Lemma extend_neq A (ctxt: partial_map A) x1 T x2 :
  beq_id x2 x1 = false -> (extend ctxt x2 T) x1 = ctxt x1.
Proof.
  by rewrite /extend => ->.
Qed.

Inductive has_type : context -> tm -> ty -> Prop :=
  | T_Var : forall Γ x T,
      Γ x = Some T ->
      has_type Γ (tm_var x) T
  | T_Abs : forall Γ x T11 T12 t12,
      has_type (extend Γ x T11) t12 T12 ->
      has_type Γ (tm_abs x T11 t12) (ty_arrow T11 T12)
  | T_App : forall T11 T12 Γ t1 t2,
      has_type Γ t1 (ty_arrow T11 T12) ->
      has_type Γ t2 T11 ->
      has_type Γ (tm_app t1 t2) T12
  | T_True : forall Γ,
       has_type Γ tm_true ty_Bool
  | T_False : forall Γ,
       has_type Γ tm_false ty_Bool
  | T_If : forall t1 t2 t3 T Γ,
       has_type Γ t1 ty_Bool ->
       has_type Γ t2 T ->
       has_type Γ t3 T ->
       has_type Γ (tm_if t1 t2 t3) T.

Hint Constructors has_type.

Example typing_example_1 :
  has_type empty (tm_abs a ty_Bool (tm_var a)) (ty_arrow ty_Bool ty_Bool).
Proof.
  auto.
Qed.

Example typing_example_2 :
  has_type empty
    (tm_abs a ty_Bool
       (tm_abs b (ty_arrow ty_Bool ty_Bool)
          (tm_app (tm_var b) (tm_app (tm_var b) (tm_var a)))))
    (ty_arrow ty_Bool (ty_arrow (ty_arrow ty_Bool ty_Bool) ty_Bool)).
Proof.
  apply T_Abs. apply T_Abs. eapply T_App.
    - by apply T_Var.
    - eapply T_App.
      + by apply T_Var.
      + by apply T_Var.
Qed.

Inductive appears_free_in : id -> tm -> Prop :=
  | afi_var : forall x,
      appears_free_in x (tm_var x)
  | afi_app1 : forall x t1 t2,
      appears_free_in x t1 -> appears_free_in x (tm_app t1 t2)
  | afi_app2 : forall x t1 t2,
      appears_free_in x t2 -> appears_free_in x (tm_app t1 t2)
  | afi_abs : forall x y T11 t12,
      y <> x ->
      appears_free_in x t12 ->
      appears_free_in x (tm_abs y T11 t12)
  | afi_if1 : forall x t1 t2 t3,
      appears_free_in x t1 ->
      appears_free_in x (tm_if t1 t2 t3)
  | afi_if2 : forall x t1 t2 t3,
      appears_free_in x t2 ->
      appears_free_in x (tm_if t1 t2 t3)
  | afi_if3 : forall x t1 t2 t3,
      appears_free_in x t3 ->
      appears_free_in x (tm_if t1 t2 t3).

Hint Constructors appears_free_in.

Definition closed (t:tm) := forall x, ~ appears_free_in x t.

Lemma free_in_context x t T Γ :
  appears_free_in x t ->
  has_type Γ t T ->
  exists T', Γ x = Some T'.
Proof.
  intros. generalize dependent Γ. generalize dependent T.
  induction H; intros; try solve [inversion H0; eauto].
  inversion H1; subst.
  apply IHappears_free_in in H7.
  apply not_eq_beq_id_false in H.
  rewrite extend_neq in H7; assumption.
Qed.

Corollary typable_empty__closed t T :
  has_type empty t T -> closed t.
Proof.
  move=> P. rewrite /closed => x Q. move: T P. induction Q => T P.
    - inversion P; subst. inversion H1.
    - inversion P; subst. eapply IHQ. apply H2.
    - inversion P; subst. eapply IHQ. apply H4.
    - inversion P; subst. eapply IHQ. admit.
    - inversion P; subst. eapply IHQ. apply H3.
    - inversion P; subst. eapply IHQ. apply H5.
    - inversion P; subst. eapply IHQ. apply H6.
Admitted.

Lemma context_invariance Γ Γ' t S :

  has_type Γ t S ->
     (forall x, appears_free_in x t -> Γ x = Γ' x) ->
     has_type Γ' t S.
Proof.
  move=> P. move: Γ'. induction P => Γ' Q.
    - constructor. symmetry. move: (Q x). by rewrite H => ->.
    - constructor. eapply IHP => x' R. case S: (beq_id x x').
      + by rewrite (beq_id_eq _ _ S) /extend (beq_id_refl x').
      + rewrite /extend. rewrite S. apply Q. constructor. admit. apply R.
    - eapply T_App.
      + apply IHP1 => x R. apply Q. by constructor.
      + apply IHP2 => x R. apply Q. constructor. admit.
    - constructor.
    - constructor.
    - constructor.
      + apply IHP1 => x R. apply Q. by constructor.
      + apply IHP2 => x R. apply Q. constructor. admit.
      + apply IHP3 => x R. apply Q. constructor. admit.
Admitted.

Lemma context_invariance_ Γ t S :
     has_type empty t S -> has_type Γ t S.
Proof.
Admitted.

Lemma substitution_preserves_typing Γ x U v t T :
     has_type (extend Γ x U) t T ->
     has_type empty v U ->
     has_type Γ (subst v x t) T.
Proof.
  move: T Γ. induction t => T Γ P Q.
    - case R: (beq_id x i).
      + inversion P; subst. rewrite /extend R in H1. inversion H1; subst. rewrite /subst R.
        by apply context_invariance_.
      + inversion P; subst. rewrite /extend R in H1. rewrite /subst R. by constructor.
    - inversion P; subst. rewrite /subst -/subst. eapply T_App.
      + apply IHt1. apply H2. apply Q.
      + apply IHt2. apply H4. apply Q.
    - case R: (beq_id x i).
      + rewrite /subst -/subst R. eapply context_invariance.
        * apply P.
        * move=> y S. inversion S; subst. rewrite /extend. move: H2. move/not_eq_beq_id_false.
          move=> V. by move: (beq_id_eq_neq _ _ _ R V) => ->.
      + inversion P; subst. have S: has_type (extend (extend Γ i t) x U) t0 T12.
        * eapply context_invariance. apply H4. move=> y S. rewrite /extend.
          case T: (beq_id x y) => //. rewrite (beq_id_sym x y) in T.
          move: (beq_id_eq_neq _ _ _ T R). by rewrite beq_id_sym => ->.
        rewrite /subst -/subst R. constructor. apply IHt. apply S. apply Q.
    - rewrite /subst. inversion P; subst. constructor.
    - rewrite /subst. inversion P; subst. constructor.
    - rewrite /subst -/subst. inversion P; subst. constructor.
      + by apply IHt1.
      + by apply IHt2.
      + by apply IHt3.
Qed.

Theorem preservation t t' T :
     has_type empty t T ->
     t ==> t' ->
     has_type empty t' T.
Proof.
  move=> P. move: t'. induction P => t' Q; inversion Q; subst => //.
    - eapply substitution_preserves_typing with T11.
      + inversion P1; subst. apply H1.
      + admit.
    - eapply T_App.
      + by apply IHP1.
      + assumption.
    - eapply T_App.
      + apply P1.
      + by apply IHP2.
    - apply T_If.
      + by apply IHP1.
      + assumption.
      + assumption.
Admitted.

Theorem progress t T : has_type empty t T -> value t \/ exists t', t ==> t'.
Proof.
  move=> P. remember (@empty ty) as Γ. induction P; subst.
    - inversion H.
    - left. constructor.
    - destruct IHP1 => //.
      + destruct IHP2 => //.
        * inversion H; subst. right. exists (subst t2 x t). by constructor.
          inversion P1. inversion P1.
        * right. elim: H0 => x Q. exists (tm_app t1 x). by constructor.
      + right. elim: H => x Q. exists (tm_app x t2). by constructor.
    - left. constructor.
    - left. constructor.
    - right. destruct IHP1 => //.
      + destruct H.
        * inversion P1.
        * exists t2. constructor.
        * exists t3. constructor.
      + elim: H => x Q. exists (tm_if x t2 t3). by constructor.
Qed.
