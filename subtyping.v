Require Import mathcomp.ssreflect.ssreflect.
From mathcomp Require Import all_ssreflect.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Inductive id : Type := Id : nat -> id.

Definition eqb_id i1 i2 :=
  match (i1, i2) with
  | (Id n1, Id n2) => n1 == n2
  end.

Lemma eqb_idP : Equality.axiom eqb_id.
Proof.
  rewrite /Equality.axiom. move=> [] nx [] ny. case H0: (nx == ny).
  - rewrite /eqb_id. rewrite H0. constructor. move/eqP :H0 => <- //.
  - rewrite /eqb_id. rewrite H0. constructor. move/eqP :H0 => H0 H1. apply H0. by inversion H1.
Qed.

Canonical id_eqMixin := EqMixin eqb_idP.
Canonical id_eqType := Eval hnf in EqType id id_eqMixin.

Lemma eqb_id_refl (x : id) : x == x.
Proof. by apply/eqP. Qed.

Inductive ty : Type :=
  | TyTop : ty
  | TyBool : ty
  | TyBase : id -> ty
  | TyArrow : ty -> ty -> ty
  | TyUnit : ty
  | TyRnil : ty
  | TyRcons : id -> ty -> ty -> ty.

Inductive tm : Type :=
  | TmVar : id -> tm
  | TmApp : tm -> tm -> tm
  | TmAbs : id -> ty -> tm -> tm
  | TmTrue : tm
  | TmFalse : tm
  | TmIf : tm -> tm -> tm -> tm
  | TmUnit : tm
  | TmProj : tm -> id -> tm
  | TmRnil : tm
  | TmRcons : id -> tm -> tm -> tm.

Fixpoint subst (s:tm) (x:id) (t:tm) : tm :=
  match t with
  | TmVar y => if x == y then s else t
  | TmAbs y T t1 => TmAbs y T (if x == y then t1 else (subst s x t1))
  | TmApp t1 t2 => TmApp (subst s x t1) (subst s x t2)
  | TmTrue => TmTrue
  | TmFalse => TmFalse
  | TmIf t1 t2 t3 => TmIf (subst s x t1) (subst s x t2) (subst s x t3)
  | TmUnit => TmUnit
  | TmProj t i => TmProj (subst s x t) i
  | TmRnil => TmRnil
  | TmRcons i t1 t2 => TmRcons i (subst s x t1) (subst s x t2)
  end.

Inductive value : tm -> Prop :=
  | VAbs x T t : value (TmAbs x T t)
  | VTrue : value TmTrue
  | VFalse : value TmFalse
  | VUnit : value TmUnit
  | VRnil : value TmRnil
  | VRcons i t1 t2 : value t1 -> value t2 -> value (TmRcons i t1 t2).

Hint Constructors value.

Fixpoint ty_lookup (i:id) (Tr:ty) : option ty :=
  match Tr with
  | TyRcons i' T Tr' => if i == i' then Some T else ty_lookup i Tr'
  | _ => None
  end.

Fixpoint tm_lookup (i:id) (tr:tm) : option tm :=
  match tr with
  | TmRcons i' t tr' => if i == i' then Some t else tm_lookup i tr'
  | _ => None
  end.

Inductive step : tm -> tm -> Prop :=
  | STAppAbs x T t12 v2 : value v2 -> TmApp (TmAbs x T t12) v2 ==> subst v2 x t12
  | STApp1 t1 t1' t2 : t1 ==> t1' -> TmApp t1 t2 ==> TmApp t1' t2
  | STApp2 v1 t2 t2' : value v1 -> t2 ==> t2' -> TmApp v1 t2 ==> TmApp v1 t2'
  | STIfTrue t1 t2 : TmIf TmTrue t1 t2 ==> t1
  | STIfFalse t1 t2 : TmIf TmFalse t1 t2 ==> t2
  | STIf t1 t1' t2 t3 : t1 ==> t1' -> TmIf t1 t2 t3 ==> TmIf t1' t2 t3
  | STProj1 t t' i : t ==> t' -> TmProj t i ==> TmProj t' i
  | STProjRcd tr i vi : value tr -> tm_lookup i tr = Some vi -> TmProj tr i ==> vi
  | STRcdHead i t1 t1' tr2 : t1 ==> t1' -> TmRcons i t1 tr2 ==> TmRcons i t1' tr2
  | STRcdTail i v1 tr2 tr2' : value v1 -> tr2 ==> tr2' -> TmRcons i v1 tr2 ==> TmRcons i v1 tr2'
where "t1 '==>' t2" := (step t1 t2).

Hint Constructors step.

Inductive refl_step_closure (X:Type) (R: X -> X -> Prop) 
                            : X -> X -> Prop :=
  | rsc_refl : forall (x : X),
                 refl_step_closure R x x
  | rsc_step : forall (x y z : X),
                    R x y ->
                    refl_step_closure R y z ->
                    refl_step_closure R x z.

Implicit Arguments refl_step_closure [[X]].

Notation stepmany := (refl_step_closure step).
Notation "t1 '==>*' t2" := (stepmany t1 t2) (at level 40).

Inductive record_app : id -> ty -> ty -> ty -> Prop :=
  | RecordAppHead i T1 T2 : record_app i T1 T2 (TyRcons i T1 T2)
  | RecordAppTail i1 i2 T1 T2 T3 T3' : record_app i1 T1 T3 T3' ->
      record_app i1 T1 (TyRcons i2 T2 T3) (TyRcons i2 T2 T3').

Inductive record_perm : ty -> ty -> Prop :=
  | RecordPermNil : record_perm TyRnil TyRnil
  | RecordPermCons i T T1 T2 T2' : record_perm T1 T2 -> record_app i T T2 T2' ->
      record_perm (TyRcons i T T1) T2'.

Inductive subtype : ty -> ty -> Prop :=
  | SRefl T : subtype T T
  | STrans S U T : subtype S U -> subtype U T -> subtype S T
  | STop S : subtype S TyTop
  | SArrow S1 S2 T1 T2 :
      subtype T1 S1 -> subtype S2 T2 -> subtype (TyArrow S1 S2) (TyArrow T1 T2)
  | SRcdWidth i T1 T2 : subtype (TyRcons i T1 T2) T2
  | SRcdDepth i S1 S2 T1 T2 :
      subtype T1 S1 -> subtype T2 S2 -> subtype (TyRcons i T1 T2) (TyRcons i S1 S2)
  | SRcdPerm T1 T2 : record_perm T1 T2 -> subtype T1 T2.

Hint Constructors subtype.

Module Examples.

Notation name := (Id 0).
Notation gpa := (Id 1).
Notation ssn := (Id 2).

Notation A := (TyBase (Id 6)).
Notation B := (TyBase (Id 7)).
Notation C := (TyBase (Id 8)).

Notation String := (TyBase (Id 9)).
Notation Float := (TyBase (Id 10)).
Notation Integer := (TyBase (Id 11)).

Definition Person : ty := TyRcons name String TyRnil.
Definition Student : ty := TyRcons name String (TyRcons gpa Float TyRnil).
Definition Employee : ty := TyRcons name String (TyRcons ssn Integer TyRnil).

Example sub_student_person : subtype Student Person.
Proof.
  rewrite /Student /Person. by constructor.
Qed.

Example sub_employee_person : subtype Employee Person.
Proof.
  rewrite /Employee /Person. by constructor.
Qed.

Example subtyping_example_0 : subtype (TyArrow C Person) (TyArrow C TyTop).
Proof.
  rewrite /Person. by constructor.
Qed.

Definition context := id -> (option ty).
Definition empty : context := (fun _ => None).
Definition extend (c : context) (x:id) (T : ty) :=
  fun x' => if x == x' then Some T else c x'.

Inductive has_type : context -> tm -> ty -> Prop :=
  | TVar c x T :
      c x = Some T ->
      has_type c (TmVar x) T
  | TAbs c x T11 T12 t12 :
      has_type (extend c x T11) t12 T12 ->
      has_type c (TmAbs x T11 t12) (TyArrow T11 T12)
  | TApp T1 T2 c t1 t2 :
      has_type c t1 (TyArrow T1 T2) ->
      has_type c t2 T1 ->
      has_type c (TmApp t1 t2) T2
  | TTrue c :
       has_type c TmTrue TyBool
  | TFalse c :
       has_type c TmFalse TyBool
  | TIf t1 t2 t3 T c :
       has_type c t1 TyBool ->
       has_type c t2 T ->
       has_type c t3 T ->
       has_type c (TmIf t1 t2 t3) T
  | TUnit c :
      has_type c TmUnit TyUnit
  | TSub c t S T :
      has_type c t S ->
      subtype S T ->
      has_type c t T.

Hint Constructors has_type.

Lemma sub_inversion_Bool U : subtype U TyBool -> U = TyBool.
Proof.
  move H1: (TyBool) => V H2. elim: H2 => //=.

