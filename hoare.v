From mathcomp.ssreflect Require Import ssreflect ssrbool ssrfun eqtype ssrnat seq.

Load implist.

Inductive com : Type :=
  | CSkip : com
  | CAss : id -> aexp -> com
  | CSeq : com -> com -> com
  | CIf : bexp -> com -> com -> com
  | CWhile : bexp -> com -> com.

Notation "'SKIP'" := CSkip.
Notation "l '::=' a" := (CAss l a) (at level 60).
Notation "c1 ; c2" := (CSeq c1 c2) (at level 80, right associativity).
Notation "'WHILE' b 'DO' c 'END'" := (CWhile b c) (at level 80, right associativity).
Notation "'IFB' e1 'THEN' e2 'ELSE' e3 'FI'" := (CIf e1 e2 e3) (at level 80, right associativity).

Reserved Notation "c1 '/' st '||' st'" (at level 40, st at level 39).

Inductive ceval : state -> com -> state -> Prop :=
  | E_Skip : forall st, SKIP / st || st
  | E_Asgn : forall st a1 n l, aeval st a1 = n -> (l ::= a1) / st || update st l n
  | E_Seq : forall c1 c2 st st' st'', c1 / st || st' -> c2 / st' || st'' -> (c1 ; c2) / st || st''
  | E_IfTrue : forall st st' b1 c1 c2, beval st b1 = true -> c1 / st || st' -> (IFB b1 THEN c1 ELSE c2 FI) / st || st'
  | E_IfFalse : forall st st' st'' b1 c1 c2, beval st b1 = false -> c2 / st || st' -> (c1 ; c2) / st || st''
  | E_WhileEnd : forall st b1 c1, beval st b1 = false -> (WHILE b1 DO c1 END) / st || st
  | E_WhileLoop : forall st st' st'' b1 c1, beval st b1 = true -> c1 / st || st' -> (WHILE b1 DO c1 END) / st' || st'' -> WHILE b1 DO c1 END / st || st''
  where "c1 '/' st '||' st'" := (ceval st c1 st').

Definition loop : com :=
  WHILE BTrue DO
    SKIP
  END.

Theorem loop_never_stops st st' : ~ (loop / st || st').
  rewrite /loop => P. remember (WHILE BTrue DO SKIP END) as loopdef. induction P => //.
    - inversion Heqloopdef. rewrite H1 in H. inversion H.
Qed.

Definition Assertion := state -> Prop.

Definition hoare_triple (P : Assertion) (c : com) (Q : Assertion) : Prop :=
  forall st st', c / st || st' -> P st -> Q st'.

Notation "{{ P }} c" := (hoare_triple P c (fun st => True)) (at level 90) : hoare_spec_scope.
Notation "{{ P }} c {{ Q }}" := (hoare_triple P c Q) (at level 90, c at next level) : hoare_spec_scope.
Open Scope hoare_spec_scope.

Theorem hoare_post_true (P Q : Assertion) c : (forall st, Q st) -> {{P}} c {{Q}}.
Proof.
  rewrite /hoare_triple => R st1 st2 S T. apply R.
Qed.

Theorem hoare_pre_false (P Q : Assertion) c : (forall st, ~ (P st)) -> {{P}} c {{Q}}.
Proof.
  rewrite /hoare_triple => R st st' S. by move/R.
Qed.

Definition assn_sub V a Q : Assertion := fun (st : state) => Q (update st V (aeval st a)).

Theorem hoare_asgn Q V a : {{assn_sub V a Q}} (V ::= a) {{Q}}.
Proof.
  rewrite /hoare_triple /assn_sub => st st' P R. inversion P. by subst.
Qed.

Theorem hoare_asgn_eq Q Q' V a : Q' = assn_sub V a Q -> {{Q'}} (V ::= a) {{Q}}.
Proof.
  move=> ->. apply hoare_asgn.
Qed.

Example assn_sub_example' : {{fun st => 3 = 3}} (X ::= (ANum 3)) {{fun st => asnat (st X) = 3}}.
Proof.
  by apply hoare_asgn_eq.
Qed.

Example hoare_asgn_examples_1 :
  {{ fun st => asnat (st X) + 1 <= 5 }}
  (X ::= APlus (AId X) (ANum 1))
  {{ fun st => asnat (st X) <= 5 }}.
Proof.
  by apply hoare_asgn_eq.
Qed.

Theorem hoare_asgn_weakest P V a Q : {{P}} (V ::= a) {{Q}} -> forall st, P st -> assn_sub V a Q st.
Proof.
  rewrite /hoare_triple /assn_sub => H1 st. apply H1. by constructor.
Qed.

Theorem hoare_consequence (P P' Q Q' : Assertion) c :
  {{P'}} c {{Q'}} -> (forall st, P st -> P' st) -> (forall st, Q' st -> Q st) -> {{P}} c {{Q}}.
Proof.
  rewrite /hoare_triple => H1 H2 H3 st st' H4 H5. apply H3. apply (H1 st st') => //. apply (H2 _ H5).
Qed.

Theorem hoare_consequence_pre (P P' Q : Assertion) c : {{P'}} c {{Q}} -> (forall st, P st -> P' st) -> {{P}} c {{Q}}.
Proof.
  move=> H1 H2. apply (hoare_consequence P _ Q _ c H1) => //.
Qed.

Theorem hoare_consequence_post (P Q Q' : Assertion) c : {{P}} c {{Q'}} -> (forall st, Q' st -> Q st) -> {{P}} c {{Q}}.
Proof.
  move=> H1 H2. apply (hoare_consequence P _ Q _ c H1) => //.
Qed.

Example hoare_asgn_example1' : {{fun st => True}} (X ::= (ANum 1)) {{fun st => asnat (st X) = 1}}.
Proof.
  eapply hoare_consequence_pre. by apply hoare_asgn_eq. done.
Qed.

Theorem hoare_skip P : {{P}} SKIP {{P}}.
Proof.
  rewrite /hoare_triple => st st' H1. by inversion H1.
Qed.

Theorem hoare_seq P Q R c1 c2 : {{Q}} c2 {{R}} -> {{P}} c1 {{Q}} -> {{P}} c1 ; c2 {{R}}.
Proof.
  rewrite /hoare_triple => S T st st' U V. inversion U; subst.
    - apply (S st'0 st'); try assumption. apply (T st st'0); try assumption.
    - admit.
Admitted.

Example hoare_asgn_example3 a n :
  {{fun st => aeval st a = n}}
  (X ::= a; SKIP)
  {{fun st => st X = n}}.
Proof.
 eapply hoare_seq.
    - apply hoare_skip.
    - eapply hoare_consequence_pre.
      + by apply hoare_asgn.
      + done.
Qed.

Example hoare_asgn_example4 :
  {{fun st => True}} (X ::= (ANum 1); Y ::= (ANum 2))
  {{fun st => asnat (st X) = 1 /\ asnat (st Y) = 2}}.
Proof.
  eapply hoare_seq.
    - apply hoare_asgn.
    - eapply hoare_asgn; try (repeat constructor).
      + rewrite update_neq => //. by inversion H; subst; rewrite update_eq.
Admitted.

Example swap_exercise:
  {{ fun st => asnat (st X) <= asnat (st Y) }}
  (Z ::= (AId X); X ::= (AId Y); Y ::= (AId Z))
  {{ fun st => asnat (st Y) <= asnat (st X) }}.
Proof.
  apply hoare_seq with (Q := fun st => asnat (st Z) <= asnat (st Y)).
    - apply hoare_seq with (Q := fun st => asnat (st Z) <= asnat (st X)).
      + apply hoare_asgn_eq. constructor.
      + apply hoare_asgn_eq. constructor.
    - apply hoare_asgn_eq. constructor.
Qed.

Definition bassn b : Assertion := fun st => (beval st b = true).

Lemma bexp_eval_true b st : beval st b = true -> (bassn b) st.
Proof.
  by rewrite /bassn.
Qed.

Lemma bexp_eval_false b st : beval st b = false -> ~ ((bassn b) st).
Proof.
  by rewrite /bassn => ->.
Qed.

Theorem hoare_if P Q b c1 c2 :
  {{fun st => P st /\ bassn b st}} c1 {{Q}} ->
  {{fun st => P st /\ ~(bassn b st)}} c2 {{Q}} ->
  {{P}} (IFB b THEN c1 ELSE c2 FI) {{Q}}.
Proof.
  move=> H1 H2 st st' H3 H4. inversion H3; subst.
  apply (H1 st st'); try assumption. split; assumption.
Qed.

Example if_example :
  {{fun st => True}}
  IFB (BEq (AId X) (ANum 0))
    THEN (Y ::= (ANum 2))
    ELSE (Y ::= APlus (AId X) (ANum 1))
  FI
  {{fun st => asnat (st X) <= asnat (st Y)}}.
Proof.
  apply hoare_if.
    - eapply hoare_consequence_pre.
      + apply hoare_asgn.
      + rewrite /bassn /assn_sub /update => /= st H. inversion H. by move: H1 => /eqnP ->.
    - eapply hoare_consequence_pre.
      + apply hoare_asgn.
      + rewrite /bassn /update => /= st H. inversion H. rewrite /assn_sub update_neq => //.
        rewrite update_eq => /=. by elim: (asnat (st X)).
Qed.

Lemma hoare_while P b c :
  {{fun st => P st /\ bassn b st}} c {{P}} ->
  {{P}} WHILE b DO c END {{fun st => P st /\ ~ (bassn b st)}}.
Proof.
  move=> Q st st' R S. move: R; move T: (WHILE b DO c END) => wcom R.
  induction R; try (inversion T); subst.
    - split => //. by apply bexp_eval_false.
    - apply IHR2 => //. by apply (Q st st').
Qed.

Example while_example :
  {{fun st => asnat (st X) <= 3}}
  WHILE (BLe (AId X) (ANum 2))
    DO X ::= APlus (AId X) (ANum 1)
  END
  {{fun st => asnat (st X) = 3}}.
Proof.
  eapply hoare_consequence_post.
    - apply hoare_while. eapply hoare_consequence_pre.
      + apply hoare_asgn.
      + admit. (* rewrite /assn_sub /bassn. *)
    - rewrite /is_true /bassn /beval /aeval {3}/asnat => st [P Q]. move: P. rewrite leq_eqVlt. move /orP. case.
      + move/eqP => //. case Q. admit.
Admitted.

Definition subtract_slowly : com :=
  WHILE BNot (BEq (AId X) (ANum 0)) DO
    Z ::= AMinus (AId Z) (ANum 1);
    X ::= AMinus (AId X) (ANum 1)
  END.

Definition subtract_slowly_invariant x z :=
  fun st => minus (asnat (st Z)) (asnat (st X)) = minus z x.

Theorem subtract_slowly_correct x z :
  {{fun st => asnat (st X) = x /\ asnat (st Z) = z}}
  subtract_slowly
  {{fun st => asnat (st Z) = minus z x}}.
Proof.

  rewrite /subtract_slowly. eapply hoare_consequence with (P' := subtract_slowly_invariant x z).
    - apply hoare_while. eapply hoare_seq.
      + apply hoare_asgn.
      + eapply hoare_consequence_pre.
        * apply hoare_asgn.
        * rewrite /subtract_slowly_invariant /assn_sub /update /bassn => /= st [P Q].
          admit.
    - move=> st [P Q]. by rewrite /subtract_slowly_invariant P Q.
    - rewrite /subtract_slowly_invariant /bassn => /= st [P Q]. rewrite -P.
      have R (n : nat): (n != 0) <> true -> n = 0. by elim: n.
      rewrite (R _ Q).
      have S (n : nat): (n - 0 = n)%coq_nat. by elim: n.
      by rewrite S.
Admitted.


