From mathcomp.ssreflect Require Import ssreflect ssrbool ssrfun eqtype ssrnat seq.


Inductive id : Type := Id : nat -> id.

Definition beq_id id1 id2 :=
  match (id1, id2) with
    (Id n1, Id n2) => n1 == n2
  end.

Theorem beq_id_refl i : beq_id i i.
Proof.
  case i => n. rewrite /beq_id. by apply/eqnP.
Qed.

Theorem beq_id_eq i1 i2 : beq_id i1 i2 -> i1 = i2.
Proof.
  rewrite /beq_id. by case i1; case i2 => m n /eqnP => ->.
Qed.

Theorem beq_id_false_not_eq i1 i2 : beq_id i1 i2 = false -> i1 <> i2.
Proof.
  rewrite /beq_id. case i1; case i2 => m n. move/eqnP => P Q. apply P. by case: Q => ->.
Qed.

Theorem not_eq_beq_id_false i1 i2 : i1 <> i2 -> beq_id i1 i2 = false.
Proof.
  rewrite /beq_id. case i1; case i2 => m n P. apply/negbTE/negP. move/eqP => Q. apply P. by rewrite Q.
Qed.

Definition X : id := Id 0.
Definition Y : id := Id 1.
Definition Z : id := Id 2.

Inductive val : Type :=
  | VNat : nat -> val
  | VList : list nat -> val.

Definition state := id -> val.

Definition empty_state : state := fun _ => VNat 0.
Definition update (st : state) (V:id) (n : val) : state :=
  fun V' => if beq_id V V' then n else st V'.

Definition asnat (v : val) : nat :=
  match v with
  | VNat n => n
  | VList _ => 0
  end.

Definition aslist (v : val) : list nat :=
  match v with
  | VNat n => [::]
  | VList xs => xs
  end.

Inductive aexp : Type :=
  | ANum : nat -> aexp
  | AId : id -> aexp
  | APlus : aexp -> aexp -> aexp
  | AMinus : aexp -> aexp -> aexp
  | AMult : aexp -> aexp -> aexp
  | AHead : aexp -> aexp
  | ATail : aexp -> aexp
  | ACons : aexp -> aexp -> aexp
  | ANil : aexp.

Definition head (l : list nat) :=
  match l with
  | x::xs => x
  | [::] => 0
  end.

Definition tail (l : list nat) :=
  match l with
  | x::xs => xs
  | [::] => [::]
  end.

Fixpoint aeval (st : state) (e : aexp) : val :=
  match e with
  | ANum n => VNat n
  | AId i => st i
  | APlus a1 a2 => VNat (asnat (aeval st a1) + asnat (aeval st a2))
  | AMinus a1 a2 => VNat (asnat (aeval st a1) - asnat (aeval st a2))
  | AMult a1 a2 => VNat (asnat (aeval st a1) * asnat (aeval st a2))
  
  | ATail a => VList (tail (aslist (aeval st a)))
  | AHead a => VNat (head (aslist (aeval st a)))
  | ACons a1 a2 => VList (asnat (aeval st a1) :: aslist (aeval st a2))
  | ANil => VList [::]
  end.

Inductive bexp : Type :=
  | BTrue : bexp
  | BFalse : bexp
  | BEq : aexp -> aexp -> bexp
  | BLe : aexp -> aexp -> bexp
  | BNot : bexp -> bexp
  | BAnd : bexp -> bexp -> bexp
  | BIsCons : aexp -> bexp.

Fixpoint beval (st : state) (e : bexp) : bool :=
  match e with
  | BTrue => true
  | BFalse => false
  | BEq a1 a2 => asnat (aeval st a1) == asnat (aeval st a2)
  | BLe a1 a2 => asnat (aeval st a1) < asnat (aeval st a2)
  | BNot b1 => negb (beval st b1)
  | BAnd b1 b2 => andb (beval st b1) (beval st b2)
  | BIsCons a => match aslist (aeval st a) with
                     | _::_ => true
                     | [::] => false
                   end
  end.

Theorem update_eq n V st : (update st V n) V = n.
Proof.
  by rewrite /update beq_id_refl.
Qed.

Theorem update_neq V2 V1 n st : beq_id V2 V1 = false -> (update st V2 n) V1 = st V1.
Proof.
  by rewrite /update => ->.
Qed.

Theorem update_shadow x1 x2 k1 k2 (f : state) : (update (update f k2 x1) k2 x2) k1 = (update f k2 x2) k1.
Proof.
  rewrite /update. by case: (beq_id k2 k1).
Qed.

Theorem update_same x1 k1 k2 (f : state) : f k1 = x1 -> (update f k1 x1) k2 = f k2.
Proof.
  rewrite /update => <-. case Q: (beq_id k1 k2) => //.
    - by rewrite (beq_id_eq _ _ Q).
Qed.

Theorem update_permute x1 x2 k1 k2 k3 f :
  beq_id k2 k1 = false ->
  (update (update f k2 x1) k1 x2) k3 = (update (update f k1 x2) k2 x1) k3.
Proof.
  rewrite /update. move/beq_id_false_not_eq. case P: (beq_id k1 k3) => //.
    - case Q: (beq_id k2 k3) => //.
      + rewrite (beq_id_eq _ _ Q). rewrite (beq_id_eq _ _ P) => R. by contradiction R.
Qed.

