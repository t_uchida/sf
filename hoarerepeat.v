From mathcomp.ssreflect Require Import ssreflect ssrbool ssrfun eqtype ssrnat seq.

Load implist.

Inductive com : Type :=
  | CSkip : com
  | CAss : id -> aexp -> com
  | CSeq : com -> com -> com
  | CIf : bexp -> com -> com -> com
  | CWhile : bexp -> com -> com
  | CRepeat : com -> bexp -> com.

Notation "'SKIP'" := CSkip.
Notation "l '::=' a" := (CAss l a) (at level 60).
Notation "c1 ; c2" := (CSeq c1 c2) (at level 80, right associativity).
Notation "'WHILE' b 'DO' c 'END'" := (CWhile b c) (at level 80, right associativity).
Notation "'IFB' e1 'THEN' e2 'ELSE' e3 'FI'" := (CIf e1 e2 e3) (at level 80, right associativity).
Notation "'REPEAT' e1 'UNTIL' b2 'END'" := (CRepeat e1 b2) (at level 80, right associativity).

Reserved Notation "c1 '/' st '||' st'" (at level 40, st at level 39).

Inductive ceval : state -> com -> state -> Prop :=
  | E_Skip : forall st, SKIP / st || st
  | E_Asgn : forall st a1 n l, aeval st a1 = n -> (l ::= a1) / st || update st l n
  | E_Seq : forall c1 c2 st st' st'', c1 / st || st' -> c2 / st' || st'' -> (c1 ; c2) / st || st''
  | E_IfTrue : forall st st' b1 c1 c2, beval st b1 = true -> c1 / st || st' -> (IFB b1 THEN c1 ELSE c2 FI) / st || st'
  | E_IfFalse : forall st st' st'' b1 c1 c2, beval st b1 = false -> c2 / st || st' -> (c1 ; c2) / st || st''
  | E_WhileEnd : forall st b1 c1, beval st b1 = false -> (WHILE b1 DO c1 END) / st || st
  | E_WhileLoop : forall st st' st'' b1 c1, beval st b1 = true -> c1 / st || st' -> (WHILE b1 DO c1 END) / st' || st'' -> WHILE b1 DO c1 END / st || st''
  | E_RepeatEnd : forall st st' b c, c / st || st' -> beval st' b = false -> (REPEAT c UNTIL b END) / st || st'
  | E_RepeatLoop : forall st st' st'' b c, c / st || st' -> beval st' b = true -> (REPEAT c UNTIL b END) / st' || st'' -> (REPEAT c UNTIL b END) / st || st''
  where "c1 '/' st '||' st'" := (ceval st c1 st').

Definition Assertion := state -> Prop.

Definition hoare_triple (P : Assertion) (c : com) (Q : Assertion) : Prop :=
  forall st st', c / st || st' -> P st -> Q st'.

Notation "{{ P }} c" := (hoare_triple P c (fun st => True)) (at level 90) : hoare_spec_scope.
Notation "{{ P }} c {{ Q }}" := (hoare_triple P c Q) (at level 90, c at next level) : hoare_spec_scope.
Open Scope hoare_spec_scope.

(*
Lemma hoare_while P b c :
  {{fun st => P st /\ bassn b st}} c {{P}} ->
  {{P}} WHILE b DO c END {{fun st => P st /\ ~ (bassn b st)}}.
Proof.
  move=> Q st st' R S. move: R; move T: (WHILE b DO c END) => wcom R.
  induction R; try (inversion T); subst.
    - split => //. by apply bexp_eval_false.
    - apply IHR2 => //. by apply (Q st st').
Qed.
*)

Definition bassn b : Assertion := fun st => (beval st b = true).

Lemma bexp_eval_true b st : beval st b = true -> (bassn b) st.
Proof.
  by rewrite /bassn.
Qed.

Lemma bexp_eval_false b st : beval st b = false -> ~ ((bassn b) st).
Proof.
  by rewrite /bassn => ->.
Qed.

Lemma hoare_repeat P b c :
  {{fun st => P st /\ bassn b st}} c {{P}} ->
  {{fun st => P st /\ bassn b st}} REPEAT c UNTIL b END {{fun st => P st /\ ~ (bassn b st)}}.
Proof.
  move=> Q st st' R S. move: R; move T: (REPEAT c UNTIL b END) => rcom R.
  induction R; try (inversion T); subst.
    - split. apply (Q st st') => //. by apply bexp_eval_false.
    - apply IHR2 => //. split => //. apply (Q st st') => //.
Qed.


