gc_step(_, [tm_var, _], Ls, Ls).
gc_step(Mem, [tm_app, T1, T2], Ls1, Ls3) :- gc_step(Mem, T1, Ls1, Ls2), gc_step(Mem, T2, Ls2, Ls3).
gc_step(Mem, [tm_abs, _, _, T], Ls1, Ls2) :- gc_step(Mem, T, Ls1, Ls2).
gc_step(_, [tm_nat, _], Ls, Ls).
gc_step(Mem, [tm_succ, T], Ls1, Ls2) :- gc_step(Mem, T, Ls1, Ls2).
gc_step(Mem, [tm_pred, T], Ls1, Ls2) :- gc_step(Mem, T, Ls1, Ls2).
gc_step(Mem, [tm_mult, T1, T2], Ls1, Ls3) :- gc_step(Mem, T1, Ls1, Ls2), gc_step(Mem, T2, Ls2, Ls3).
gc_step(Mem, [tm_if0, T1, T2, T3], Ls1, Ls4) :- gc_step(Mem, T1, Ls1, Ls2), gc_step(Mem, T2, Ls2, Ls3), gc_step(Mem, T3, Ls3, Ls4).
gc_step(_, [tm_unit], Ls, Ls).
gc_step(Mem, [tm_ref, T], Ls1, Ls2) :- gc_step(Mem, T, Ls1, Ls2).
gc_step(Mem, [tm_deref, T], Ls1, Ls2) :- gc_step(Mem, T, Ls1, Ls2).
gc_step(Mem, [tm_assign, T1, T2], Ls1, Ls3) :- gc_step(Mem, T1, Ls1, Ls2), gc_step(Mem, T2, Ls2, Ls3).
gc_step(_, [tm_loc, L], Ls, Ls) :- member(L, Ls).
gc_step(Mem, [tm_loc, L], Ls1, Ls3) :- \+ member(L, Ls1), Ls2 = [L|Ls1], nth0(L, Mem, T), gc_step(Mem, T, Ls2, Ls3).

gc_step(Mem, Tm, Ls) :- gc_step(Mem, Tm, [], Ls).

gc_update_term(_, [tm_var, X], [tm_var, X]).
gc_update_term(Ls, [tm_app, T11, T12], [tm_app, T21, T22]) :- gc_update_term(Ls, T11, T21), gc_update_term(Ls, T12, T22).
gc_update_term(Ls, [tm_abs, X, Ty, T1], [tm_abs, X, Ty, T2]) :- gc_update_term(Ls, T1, T2).
gc_update_term(_, [tm_nat, N], [tm_nat, N]).
gc_update_term(Ls, [tm_succ, T1], [tm_succ, T2]) :- gc_update_term(Ls, T1, T2).
gc_update_term(Ls, [tm_pred, T1], [tm_pred, T2]) :- gc_update_term(Ls, T1, T2).
gc_update_term(Ls, [tm_mult, T11, T12], [tm_mult, T21, T22]) :- gc_update_term(Ls, T11, T21), gc_update_term(Ls, T12, T22).
gc_update_term(Ls, [tm_if0, T11, T12, T13], [tm_if0, T21, T22, T23]) :- gc_update_term(Ls, T11, T21), gc_update_term(Ls, T12, T22), gc_update_term(Ls, T13, T23).
gc_update_term(_, [tm_unit], [tm_unit]).
gc_update_term(Ls, [tm_ref, T1], [tm_ref, T2]) :- gc_update_term(Ls, T1, T2).
gc_update_term(Ls, [tm_deref, T1], [tm_deref, T2]) :- gc_update_term(Ls, T1, T2).
gc_update_term(Ls, [tm_assign, T11, T12], [tm_assign, T21, T22]) :- gc_update_term(Ls, T11, T21), gc_update_term(Ls, T12, T22).
gc_update_term(Ls, [tm_loc, L1], [tm_loc, L2]) :- nth0(L2, Ls, L1).

gc_copy(Mem, Ls, Ts) :- gc_copy(Mem, Ls, Ls, Ts).

gc_copy(_, _, [], []).
gc_copy(Mem, LsRef, [L|Ls], [T2|Ts]) :- nth0(L, Mem, T1), gc_update_term(LsRef, T1, T2), gc_copy(Mem, LsRef, Ls, Ts).

:- begin_tests(gc_step).

test(gc_step_var, all(Count = [1])) :- gc_step([], [tm_var, x], []).
test(gc_step_app, all(Count = [1])) :- gc_step([], [tm_app, [tm_abs, [tm_var, x], ty_nat, [tm_succ, [tm_var, x]]], [tm_nat, 1]], []).
test(gc_step_abs, all(Count = [1])) :- gc_step([], [tm_abs, [tm_var, x], ty_nat, [tm_succ, [tm_var, x]]], []).
test(gc_step_loc_01, all(Count = [1])) :- gc_step([[tm_var, x]], [tm_loc, 0], [0]).
test(gc_step_loc_02, all(Count = [1])) :- gc_step([[tm_loc, 1], [tm_var, x]], [tm_loc, 0], [1, 0]).
test(gc_step_loc_03, all(Count = [1])) :- gc_step([[tm_loc, 2], [tm_var, ys], [tm_var, x]], [tm_loc, 0], [2, 0]).
test(gc_step_loc_04, all(Count = [1])) :- gc_step([[tm_var, x]], [tm_deref, [tm_loc, 0]], [0]).
test(gc_step_loc_05, all(Count = [1])) :- gc_step([[tm_var, x], [tm_var, ys], [tm_var, zs]], [tm_mult, [tm_deref, [tm_loc, 2]], [tm_deref, [tm_loc, 0]]], [0, 2]).
test(gc_step_loc_06, all(Count = [1])) :- gc_step([[tm_loc, 1], [tm_var, ys], [tm_var, zs]], [tm_mult, [tm_deref, [tm_loc, 2]], [tm_deref, [tm_loc, 0]]], [1, 0, 2]).
test(gc_step_loc_07, all(Count = [1])) :- gc_step([[tm_loc, 1], [tm_loc, 0]], [tm_loc, 0], [1, 0]).
test(gc_step_loc_08, all(Count = [1])) :- gc_step([[tm_loc, 2], [tm_var, x], [tm_loc, 0]], [tm_loc, 0], [2, 0]).

:- end_tests(gc_step).

:- begin_tests(gc_copy).

test(gc_copy_01, all(Count = [1])) :- gc_copy([], [], []).
test(gc_copy_02, all(Count = [1])) :- gc_copy([[tm_nat, 1]], [0], [[tm_nat, 1]]).
test(gc_copy_03, all(Count = [1])) :- gc_copy([[tm_nat, 1]], [], []).
test(gc_copy_04, all(Count = [1])) :- gc_copy([[tm_unit], [tm_nat, 1]], [1], [[tm_nat, 1]]).
test(gc_copy_05, all(Count = [1])) :- gc_copy([[tm_var, x], [tm_unit], [tm_nat, 1]], [2, 0], [[tm_nat, 1], [tm_var, x]]).
test(gc_copy_06, all(Count = [1])) :- gc_copy([[tm_var, x], [tm_unit], [tm_nat, 1], [tm_succ, [tm_nat, 1]]], [3, 1], [[tm_succ, [tm_nat, 1]], [tm_unit]]).
test(gc_copy_07, all(Count = [1])) :- gc_copy([[tm_loc, 2], [tm_unit], [tm_loc, 0]], [2, 0], [[tm_loc, 1], [tm_loc, 0]]).

:- end_tests(gc_copy).

:- begin_tests(gc_update_term).

test(gc_update_term_var, all(Count = [1])) :- gc_update_term([], [tm_var, x], [tm_var, x]).
test(gc_update_term_abs, all(Count = [1])) :- gc_update_term([], [tm_abs, [tm_var, x], ty_nat, [tm_succ, [tm_var, x]]], [tm_abs, [tm_var, x], ty_nat, [tm_succ, [tm_var, x]]]).
test(gc_update_term_loc_01, all(Count = [1])) :- gc_update_term([0], [tm_loc, 0], [tm_loc, 0]).
test(gc_update_term_loc_02, all(Count = [1])) :- gc_update_term([1, 0], [tm_mult, [tm_deref, [tm_loc, 1]], [tm_deref, [tm_loc, 0]]], [tm_mult, [tm_deref, [tm_loc, 0]], [tm_deref, [tm_loc, 1]]]).
test(gc_update_term_loc_03, all(Count = [1])) :- gc_update_term([2, 4], [tm_mult, [tm_deref, [tm_loc, 4]], [tm_deref, [tm_loc, 2]]], [tm_mult, [tm_deref, [tm_loc, 1]], [tm_deref, [tm_loc, 0]]]).

:- end_tests(gc_update_term).

