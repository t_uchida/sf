Require Import mathcomp.ssreflect.ssreflect.
From mathcomp Require Import all_ssreflect.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Inductive id : Type := Id : nat -> id.

Definition eqb_id i1 i2 :=
  match (i1, i2) with
  | (Id n1, Id n2) => n1 == n2
  end.

Lemma eqb_idP : Equality.axiom eqb_id.
Proof.
  rewrite /Equality.axiom. move=> [] nx [] ny. case H0: (nx == ny).
  - rewrite /eqb_id. rewrite H0. constructor. move/eqP :H0 => <- //.
  - rewrite /eqb_id. rewrite H0. constructor. move/eqP :H0 => H0 H1. apply H0. by inversion H1.
Qed.

Canonical id_eqMixin := EqMixin eqb_idP.
Canonical id_eqType := Eval hnf in EqType id id_eqMixin.

Lemma eqb_id_refl (x : id) : x == x.
Proof. by apply/eqP. Qed.

Inductive ty : Type :=
  | TyBool : ty
  | TyArrow : ty -> ty -> ty.

Inductive tm : Type :=
  | TmVar : id -> tm
  | TmApp : tm -> tm -> tm
  | TmAbs : id -> ty -> tm -> tm
  | TmTrue : tm
  | TmFalse : tm
  | TmIf : tm -> tm -> tm -> tm.

Inductive value : tm -> Prop :=
  | VAbs : forall x T t, value (TmAbs x T t)
  | VTrue : value TmTrue
  | VFalse : value TmFalse.

Hint Constructors value.

Fixpoint subst (s:tm) (x:id) (t:tm) : tm :=
  match t with
  | TmVar x' => if x == x' then s else t
  | TmAbs x' T t1 => TmAbs x' T (if x == x' then t1 else (subst s x t1))
  | TmApp t1 t2 => TmApp (subst s x t1) (subst s x t2)
  | TmTrue => TmTrue
  | TmFalse => TmFalse
  | TmIf t1 t2 t3 => TmIf (subst s x t1) (subst s x t2) (subst s x t3)
  end.

Inductive step : tm -> tm -> Prop :=
  | STAppAbs : forall x T t12 v2, value v2 -> TmApp (TmAbs x T t12) v2 ==> subst v2 x t12
  | STApp1 : forall t1 t1' t2, t1 ==> t1' -> TmApp t1 t2 ==> TmApp t1' t2
  | STApp2 : forall v1 t2 t2', value v1 -> t2 ==> t2' -> TmApp v1 t2 ==> TmApp v1 t2'
  | STIfTrue : forall t1 t2, TmIf TmTrue t1 t2 ==> t1
  | STIfFalse : forall t1 t2, TmIf TmFalse t1 t2 ==> t2
  | STIf : forall t1 t1' t2 t3, t1 ==> t1' -> TmIf t1 t2 t3 ==> TmIf t1' t2 t3
  where "t1 '==>' t2" := (step t1 t2).

Definition relation (X:Type) := X -> X -> Prop.

Inductive refl_step_closure (X:Type) (R: relation X) : X -> X -> Prop :=
  | RSCRefl : forall x, refl_step_closure R x x
  | RSCStep : forall x y z, R x y -> refl_step_closure R y z -> refl_step_closure R x z.

Notation stepmany := (refl_step_closure step).
Notation "t1 '==>*' t2" := (stepmany t1 t2) (at level 40).

Definition partial_map A := id -> option A.

Hint Constructors step.

Definition empty {A : Type} : partial_map A := (fun _ => None).

Definition extend {A : Type} (c : partial_map A) (x:id) (T : A) :=
  fun x' => if x == x' then Some T else c x'.

Lemma extend_eq {A : Type} : forall c x T, (@extend A c x T) x = Some T.
Proof. move=> c x. rewrite /extend eqb_id_refl => //. Qed.

Lemma extend_neq {A : Type} : forall c x1 T x2,
  x2 == x1 = false -> (@extend A c x2 T) x1 = c x1.
Proof. move=> c x1 T x2 H0. rewrite /extend. by rewrite H0. Qed.

Definition context := partial_map ty.

Inductive has_type : context -> tm -> ty -> Prop :=
  | HVar : forall c x T, c x = Some T -> has_type c (TmVar x) T
  | HAbs : forall c x T11 T12 t12,
      has_type (extend c x T11) t12 T12 -> has_type c (TmAbs x T11 t12) (TyArrow T11 T12)
  | HApp : forall T11 T12 c t1 t2,
      has_type c t1 (TyArrow T11 T12) -> has_type c t2 T11 -> has_type c (TmApp t1 t2) T12
  | HTrue : forall c, has_type c TmTrue TyBool
  | HFalse : forall c, has_type c TmFalse TyBool
  | HIf : forall t1 t2 t3 T c,
      has_type c t1 TyBool -> has_type c t2 T -> has_type c t3 T -> has_type c (TmIf t1 t2 t3) T.

Hint Constructors has_type.

Inductive appears_free_in : id -> tm -> Prop :=
  | AFIVar : forall x, appears_free_in x (TmVar x)
  | AFIApp1 : forall x t1 t2, appears_free_in x t1 -> appears_free_in x (TmApp t1 t2)
  | AFIApp2 : forall x t1 t2, appears_free_in x t2 -> appears_free_in x (TmApp t1 t2)
  | AFIAbs : forall x y T11 t12,
      y == x = false -> appears_free_in x t12 -> appears_free_in x (TmAbs y T11 t12)
  | AFIIf1 : forall x t1 t2 t3, appears_free_in x t1 -> appears_free_in x (TmIf t1 t2 t3)
  | AFIIf2 : forall x t1 t2 t3, appears_free_in x t2 -> appears_free_in x (TmIf t1 t2 t3)
  | AFIIf3 : forall x t1 t2 t3, appears_free_in x t3 -> appears_free_in x (TmIf t1 t2 t3).

Hint Constructors appears_free_in.

Definition closed (t:tm) := forall x, ~ appears_free_in x t.

Lemma free_in_context :
  forall x t T c, appears_free_in x t -> has_type c t T -> exists T', c x = Some T'.
Proof.
  move=> x t T c H0. elim: H0 c T.
  - move=> x1 c T H0. inversion H0; subst. by exists T.
  - move=> x1 t1 t2 H0 IH0 c T H1. inversion H1; subst. eapply IH0. apply H4.
  - move=> x1 t1 t2 H0 IH0 c T H1. inversion H1; subst. eapply IH0. apply H6.
  - move=> x1 x2 T11 t12 H0 H1 IH0 c T H2. inversion H2; subst.
    move/IH0 :H7. rewrite (extend_neq c _ H0). done.
  - move=> x1 t1 t2 t3 H0 IH0 c T H1. inversion H1; subst. eapply IH0. apply H5.
  - move=> x1 t1 t2 t3 H0 IH0 c T H1. inversion H1; subst. eapply IH0. apply H7.
  - move=> x1 t1 t2 t3 H0 IH0 c T H1. inversion H1; subst. eapply IH0. apply H8.
Qed.

Corollary typable_empty__closed t T : has_type empty t T -> closed t.
Proof. rewrite /closed => H0 x H1. move: (free_in_context H1 H0) => [] //. Qed.

Lemma context_invariance c1 c2 t S :
  has_type c1 t S -> (forall x, appears_free_in x t -> c1 x = c2 x) -> has_type c2 t S.
Proof.
  move=> H0. elim: H0 c2.
  - move=> c2 x T H0 c3 H1. constructor. by rewrite -H1.
  - move=> c2 x T11 T12 t12 H0 H1 c3 H2. constructor. apply H1 => y H3.
    move H4: (x == y) => b. case: b H4 => H4.
    + by rewrite /extend H4.
    + rewrite /extend H4. apply H2. by constructor.
  - move=> T11 T12 c2 t1 t2 H1 IH1 H2 IH2 c3 H3. eapply HApp.
    + apply IH1 => x H4. apply H3. by apply AFIApp1.
    + apply IH2 => x H4. apply H3. by apply AFIApp2.
  - constructor.
  - constructor.
  - move=> t1 t2 t3 T c2 H1 IH1 H2 IH2 H3 IH3 c3 H4. constructor.
    + apply IH1 => x H5. apply H4. by apply AFIIf1.
    + apply IH2 => x H5. apply H4. by apply AFIIf2.
    + apply IH3 => x H5. apply H4. by apply AFIIf3.
Qed.

Lemma substitution_preserves_typing c x U v t T :
  has_type (extend c x U) t T -> has_type empty v U -> has_type c (subst v x t) T.
Proof.
  elim: t T U.
  - move=> x1 T U H1 H2. rewrite /subst. move H3: (x == x1) => b. case: b H3 => H3.
    + inversion H1; subst. move: H4. rewrite /extend H3. case => <-. eapply context_invariance.
      apply H2. move :(typable_empty__closed H2). rewrite /closed. move=> H4 x2. by move/H4.
    + inversion H1; subst. move: H4. rewrite /extend H3. by constructor.
  - move=> t1 IH1 t2 IH2 T U H1 H2 /=. inversion H1; subst. eapply HApp.
    eapply IH1. apply H4. apply H2. eapply IH2. apply H6. apply H2.
  - move=> x1 t1 t2 IHt T U H1 H2 /=. move H3: (x == x1) => b. case: b H3 => H3.
    + 