From mathcomp.ssreflect Require Import ssreflect ssrbool ssrfun eqtype ssrnat seq.

Inductive tm : Type :=
  | tm_true : tm
  | tm_false : tm
  | tm_if : tm -> tm -> tm -> tm
  | tm_zero : tm
  | tm_succ : tm -> tm
  | tm_pred : tm -> tm
  | tm_iszero : tm -> tm.

Inductive bvalue : tm -> Prop :=
  | bv_true : bvalue tm_true
  | bv_false : bvalue tm_false.

Inductive nvalue : tm -> Prop :=
  | nv_zero : nvalue tm_zero
  | nv_succ : forall t, nvalue t -> nvalue (tm_succ t).

Definition value (t:tm) := bvalue t \/ nvalue t.

Hint Constructors bvalue nvalue.
Hint Unfold value.


Inductive step : tm -> tm -> Prop :=
  | ST_IfTrue : forall t1 t2, (tm_if tm_true t1 t2) ==> t1
  | ST_IfFalse : forall t1 t2, (tm_if tm_false t1 t2) ==> t2
  | ST_If : forall t1 t1' t2 t3, t1 ==> t1' -> (tm_if t1 t2 t3) ==> (tm_if t1' t2 t3)
  | ST_Succ : forall t1 t1', t1 ==> t1' -> (tm_succ t1) ==> (tm_succ t1')
  | ST_PredZero : (tm_pred tm_zero) ==> tm_zero
  | ST_PredSucc : forall t1, nvalue t1 -> (tm_pred (tm_succ t1)) ==> t1
  | ST_Pred : forall t1 t1', t1 ==> t1' -> (tm_pred t1) ==> (tm_pred t1')
  | ST_IszeroZero : (tm_iszero tm_zero) ==> tm_true
  | ST_IszeroSucc : forall t1, nvalue t1 -> (tm_iszero (tm_succ t1)) ==> tm_false
  | ST_Iszero : forall t1 t1', t1 ==> t1' -> (tm_iszero t1) ==> (tm_iszero t1')

where "t1 '==>' t2" := (step t1 t2).

Hint Constructors step.

Definition relation A := A -> A -> Prop.

Definition normal_form {X : Type} (R : relation X) (t : X) : Prop := ~ exists t', R t t'.

Notation step_normal_form := (normal_form step).

Definition stuck (t : tm) : Prop := step_normal_form t /\ ~ value t.

Hint Unfold stuck.

Example some_tm_is_stuck : exists t, stuck t.
  exists (tm_succ tm_true). rewrite /stuck /step_normal_form /value. split.
    - elim=> t P. inversion P; subst. inversion H0.
    - move=> P. elim P => Q; inversion Q; subst. inversion H0.
Qed.

Lemma value_is_nf t : value t -> step_normal_form t.
Proof.
  rewrite /step_normal_form. induction t => P Q; elim: Q; move=> x Q; subst.
    - inversion Q.
    - inversion Q.
    - inversion P; inversion H.
    - inversion Q.
    - inversion P; inversion H.
    - inversion P; inversion H2; subst. inversion Q; subst. apply IHt. rewrite /value. right. apply H1. by exists t1'.
    - inversion P; inversion H.
    - inversion P; inversion H.
Qed.

Lemma value_is_nf' t : value t -> step_normal_form t.
Proof.
  rewrite /step_normal_form => P Q. elim: Q => x Q. induction P.
    - inversion H; subst; inversion Q.
    - inversion H; subst. inversion Q.
      inversion Q; subst. eapply value_is_nf. right. apply H0. by exists t1'. 
Qed.

Inductive ty : Type :=
  | ty_Bool : ty
  | ty_Nat : ty.

Inductive has_type : tm -> ty -> Prop :=
  | T_True :
       has_type tm_true ty_Bool
  | T_False :
       has_type tm_false ty_Bool
  | T_If : forall t1 t2 t3 T,
       has_type t1 ty_Bool ->
       has_type t2 T ->
       has_type t3 T ->
       has_type (tm_if t1 t2 t3) T
  | T_Zero :
       has_type tm_zero ty_Nat
  | T_Succ : forall t1,
       has_type t1 ty_Nat ->
       has_type (tm_succ t1) ty_Nat
  | T_Pred : forall t1,
       has_type t1 ty_Nat ->
       has_type (tm_pred t1) ty_Nat
  | T_Iszero : forall t1,
       has_type t1 ty_Nat ->
       has_type (tm_iszero t1) ty_Bool.

Hint Constructors has_type.

Example has_type_1 :
  has_type (tm_if tm_false tm_zero (tm_succ tm_zero)) ty_Nat.
Proof. auto. Qed.

Example has_type_not :
  ~ has_type (tm_if tm_false tm_zero tm_true) ty_Bool.
Proof.
  move=> P. inversion P; subst. inversion H4.
Qed.

Example succ_hastype_nat__hastype_nat t :
  has_type (tm_succ t) ty_Nat ->
  has_type t ty_Nat.
Proof.
  move=> P. by inversion P.
Qed.

Theorem progress t T : has_type t T -> value t \/ exists t', t ==> t'.
Proof.
  move=> P. induction P; auto.
    - case: IHP1 => Q.
      + case: Q => Q. case: Q.
        * right. by exists t2.
        * right. by exists t3.
      + move: P1. case: Q => [Q|t4 Q R]. inversion Q. inversion R.
      + case Q => t4 R. right. exists (tm_if t4 t2 t3). by constructor.
    - case: IHP => Q.
      + case: Q => Q.
        * inversion Q; subst; inversion P.
        * left. right. by constructor.
      + case: Q => t1' Q. right. exists (tm_succ t1'). by constructor.
    - case: IHP => Q.
      + case: Q => Q.
        * inversion Q; subst; inversion P.
        * right. case: Q => [|t2 Q]. exists tm_zero. constructor. exists t2. by constructor.
      + case: Q => t1' Q. right. exists (tm_pred t1'). by constructor.
    - case: IHP => Q.
      + case: Q => Q.
        * inversion Q; subst; inversion P.
        * right. case: Q => [|t2 Q]. exists tm_true. constructor. exists tm_false. by constructor.
      + case: Q => t1' Q. right. exists (tm_iszero t1'). by constructor.
Qed.

Theorem preservation t t' T :
  has_type t T -> t ==> t' -> has_type t' T.
Proof.
  move=> P. move: t'. induction P; move=> t Q.
    - inversion Q.
    - inversion Q.
    - inversion Q; subst; try assumption.
      + constructor; try assumption. by apply IHP1.
    - inversion Q.
    - inversion Q; subst. constructor. by apply IHP.
    - inversion Q; subst; try assumption.
      + by inversion P; subst.
      + constructor. by apply IHP.
    - inversion Q; subst; try constructor.
       + by apply IHP.
Qed.

Theorem preservation' t t' T : has_type t T -> t ==> t' -> has_type t' T.
Proof.
  move=> P Q. move: T P. induction Q => T P.
    - by inversion P.
    - by inversion P.
    - inversion P; subst. constructor => //. by apply IHQ.
    - inversion P; subst. constructor => //. by apply IHQ.
    - inversion P; subst. constructor.
    - inversion P; subst. by inversion H1; subst.
    - inversion P; subst. constructor. by apply IHQ.
    - inversion P; subst. constructor.
    - inversion P; subst. constructor.
    - inversion P; subst. constructor. by apply IHQ.
Qed.

Inductive refl_step_closure (X:Type) (R: relation X) 
                            : X -> X -> Prop :=
  | rsc_refl : forall (x : X),
                 refl_step_closure X R x x
  | rsc_step : forall (x y z : X),
                    R x y ->
                    refl_step_closure X R y z ->
                    refl_step_closure X R x z.
Implicit Arguments refl_step_closure [[X]].

Definition stepmany := (refl_step_closure step).
Notation "t1 '==>*' t2" := (stepmany t1 t2) (at level 40).

Corollary soundness t t' T : has_type t T -> t ==>* t' -> ~ stuck t'.
Proof.
  move=> P Q. induction Q.
    - move=> [R S]. elim: (progress x T P); auto.
    - apply IHQ. apply (preservation x y T P H).
Qed.

Theorem subject_expansion : exists t t' T, t ==> t' -> has_type t' T -> ~ has_type t T.
  exists (tm_if tm_true tm_true tm_zero). exists tm_true. exists ty_Bool.
  move=> P Q R. inversion R; subst. inversion H5.
Qed.




